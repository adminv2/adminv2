import { Component, OnInit, ElementRef } from '@angular/core';
import { Http, Response } from '@angular/http';
import { FileUploader } from 'ng2-file-upload';
import "rxjs/add/operator/do";
import { DataService } from '../data.service';
import "rxjs/add/operator/map";
import { baseUrl } from '../app.config';
import { ExcelService } from '../excel.service';
import * as _ from "lodash";

@Component({
  selector: 'app-payout-histr',
  templateUrl: './payout-histr.component.html',
  styleUrls: ['./payout-histr.component.css']
})
export class PayoutHistrComponent implements OnInit {
  model: any;
  filesData = new Array<any>();
  cars = new Array<any>();
  emptyArray = new Array<any>();
  IShow: Boolean = true;
  public uploader: FileUploader = new FileUploader({ url: baseUrl + "payout/histr" });

  constructor(private http: Http, private el: ElementRef, private _dataService: DataService, private excelService: ExcelService) {
    this._dataService.authCheck('WithdrawINR');
    this.excelService = excelService;
  }

  ngOnInit() {
    this.IShow = false;
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      var SomeData = JSON.parse(response);
      console.log(SomeData)
      this.IShow = true,
        this.filesData = SomeData.data;
    };
  }
  upload() {
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
    let fileCount: number = inputEl.files.length;
    let formData = new FormData();
    if (fileCount > 0) {
      formData.append('photo', inputEl.files.item(0));
      this.http.post(baseUrl + "payout/histr", formData).map((res: Response) => res.json()).subscribe(
        (success) => {
          console.log(success._body);
        },
        (error) => alert(error))
    }
  }
  randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  exportToExcel() {
    this.excelService.exportAsExcelFile(this.emptyArray, 'Payment' + '_' + this.randomName());
  }
  check(option, state, e) {
    if (state) {
      this.emptyArray.push(option);
      this.emptyArray = _.uniqBy(this.emptyArray, 'client_id');
    } else {
      for (var i = 0; i < this.emptyArray.length; i++) {
        console.log(this.emptyArray[i].state);
        if (this.emptyArray[i].state === true) {
          _.remove(this.emptyArray, { Client_id: option.client_id });

        }
      }
    }
  }
  
  checkAll(ev) {
    this.emptyArray = this.filesData;
    this.cars.length = 0;
    var checkedList = {};
    if (ev.target.checked) {
      this.emptyArray.forEach(element => {
        element.state = ev.target.checked;
        checkedList['client_id'] = element.client_id;
        checkedList['amount'] = element.amount;
        checkedList['txid'] = element.txid;
        checkedList['sent_address'] = element.sent_address;
        checkedList['transaction_date'] = element.transaction_date;
        checkedList['created'] = element.created;
        checkedList['updated'] = element.updated;
        checkedList['withdrawal_type'] = element.withdrawal_type;
        checkedList['confirmations'] = element.confirmations;
        checkedList['status'] = element.status;
        checkedList['currency'] = element.currency;
        checkedList['type'] = element.type;
        checkedList['state'] = true;
        this.cars.push(checkedList);
        checkedList = {};
      });
      this.emptyArray = this.cars;
      _.uniqBy(this.emptyArray, 'client_id');
    } else {
      this.emptyArray.forEach(x => x.state = ev.target.checked)

    }

  }
  isAllChecked() {
    return this.emptyArray.every(_ => _.state);
  }

  buttonState() {
    return !this.emptyArray.some(_ => _.state);
  }
  UncheckAll() {
    var w = document.getElementsByTagName('input');
    for (var i = 0; i < w.length; i++) {
      if (w[i].type == 'checkbox') {
        w[i].checked = false;
      }
    }
  }
}
