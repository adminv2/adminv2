import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutHistrComponent } from './payout-histr.component';

describe('PayoutHistrComponent', () => {
  let component: PayoutHistrComponent;
  let fixture: ComponentFixture<PayoutHistrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutHistrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutHistrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
