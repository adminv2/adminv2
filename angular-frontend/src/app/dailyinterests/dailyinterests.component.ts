import { Component, OnInit ,AfterViewInit,ViewChild,Input} from '@angular/core';
import { DataService } from '../data.service';
import {MatTableDataSource} from '@angular/material';
import { MatPaginator } from '@angular/material';
import { MatSort } from '@angular/material';
import { Sort } from '@angular/material';
import {MatSortModule} from '@angular/material/sort';
import { Element } from '@angular/compiler';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExcelService } from '../excel.service';
import * as _ from "lodash";

@Component({
  selector: 'app-dailyinterests',
  templateUrl: './dailyinterests.component.html',
  styleUrls: ['./dailyinterests.component.css']
})
export class DailyinterestsComponent implements AfterViewInit {
  p:number= 1;
  count:number= 0;
  data:any;
  message:any;
  model:any;
  interest = new Array<any>();
  emptyArray = new Array<any>();
  sort_value: number = -1;
  sort: any;
  constructor(public _dataService:DataService,private excelService: ExcelService) {
  this.model= {search:''}
  this._dataService.authCheck('DailyIntrest');
  this.sort = {sortField:'created',sortType:'-1'}

  this.excelService = excelService;
  
 }
 ngAfterViewInit() {
  this._dataService.getDailyInterest(this.p,this.model.search,this.sort).subscribe(res=>{
    console.log(" res.data", res.data);
    this.data = res.data;
    this.count = res.count;
   });  
  }

  pageChange(p){
    this.interest.length=0;
    this.emptyArray.length=0;
    this.UncheckAll();
    this.p = p
    this.ngAfterViewInit()
  }

  sortByAmount(sortField) {
    if(this.sort_value == -1){
      this.sort = {sortField:sortField,sortType:-1};
      this.sort_value = 1
    }else{
      this.sort = {sortField:sortField,sortType: 1};
      this.sort_value = -1
    }
    this.ngAfterViewInit()
  }


  searchResult(){
    if(this.model.search == ''){
      this.p = 1
      this.ngAfterViewInit()
    }
  }
  
  searchResultData(){
    this.p = 1
    this.ngAfterViewInit()
   
  }
  randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  exportToExcel() {
    console.log("exportToExcel",this.interest);
    this.excelService.exportAsExcelFile(this.interest, 'interests' + '_' + this.randomName());
  }
  check(option,state, e) {
    if(state){
      this.interest.push(option);
      this.interest= _.uniqBy(this.interest, '_id');
    } else {
      for(var i=0;i<this.interest.length;i++){
        console.log(this.interest[i].state);
         if(this.interest[i].state === true){
            _.remove(this.interest, {client_id: option.client_id});
         }
      }
   }
 }
  checkAll (ev) {
    this.interest=this.data;
    this.emptyArray.length=0;
    var checkedList={};
    if (ev.target.checked) {
      this.interest.forEach(element => {
        element.state = ev.target.checked;
        checkedList['client_id'] = element.client_id;
        checkedList['date'] = element.date;
        checkedList['interest'] = element.interest;
        checkedList['dailyAmount'] = element.dailyAmount;
        checkedList['totalAmount'] = element.totalAmount;
        checkedList['state'] = true;
        this.emptyArray.push(checkedList);
        checkedList = {};
      });
      this.interest = this.emptyArray;
      _.uniqBy(this.interest, 'client_id');
    } else {
      this.interest.forEach(x => x.state = ev.target.checked)
      console.log(this.interest);
    }

  }
  isAllChecked() {
    return this.interest.every(_ => _.state);
  }

  buttonState() {
    return !this.interest.some( _=>_ .state);
  }
  UncheckAll() {
    var w = document.getElementsByTagName('input');
    for (var i = 0; i < w.length; i++) {
      if (w[i].type == 'checkbox') {
        w[i].checked = false;
      }
    }
  }
  
}