import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

import { baseUrl } from './app.config';

@Injectable()
export class UserProfile {

  constructor(private _http: Http, private router: Router) {

  }

  getUserDetails(client_id) {
    return this._http.post(baseUrl + "profile/userprofile_info", { client_id: client_id })
      .map(result => result.json());
  }

  sendMail(data) {
    return this._http.post(baseUrl + "profile/sendMail", data)
      .map(result => result.json());
  }

  getUsersList(isVerified, p, search) {
    return this._http.post(baseUrl + "profile/usersList/" + isVerified + "/" + p, { search: search })
      .map(result => result.json());
  }
  getNotifications(client_id) {
    return this._http.post(baseUrl + "profile/getNotifications", { client_id: client_id })
      .map(result => result.json());
  }


  getDailyInterestDetails(client_id, dailyInterestPage) {
    return this._http.post(baseUrl + "profile/usersDailyInterest" + "/" + dailyInterestPage, { client_id: client_id })
      .map(result => result.json());
  }
  updateUsersInfo(data) {
    return this._http.post(baseUrl + "profile/updateUsersInfo", data)
      .map(result => result.json());
  }
  addBankInfo(data) {
    return this._http.post(baseUrl + "profile/addUsersBank", data)
      .map(result => result.json());
  }

  updateWithdlimits(data) {
    return this._http.post(baseUrl + "profile/updateWithdlimits", data)
      .map(result => result.json());
  }

  updateKyc(updateKyc) {
    return this._http.post(baseUrl + "profile/updateKyc", updateKyc)
      .map(result => result.json());
  }
  updateBalance(updateBalance, for_currency, client_id, updated_by) {
    console.log("in .ts", updateBalance);
    return this._http.post(baseUrl + "profile/updateBalance/" + updateBalance + "/" + for_currency + '/' + client_id + '/' + updated_by, '')
      .map(result => result.json());
  }
  changeWithdrawalLimitStatus(status) {
    return this._http.post(baseUrl + "profile/changeWithdrawalLimitStatus", status)
      .map(result => result.json());
  }
  checkEmailExists(email, client_id) {
    return this._http.post(baseUrl + "profile/checkEmailExists", { email: email, client_id: client_id })
      .map(result => result.json());
  }
  // changeUsersStatus(){

  // }
  changeUsersStatus(id, status, doc_type, reason, suspend_changed_by) {
    let data = {
      id: id,
      status: status,
      doc_type: doc_type,
      reason: reason,
      suspend_changed_by: suspend_changed_by
    };
    return this._http.post(baseUrl + "profile/ChangeStatus", data)
      .map(result => result.json());
  }
  ChangeGoogleAuthStatus(id, status, doc_type, reason, suspend_changed_by) {
    let data = {
      id: id,
      status: status,
      doc_type: doc_type,
      reason: reason,
      suspend_changed_by: suspend_changed_by
    };
    return this._http.post(baseUrl + "profile/ChangeGoogleAuthStatus", data)
      .map(result => result.json());
  }

  getEquivalentBalanceOf(client_id) {
    return this._http.post(baseUrl + "profile/getEquivalentBalance", { client_id: client_id })
      .map(result => result.json());
  }


}
