import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositInrPendingComponent } from './deposit-inr-pending.component';

describe('DepositInrPendingComponent', () => {
  let component: DepositInrPendingComponent;
  let fixture: ComponentFixture<DepositInrPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositInrPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositInrPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
