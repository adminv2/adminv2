import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../data.service';
import { ExcelService } from '../excel.service';
import * as _ from "lodash";
import * as moment from 'moment';

@Component({
  selector: 'app-deposit-inr-pending',
  templateUrl: './deposit-inr-pending.component.html',
  styleUrls: ['./deposit-inr-pending.component.css']
})
export class DepositInrPendingComponent implements OnInit {
  currency_records: any;
  currency: any;
  status: any;
  model: any;
  loader = 1;
  processing = 1;
  selectedIndex: number;
  count: number;
  public iterator: number = 0;
  p = 1;
  payU: number = 0;
  sort: any;
  toSortBy: any;
  user: any;

  withdrawal_type: number = 0;

  public show: boolean = false;
  disableButton: boolean = true;
  useraccess: any;
  sort_value: number = -1;
  inrdiposit = new Array<any>();
  emptyArray = new Array<any>();
  // @Input() p: number = 1;

  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _dataService: DataService, private excelService: ExcelService) {
    this.disableButton = true;
    this.model = { search: '' }
    this.sort = {sortField:'created',sortType:'-1'}
    this.useraccess = JSON.parse(localStorage.getItem('useraccess'));

    this._dataService.authCheck('DepositINR');
    this.disableButton = true;
    this.excelService = excelService;
  }
  ngOnInit() {
    this.getDeposits();
    this.user = JSON.parse(localStorage.getItem('current'));
    // console.log("this.user", this.user._id);


  }


  ngOnChanges() {
    // console.log('on change', this.p)

  }
  deposit_inr_status(id) {

    if (this.selectedIndex == id) {
      console.log("deposit_inr_status-if");
      this.selectedIndex = null;
    } else {
      console.log("deposit_inr_status-else");
      this.selectedIndex = id;
      this.show = !this.show;
    }
  }
  changeStatus(client_id, id, status, amount) {
    this.disableButton = false;
    this._dataService.changeInrDepositStatus(client_id, id, status, amount,this.user._id)
      .subscribe(res => {
        this.getDeposits();
      });
  }


  sortByAmount(sortField) {
    if(this.sort_value == -1){
      this.sort = {sortField:sortField,sortType:-1};
      this.sort_value = 1
    }else{
      this.sort = {sortField:sortField,sortType: 1};
      this.sort_value = -1
    }
    this.getDeposits();
  }

  getDeposits() {
    this.processing = 1;
    this.loader = 1;
    this.activatedRoute.params.subscribe((params: Params) => {
      this.currency = params['currency'];
      this.status = params['status'];
      // this.p = 1
      this._dataService.getDeposits(this.currency, this.status, this.payU, this.withdrawal_type, this.p, this.sort, this.model.search)
        .subscribe(res => {
          console.log(res)
          this.loader = 0;
          this.processing = 0;
          this.selectedIndex = null;
          this.currency_records = res.depositInr;
          this.count = res.count;
          this.disableButton = true;
        });
    });
  }

  pay(value) {
    this.payU = value
    this.getDeposits();
  }
  externalSearch(value) {
    this.withdrawal_type = value
    // this.model.search = value;
    this.getDeposits();
  }


  pageChanged(p) {
    this.p = p
    this.getDeposits();
    this.UncheckAll();
    this.inrdiposit.length = 0;
  }
  randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 3; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  exportToExcel() {
    this.inrdiposit.forEach(function (element) {
      if (element.status === 0) {
        _.assign(element, { 'status': 'pending' });

      } else if (element.status === 1) {
        _.assign(element, { 'status': 'completed' });

      } else if (element.status === 2) {
        _.assign(element, { 'status': 'verified' });

      }
    })
    this.excelService.exportAsExcelFile(this.inrdiposit, 'inr-deposit' + '_' + this.inrdiposit[0].status + '_' + this.randomName());
  }
  check(option, bankDetails, state, e) {
    if (option.state) {
      option['Date'] = moment(option.created).format("DD/MM/YYYY");
      option['registered_name'] = bankDetails.registered_name;
      option['account_number'] = bankDetails.account_number;
      option['ifsc_code'] = bankDetails.ifsc_code;
      option['state'] = true;
      delete option._id;
      delete option.__v;
      this.inrdiposit.push(option);
      _.uniqBy(this.inrdiposit, 'client_id');
      console.log("this.inrdiposit", this.inrdiposit);
    } else {
      console.log("unchek-this.inrdiposit", this.inrdiposit);
      for (var i = 0; i < this.inrdiposit.length; i++) {
        console.log("state is", this.inrdiposit[i].state);
        if (this.inrdiposit[i].state === false) {
          _.remove(this.inrdiposit, { client_id: option.client_id });
        }
      }
    }
  }
  checkAll(ev) {
    this.emptyArray.length = 0;
    this.inrdiposit = this.currency_records;
    var checkedList = {};
    if (ev.target.checked) {
      console.log("all checked");
      this.inrdiposit.forEach(element => {
        element.state = ev.target.checked;
        checkedList['_id'] = element._id;
        checkedList['date'] = element.created;
        checkedList['client_id'] = element.client_id;
        checkedList['txid'] = element.txid;
        checkedList['bank'] = element.bank;
        checkedList['state'] = element.state;
        if (element.status === 0) {
          checkedList['kyc_status'] = "Pending";
        }
        if (element.status === 1) {
          checkedList['kyc_status'] = "Complete";
        }
        if (element.status === 2) {
          checkedList['kyc_status'] = "Canceled";
        }
        if (element.bankDetails.length > 0) {
          element.bankDetails.forEach(balKey => {
            checkedList['registered_name'] = balKey.registered_name;
            checkedList['account_number'] = balKey.account_number;
            checkedList['ifsc_code'] = balKey.ifsc_code;
          });
        }
        this.emptyArray.push(checkedList);
        checkedList = {};
      });
      this.inrdiposit = this.emptyArray;
      _.uniqBy(this.inrdiposit, 'client_id');
    } else {
      console.log("all unchecked");
      this.inrdiposit.forEach(x => {
        console.log("Uncheckall", x.state = ev.target.checked);
        x.state = ev.target.checked
      })
    }
  }
  isAllChecked() {
    return this.inrdiposit.every(_ => _.state);
  }
  buttonState() {
    return !this.inrdiposit.some(_ => _.state);
  }
  cleanupDepositArr() {
    this.inrdiposit.length = 0;
  }

  searchResult($event) {
    if ($event.which == 13) {
      document.getElementById("searchDataDeposits").click();
    }
    if (this.model.search == '') {
      this.p = 1;
      this.getDeposits();
    }
  }

  searchResultData() {
    this.p = 1;
    this.getDeposits();
  }
  UncheckAll() {
    var w = document.getElementsByTagName('input');
    for (var i = 0; i < w.length; i++) {
      if (w[i].type == 'checkbox') {
        w[i].checked = false;
      }
    }
  }
}