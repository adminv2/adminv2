import { TestBed, inject } from '@angular/core/testing';
import { UserProfile } from './user.service';

describe('UserProfile', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserProfile]
    });
  });

  it('should be created', inject([UserProfile], (service: UserProfile) => {
    expect(service).toBeTruthy();
  }));
});
