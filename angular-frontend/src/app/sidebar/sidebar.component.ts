import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  useraccess: any;
  user: any;
  trade: any;
  model: any;
  client_search_message: any;
  enableSearchButton: number;

  currencies : any;
  search_loader : number
  // constructor() { }
  constructor(private router: Router, public _dataService: DataService) { }

  ngOnInit() {
    this.search_loader = 0
    this.client_search_message = '';
    this.enableSearchButton = 0

    this.model = { search: '' }
    this.useraccess = JSON.parse(localStorage.getItem('useraccess'));

    this.user = JSON.parse(localStorage.getItem('current'));

    this.getCurrenciesDetails();

  }

  getCurrenciesDetails(){
    this._dataService.getCurrencyPairs()
    .subscribe(res => {
      this.trade = res.currencies_details.trade_pairs;
      this.currencies = res.currencies_details.currencies;
    })
  }
  
  searchByCI(CI) {
    this.search_loader = 1
    this._dataService.checkUserExists(CI)
      .subscribe(res => {
        this.search_loader = 0
        if (res.status == 200) {
          this.client_search_message = "";
          this.router.navigate(['/userProfile/' + CI])
        } else {
          this.enableSearchButton = 0;
          this.client_search_message = "Invalid Client Id";
          console.log("Invalid client id");
        }
      });
  }

  searchResult($event, searchedfor) {
    if (searchedfor.length > 3) {
      this.enableSearchButton = 1
      console.log("seearchs", searchedfor);
    } else {
      this.enableSearchButton = 0
    }
    if ($event.which == 13) {
      document.getElementById("searchData").click();
    }
    if (this.model.search == '') {
    }
  }

}
