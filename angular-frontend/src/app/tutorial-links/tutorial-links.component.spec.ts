import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorialLinksComponent } from './tutorial-links.component';

describe('TutorialLinksComponent', () => {
  let component: TutorialLinksComponent;
  let fixture: ComponentFixture<TutorialLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorialLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorialLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
