import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { AdminDetails } from '../admin.service';
import { ExcelService } from '../excel.service';

@Component({
  selector: 'app-tutorial-links',
  templateUrl: './tutorial-links.component.html',
  styleUrls: ['./tutorial-links.component.css']
})
export class TutorialLinksComponent implements OnInit {
  trade_tutorials: any;
  kyc : any;

  count: number = 0;
  disableButton: boolean = true;
  
  message: any;

  tutorial: any;

  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _adminService: AdminDetails, private excelService: ExcelService) {
    // this.model = {search:''};
    // this.excelService = excelService;
  }
  ngOnInit() {

    this.getTutorialLinks();
    this.defineVariables();
  }

  getTutorialLinks() {
    this._adminService.getTutorialLinks()
      .subscribe(res => {
        this.disableButton = true;
        // this.trade_tutorials = res.tutorials.trade;
        this.trade_tutorials = res.tutorials.trade;
        this.kyc = res.tutorials.kyc;
        this.count = res.count;
      });
  }

  addLinks() {
    document.getElementById("mymodelbutton").click();
  }

  updateTutorial(i, type){
    console.log("i isss", i);
    document.getElementById("mymodelbutton").click();
    this._adminService.getTutorialDetails(i, type)
    .subscribe(res => {
      // this.getTutorialLinks();
    });
  }

  changeStatus(id, status, type) {
    this.disableButton = false;
    this._adminService.changeStatus(id, status, type)
      .subscribe(res => {
        this.getTutorialLinks();
      });
  }

  // updateCompanyBank(id) {
  //   this.message = '';
  //   this.loadBankedit = 1;
  //   this.toEdit = id;
  //   this._adminService.getBankDetails(id)
  //     .subscribe(res => {
  //       this.loadBankedit = 0;
  //       this.addbank = {
  //         beneficiary: res.bankD.beneficiary,
  //         ifsc: res.bankD.ifsc,
  //         bankName: res.bankD.bankName,
  //         account: res.bankD.account,
  //         country: res.bankD.country,
  //         _id: res.bankD._id
  //       };
  //     });

  //   document.getElementById("mymodelbutton").click();
  // }

  defineVariables() {
    this.tutorial = {
      id:'',
      type: '',
      title: '',
      link: '',
      description: ''
    }
  }
  saveTutorial(tutorialData) {
    this._adminService.saveTutorial(tutorialData)
      .subscribe(res => {
        this.disableButton = true;
        this.message = res.message;
        setTimeout(function () {
          document.getElementById("mymodelbutton").click();
        }, 3000)
        this.defineVariables();
        this.getTutorialLinks();

      });
  }

  // editBankDetails(bankData) {
  //   this._adminService.editCompanyBank(bankData)
  //     .subscribe(res => {
  //       this.disableButton = true;
  //       this.message = res.message;
  //       setTimeout(function () {
  //         document.getElementById("mymodelbutton").click();
  //       }, 3000)
  //       this.defineVariables();
  //       this.getTutorialLinks();
  //     });
  // }

  

}
