// created by satish date:3/26/2018
import { DataService } from '../data.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ChangeDetectionStrategy, Component, Input, OnInit } from "@angular/core";
import { ExcelService } from '../excel.service';
import * as _ from "lodash";
import { element } from 'protractor';
@Component({
  selector: 'app-balances',
  templateUrl: './balances.component.html',
  styleUrls: ['./balances.component.css']
})
export class BalancesComponent implements OnInit {
  users_balance: any;
  currencies: any;
  currency: any;
  cars = new Array<any>();
  emptyArray = new Array<any>();
  public show: boolean = false;
  pageNo: number = 1;
  status: any;
  loading: any;
  model:any;
  count: number = 0;
  disableButton: boolean = true;
  idStatusSelected: number;
  
  p = 1;
  sort_value: number = -1;
  sort: any;

  processing:number = 1;

  constructor(private activatedRoute: ActivatedRoute, private _dataService: DataService,
    private router: Router, private _http: Http, private excelService: ExcelService) {
    this._dataService.authCheck('Balances');
    this.disableButton = true;
    this.excelService = excelService;
    this.model= {search:''};
    this.sort = {sortField:'referring_user',sortType:'-1'}
}

  sortByAmount(sortField) {
    if(this.sort_value == -1){
      this.sort = {sortField:sortField,sortType:-1};
      this.sort_value = 1
    }else{
      this.sort = {sortField:sortField,sortType: 1};
      this.sort_value = -1
    }
    this.getbal();
  }



  ngOnInit() {    
    this.model= {search:''};
    this.getbal();
  }
  getbal() {
    this.processing = 1;
    this._dataService.balanceList(this.pageNo, this.model.search,this.sort).subscribe(res => {
      
      this.processing = 0;
      this.users_balance = res.totalData;
      this.currencies = res.currencies;
      this.count = res.count;
    });
  }
  searchResult(){
    if(this.model.search == ''){
      this.getbal();
    }
  }
  searchResultData(){
    this.getbal();
  }
  loadPage(pageNo) {
    this.UncheckAll();
    this.pageNo = pageNo;
    this.emptyArray.length=0
    this.cars.length=0;
    this.getbal();
  }
  randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  exportToExcel() {
    var value= _.uniqBy(this.cars, 'client_id');
    this.excelService.exportAsExcelFile(value, 'balance' + '_' + this.randomName());
  }
  check(option, balances, state, e) {
    if (state) {
      balances.forEach(element => {
        if (element.currency === 'btc' && state) {
          option['btc'] = element.current_balance;
        }
        if (element.currency === 'btg' && state) {
          option['btg'] = element.current_balance;
        }
        if (element.currency === 'ltc' && state) {
          option['ltc'] = element.current_balance;
        }
        if (element.currency === 'xlm' && state) {
          option['xlm'] = element.current_balance;
        }
        if (element.currency === 'xrp' && state) {
          option['xrp'] = element.current_balance;
        }
        if (element.currency === 'bch' && state) {
          option['bch'] = element.current_balance;
        }
        if (element.currency === 'inr' && state) {
          option['inr'] = element.current_balance;
        }
        if (element.currency === 'thb' && state) {
          option['thb'] = element.current_balance;
        }
      });
      this.cars.push(option);
      this.cars = _.uniqBy(this.cars, '_id');
    } else {
      for (var i = 0; i < this.cars.length; i++) {
        if (this.cars[i].state === true) {
          _.remove(this.cars, { client_id: option.client_id });
        }
      }
      console.log(this.cars);
    }
  }
  checkAll(ev) {
    this.cars = this.users_balance;
    var checkedList = {};
     if (ev.target.checked) {
      this.cars.forEach(element => {
        element.state = ev.target.checked;
        checkedList['client_id'] = element.client_id;
        checkedList['email'] = element.email;
        checkedList['full_name'] = element.full_name;
        checkedList['state'] = true;
        if (element.balances.length > 0) {
          element.balances.forEach(balKey => {
            if (balKey.currency === 'btc' && ev.target.checked) {
              checkedList['btc'] = balKey.current_balance;
            }
            if (balKey.currency === 'btg' && ev.target.checked) {
              checkedList['btg'] = balKey.current_balance;
            }
            if (balKey.currency === 'ltc' && ev.target.checked) {
              checkedList['ltc'] = balKey.current_balance;
            }
            if (balKey.currency === 'xlm' && ev.target.checked) {
              checkedList['xlm'] = balKey.current_balance;
            }
            if (balKey.currency === 'xrp' && ev.target.checked) {
              checkedList['xrp'] = balKey.current_balance;
            }
            if (balKey.currency === 'bch' && ev.target.checked) {
              checkedList['bch'] = balKey.current_balance;
            }
            if (balKey.currency === 'inr' && ev.target.checked) {
              checkedList['inr'] = balKey.current_balance;
            }
            if (balKey.currency === 'thb' && ev.target.checked) {
              checkedList['thb'] = balKey.current_balance;
            }
          });
        }
        this.emptyArray.push(checkedList);
        checkedList = {};
      });
      this.cars = this.emptyArray;
      _.uniqBy(this.cars, 'client_id');
    } else {
       this.cars.forEach(x => {
         console.log("Uncheckall", x.state = ev.target.checked);
         x.state = ev.target.checked
        })
    }

  }
  isAllChecked() {
    return this.cars.every(_ => _.state);
  }

  buttonState() {
    return !this.cars.some(_ => _.state);
  }
  UncheckAll() {
    var w = document.getElementsByTagName('input');
    for (var i = 0; i < w.length; i++) {
      if (w[i].type == 'checkbox') {
        w[i].checked = false;
      }
    }
  }
  


  // searchResult($event){
  //   if($event.which == 13){
  //     document.getElementById("searchData").click();
  //   }
  //   if(this.model.search == ''){
  //     this.p = 1;
  //     this.getbal();
  //   }
  // }
  
  // searchResultData(){
  //   this.p = 1;
  //   this.getbal();
  // }
}

