import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyinterestsAddComponent } from './dailyinterests-add.component';

describe('DailyinterestsAddComponent', () => {
  let component: DailyinterestsAddComponent;
  let fixture: ComponentFixture<DailyinterestsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyinterestsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyinterestsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
