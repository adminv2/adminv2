import { Component, OnInit, Input, Pipe, PipeTransform, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../data.service';
import { FormGroup, FormControl, Validators, FormsModule, NgForm } from '@angular/forms';
import * as moment from 'moment'


@Component({
  selector: 'app-dailyinterests-add',
  templateUrl: './dailyinterests-add.component.html',
  styleUrls: ['./dailyinterests-add.component.css']
})
export class DailyinterestsAddComponent implements OnInit {
  deposit: any;
  message: any;
  disableButton: boolean = true;
  full_name: any;
  isClientIdValid = false;
  useraccess: any;

  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _dataService: DataService, private router: Router ) {
    this.useraccess = JSON.parse(localStorage.getItem('useraccess'));
  }

  @ViewChild('depositForm') depositForm: NgForm

  ngOnInit() {
    this.defineVariables();
  }

  addDailyInterst(data) {

    data.date = new Date();

    if (data.client_id == ''|| data.amount == '') {
      this.disableButton = true;
      this.message = 'Please Submit All the fields before save';
    } else {
      this.disableButton = false;
      this._dataService.saveDailyInterest(data)
        .subscribe(res => {
          this.disableButton = true;
          this.message = res.message;
          this.depositForm.reset()
          this.defineVariables();
        });
    }
  }
  defineVariables() {
    this.full_name = '';
    this.deposit = { client_id: '', amount: ''};
  }
  getclientName(client_id) {
    this._dataService.getclientName(client_id)
      .subscribe(res => {
        if (res.data) {
          this.disableButton = true;
          this.full_name = res.data.full_name;
          this.deposit.client_id = client_id;
          this.isClientIdValid = true
        } else {
          console.log("empty ho gaya");
          this.disableButton = true;
          this.isClientIdValid = false
          // this.deposit.client_id = '';
          this.full_name = 'This user does not Exists';
        }
      });
  }
}