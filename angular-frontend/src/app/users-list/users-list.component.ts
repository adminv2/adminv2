import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserProfile } from '../user.service';
import { ExcelService } from '../excel.service';
import * as _ from "lodash";
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  usersList: any;
  model: any;
  userList: any;
  count: number = 0;
  isVerified: number = 1;
  p: number = 1;
  emptyArray = new Array<any>();
  loader = 1;
  userListArr = new Array<any>();
  disableButton: boolean = true;
  suspend:any;
  suspend_changed_by: any;
  user: any;
  suspention :any;
  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _userService: UserProfile,private excelService: ExcelService) {
     this.model = {search:''};
     this.excelService = excelService;
     
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('current'));

    this.suspend = {suepention_reason : ''}
    this.suspention = { suspended : '', client_id : ''}

    this.getUsers();
  }
  verfied(value) {
    this.isVerified = value
    this.getUsers();
    this.userListArr.length=0;
  }
  getUsers() {
    this.loader = 1;
    this._userService.getUsersList(this.isVerified, this.p,this.model.search)
      .subscribe(res => {
        console.log(res.users_doc)
        this.loader = 0;

        this.usersList = res.users_doc;
        this.count = res.count;
      });
  }
  pageChange(p) {
    this.p = p;
    this.getUsers();
    this.userListArr.length=0;
  }

  searchResult(){
    if(this.model.search == ''){
      this.p = 1;
      this.getUsers();
    }
  }

  changeStatus(id, status, doc_type, reason = null) {
    this.disableButton = false;
    this.suspend_changed_by = this.user.f_name + ' '+ this.user.l_name;

// console.log(status);

      this._userService.changeUsersStatus(id, status, doc_type, reason, this.suspend_changed_by)
        .subscribe(res => {
          this.disableButton = true;
          if (res.status == 200) {
            document.getElementById("suspendmodelbutton").click();

            this.getUsers();
          }
        });
  }


  searchResultData(){
    this.p = 1;
    this.getUsers()
  }
  randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  exportToExcel() {
    this.excelService.exportAsExcelFile(this.userListArr, 'users-list' + '_' + this.randomName());
  }
  check(option,state, e) {
    if(state){
      this.userListArr.push(option);
      this.userListArr= _.uniqBy(this.userListArr, '_id');
    } else {
      console.log("uncheck",this.userList);
      for(var i=0;i<this.userList.length;i++){
         if(this.userList[i].state === true){
            _.remove(this.userList, {"Client ID": option.client_id});
         }
      }
   }
 }
  checkAll (ev) {
    this.emptyArray.length=0;
    this.userList = this.usersList;
    var checkedList = {};
     if (ev.target.checked) {
      this.userList.forEach(element => {
        element.state = ev.target.checked;
        checkedList['ID'] = element.id;
        checkedList['Client ID'] = element.client_id;
        checkedList['Full Name'] = element.full_name;
        checkedList['Phone'] = element.mobile_number;
        checkedList['Email'] = element.email;
        checkedList['state'] = element.state;
        if(element.isVerified){
          checkedList['isVerified'] = "Verified";
        } else {
          checkedList['isVerified'] = "NotVerified";
        }
        if(element.kyc_status===0){
          checkedList['kyc_status'] = "Unverified";
        }
        if(element.kyc_status===1){
          checkedList['kyc_status'] = "Verified";
        }
        if(element.kyc_status===2){
          checkedList['kyc_status'] = "Rejected";
        }
        checkedList['Country'] = element.country;
        this.emptyArray.push(checkedList);
        checkedList = {};
      });
       this.userList= this.emptyArray;
      console.log("this.userList",this.userList);
      _.uniqBy(this.userList, 'client_id');
    } else {
       this.userList.forEach(x => {
         console.log("Uncheckall", x.state = ev.target.checked);
         x.state = ev.target.checked
        })
    }

    this.userList.forEach(x => x.state = ev.target.checked)
  }
  isAllChecked() {
    return this.userListArr.every(_ => _.state);
  }
  buttonState() {
    return !this.userListArr.some( _=>_ .state);
  }
  UncheckAll() {
    var w = document.getElementsByTagName('input');
    for (var i = 0; i < w.length; i++) {
      if (w[i].type == 'checkbox') {
        w[i].checked = false;
      }
    }
  }
  

  suspendUserPopup(client_id, status, suspend){
    this.suspention = { suspended : status, client_id : client_id}


    document.getElementById("suspendmodelbutton").click();
  }

}
