import { Component, OnInit, ElementRef } from '@angular/core';
import { Http, Response } from '@angular/http';
import { FileUploader } from 'ng2-file-upload';
import "rxjs/add/operator/do";
import { DataService } from '../data.service';
import "rxjs/add/operator/map";
import { baseUrl } from '../app.config';
import { ExcelService } from '../excel.service';
import * as _ from "lodash";
@Component({
    selector: 'app-file-upload',
    templateUrl: './file-upload.component.html',
    styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
    model:any;
    filesData = new Array<any>();
    cars = new Array<any>();
    emptyArray = new Array<any>();
    IShow:Boolean=true;   
    public uploader: FileUploader = new FileUploader({ url: baseUrl+"file/upload" });

    constructor(private http: Http, private el: ElementRef, private _dataService: DataService,private excelService: ExcelService) {
        this._dataService.authCheck('WithdrawINR');
        this.excelService = excelService;
    }

    ngOnInit() {
        this.IShow=false;
        this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            var SomeData=JSON.parse(response);
            console.log(SomeData)
            this.IShow=true,
            this.filesData=SomeData.data;
         };
    }
    upload() {
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
        let fileCount: number = inputEl.files.length;
        let formData = new FormData();
        if (fileCount > 0) {
            formData.append('photo', inputEl.files.item(0));
            this.http .post(baseUrl+"file/upload", formData).map((res: Response) => res.json()).subscribe(
                    (success) => {
                        console.log(success._body);
                    },
                    (error) => alert(error))
        }
    }
    randomName() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
      }
      exportToExcel() {
        this.excelService.exportAsExcelFile(this.emptyArray, 'Payment' + '_' + this.randomName());
      }
      check(option,state, e) {
         if(state){
          this.emptyArray.push(option);
          this.emptyArray= _.uniqBy(this.emptyArray, 'client_id');
        } else {
          for(var i=0;i<this.emptyArray.length;i++){
            console.log(this.emptyArray[i].state);
             if(this.emptyArray[i].state === true){
                 _.remove(this.emptyArray, {Client_id: option.client_id});
               
             }
          }
       }
     }
      checkAll (ev) {
        this.emptyArray=this.filesData;
        this.cars.length=0;
        var checkedList={};
        if (ev.target.checked) {
         this.emptyArray.forEach(element => {
            element.state = ev.target.checked;
            checkedList['Client_id'] = element.client_id;
            checkedList['Past Balance'] = element.pastValue;
            checkedList['Adding Balance'] = element.addingbal;
            checkedList['Total Amount'] = element.Update_balance;
            checkedList['state'] = true;
            this.cars.push(checkedList);
            checkedList = {};
          });
            this.emptyArray = this.cars;
          _.uniqBy(this.emptyArray, 'client_id');
        } else {
          this.emptyArray.forEach(x => x.state = ev.target.checked)
        
        }
    
      }
      isAllChecked() {
        return this.emptyArray.every(_ => _.state);
      }
    
      buttonState() {
        return !this.emptyArray.some( _=>_ .state);
      }
      UncheckAll() {
        var w = document.getElementsByTagName('input');
        for (var i = 0; i < w.length; i++) {
          if (w[i].type == 'checkbox') {
            w[i].checked = false;
          }
        }
      }
    }

