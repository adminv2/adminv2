import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InrWithdrawalComponent } from './inr-withdrawal.component';

describe('InrWithdrawalComponent', () => {
  let component: InrWithdrawalComponent;
  let fixture: ComponentFixture<InrWithdrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InrWithdrawalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InrWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
