import { Component, OnInit } from '@angular/core';
import { WithdrawService } from '../withdraw.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
//import { baseUrl } from '../app.config';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { ExcelService } from '../excel.service';
import * as _ from "lodash";
import * as moment from 'moment';

@Component({
  selector: 'app-inr-withdrawal',
  templateUrl: './inr-withdrawal.component.html',
  styleUrls: ['./inr-withdrawal.component.css'],
})

export class InrWithdrawalComponent implements OnInit {
  public users_withdrawa: any;
  message: any;
  count: number = 0;
  public show: boolean = false;
  rowId: any;
  currency: any;
  status: any;
  model: any;
  p: number = 1;
  disableButton: boolean = true;
  idStatusSelected: number;
  withdraw: any;
  useraccess: any;
  loader:number = 0;
  sort_value: number = -1;
  sort: any;
  user: any;

  withdrawal_type: number = 0;

  inrwithdraw = new Array<any>();
  constructor(private activatedRoute: ActivatedRoute, private _withdrawService: WithdrawService, private _dataService: DataService, private excelService: ExcelService,
    private router: Router, private _http: Http) {
    this.useraccess = JSON.parse(localStorage.getItem('useraccess'));
    this._dataService.authCheck('WithdrawINR');
    this.model= {search:''}
    this.sort = {sortField:'created',sortType:'-1'}

    this.withdraw = {txnId:''}
    this.excelService = excelService;
    if (this.useraccess.WithdrawINR == 1) {
      this.router.navigate(['/']);
    }
  }


  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('current'));

    //this.p = 1
    this.loader = 1;  
    this.withdraw = { txnId: '' };
    this.activatedRoute.params.subscribe((params: Params) => {
      this.currency = params['currency'];
      this.status = params['status'];
      //console.log('ankur'+this.p)
      this.p = 1
      this._withdrawService.getwithdraw(this.currency, this.status,this.p,this.withdrawal_type,this.sort,this.model.search).subscribe(res => {
        this.loader = 0;
        //console.log(res.users_withdrawa)
        this.users_withdrawa = res.users_withdrawa;
        this.count = res.count;
      });
    });
  }


  externalSearch(value) {
    this.withdrawal_type = value
    // this.model.search = value;
    this.getwithdrawal();
  }

  sortByAmount(sortField) {
    if(this.sort_value == -1){
      this.sort = {sortField:sortField,sortType:-1};
      this.sort_value = 1
    }else{
      this.sort = {sortField:sortField,sortType: 1};
      this.sort_value = -1
    }
    this.getwithdrawal();
  }



  id_status_toggle(id) {
    if (this.idStatusSelected == id) {
      this.idStatusSelected = null;
    } else {
      this.idStatusSelected = id;
      this.show = !this.show;
    }
  }

  changeWithdrawlStatus(id,status,currency,txnId) {
    this.disableButton = false;
    this._withdrawService.changeWithdrawStatus(id,status,currency,txnId,this.user._id)
      .subscribe(res => {
        if (res.code == 152) {
          this.message = 'Updated Successfully';
        } else if (res.code == 154) {
          this.message = 'Faild, User Not Found';
        }
        else {
          this.message = 'Faild, Please Try Again';
        }
        this.withdraw.txnId = '';
        this.disableButton = true;
        document.getElementById("close_withdrawal_model").click();
        this.getwithdrawal();
      });
  }

  changeStatus(id, status, currency) {
    this.disableButton = true;
    if (status == 1) {
       this.rowId = id;
       document.getElementById("mymodelbutton").click();
    } else {
      this.changeWithdrawlStatus(id,status,currency,"Waiting for approval");
    }
  }


  
  getwithdrawal() {
    this.loader = 1;
    this.idStatusSelected = null;
    this.activatedRoute.params.subscribe((params: Params) => {
      this.currency = params['currency'];
      this.status = params['status'];
      this._withdrawService.getwithdraw(this.currency, this.status,this.p,this.withdrawal_type,this.sort,this.model.search).subscribe(res => {
        if(res){
          this.users_withdrawa = res.users_withdrawa;
          this.count = res.count;
        }else{
          this.users_withdrawa = [];
          this.count = 0;
        }
        this.loader = 0;
        this.message = '';
      });
    });
  }
  pageChanged(p) {
    this.p = p

    this.getwithdrawal()
  }

  searchResult($event) {
    if ($event.which == 13) {
      document.getElementById("clicked_to_search").click();
    }
    if (this.model.search == '') {
      this.p = 1;
      this.getwithdrawal();
    }
  }

  // searchResult(){
  //   if(this.model.search == ''){
  //     this.getwithdrawal();
  //   }
  // }
  
  searchResultData(){
    this.getwithdrawal();
  }
  randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  exportToExcel() {
    this.excelService.exportAsExcelFile(this.inrwithdraw, 'inr-withdrawal' + '_' + this.randomName());
  }
  check(option, bankDetails, state, e) {
    if (option.state) {
      option['Date'] = moment(option.created).format("DD/MM/YYYY");
      option['registered_name'] = bankDetails.registered_name;
      option['account_number'] = bankDetails.account_number;
      option['ifsc_code'] = bankDetails.ifsc_code;
      delete option._id;
      delete option.otp;
      delete option.__v;
      this.inrwithdraw.push(option);
      this.inrwithdraw = _.uniqBy(this.inrwithdraw, 'client_id');
    } else {
      for (var i = 0; i < this.inrwithdraw.length; i++) {
        if (this.inrwithdraw[i].state === false) {
          _.remove(this.inrwithdraw, { state: false });
        }
      }
    }
  }
  checkAll(ev) {
    this.inrwithdraw=this.users_withdrawa;
    this.inrwithdraw.forEach(x => x.state = ev.target.checked)
  }
  isAllChecked() {
    return this.inrwithdraw.every(_ => _.state);
  }
  buttonState() {
    return !this.inrwithdraw.some(_ => _.state);
  }

}
