import { Component, OnInit, Input, Pipe, PipeTransform, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../data.service';
import { FormGroup, FormControl, Validators, FormsModule, NgForm } from '@angular/forms';
import * as moment from 'moment'


import { AdminDetails } from '../admin.service';
import { ExcelService } from '../excel.service';
@Component({
  selector: 'app-deposit-manual',
  templateUrl: './deposit-manual.component.html',
  styleUrls: ['./deposit-manual.component.css']
})
export class DepositManualComponent implements OnInit {
  deposit: any;
  message: any;
  disableButton: boolean = true;
  full_name: any;
  isClientIdValid = false;
  useraccess: any;
  currencies: any;
  user: any;

  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _dataService: DataService, private router: Router ) {
    this.useraccess = JSON.parse(localStorage.getItem('useraccess'));
  }

  @ViewChild('depositForm') depositForm: NgForm

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('current'));

    this.defineVariables();
    this.getCurrencyDetails();
  }

  getCurrencyDetails(){
    this._dataService.getCurrencyPairs()
    .subscribe(res => {
      this.currencies = res.currencies_details.currencies;
    })
  }


  addManualDeposit(data) {
    // console.log(data.transaction_date);
    var dd = data.transaction_date;
    data.transaction_date = moment(dd).format('YYYY-MM-DD');
    data.changed_by = this.user._id

    if (data.client_id == '' || data.currency == '' || data.amount == '' || data.txid == '' || data.created) {
      this.disableButton = true;
      this.message = 'Please Submit All the fields before save';
    } else {
      this.disableButton = false;
      this._dataService.saveManualDeposit(data)
        .subscribe(res => {
          this.disableButton = true;
          this.message = res.message;
          this.depositForm.reset()
          this.defineVariables();
        });
    }
  }
  defineVariables() {
    this.full_name = '';
    this.deposit = { client_id: '', currency: '', amount: '', txid: '', created: '', bank: '', comments: '', status: '' };
  }
  getclientName(client_id) {
    this._dataService.getclientName(client_id)
      .subscribe(res => {
        if (res.data) {
          this.disableButton = true;
          this.full_name = res.data.full_name;
          this.deposit.client_id = client_id;
          this.isClientIdValid = true
        } else {
          console.log("empty ho gaya");
          this.disableButton = true;
          this.isClientIdValid = false
          // this.deposit.client_id = '';
          this.full_name = 'This user does not Exists';
        }
      });
  }
}
