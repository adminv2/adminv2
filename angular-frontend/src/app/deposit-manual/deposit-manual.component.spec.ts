import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositManualComponent } from './deposit-manual.component';

describe('DepositManualComponent', () => {
  let component: DepositManualComponent;
  let fixture: ComponentFixture<DepositManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
