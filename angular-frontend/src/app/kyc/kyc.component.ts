

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.component.html',
  styleUrls: ['./kyc.component.css']
})
export class KycComponent implements OnInit {
  users_docs: any;
  display: any;
  loadImge: any;
  model: any;
  doc_detail_name: any;
  p: number = 1;
  count: number = 0;
  loader = 1;
  isVerified: number = 0;
  country: any;

  disableButton: boolean = true;
  kyc: any;

  id: any;
  status: any;
  doc_type: any;
  reason: any;

  public show: boolean = false;
  selectedIndex: number;
  idStatusSelected: number;
  bankStatusSelected: number;
  usd_inr: any;
  // constructor(public _dataService: DataService) {
  //   this.model = { search: '' }
  // }

  useraccess: any;
  user: any;

  constructor(private activatedRoute: ActivatedRoute, private _dataService: DataService,
    private router: Router) {
    this.useraccess = JSON.parse(localStorage.getItem('useraccess'));
    this._dataService.authCheck('Documents');
    this.model = { search: '' }
    if (this.useraccess.Documents == 1) {
      this.router.navigate(['/']);
    }
  }


  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('current'));

    this.country = 'all';

    this.getKyc();
    this.defineVariable();
    // this.getInrOfUsd();
  }
  getInrOfUsd() {
    this._dataService.getInrOfUsd()
      .subscribe(res => {
        this.usd_inr = res.rates.INR;
        // console.log("result is", res.rates.INR);
      });
  }
  defineVariable() {
    this.kyc = { reason: '' };
  }

  toggle(id) {
    if (this.selectedIndex == id) {
      this.selectedIndex = null;
    } else {
      this.selectedIndex = id;
      this.show = !this.show;
    }
  }
  id_status_toggle(id) {
    if (this.idStatusSelected == id) {
      this.idStatusSelected = null;
    } else {
      this.idStatusSelected = id;
      this.show = !this.show;
    }
  }
  Banktoggle(id) {
    console.log(id);
    if (this.bankStatusSelected == id) {
      this.bankStatusSelected = null;
    } else {
      this.bankStatusSelected = id;
      this.show = !this.show;
    }
  }

  changeStatus(id, status, doc_type, reason) {
    this.id = id;
    this.status = status;
    this.doc_type = doc_type;
    this.reason = reason;

    this.disableButton = false;
    if (status == 2) {
      this.disableButton = true;
      document.getElementById("kyc_message").click();
    } else {
      this._dataService.changeDocumentStatus(id, status, doc_type, this.reason = null, this.user._id)
        .subscribe(res => {
          this.disableButton = true;
          if (res.msg == "success") {
            this.getKyc();
          }
          this.users_docs = res.users_doc;
        });
    }
  }

  // payJoiningBonus(client_id) {
  //   this._dataService.payJoiningBonus(client_id, this.usd_inr)
  //     .subscribe(res => {
  //       console.log("in .ts result is", res);

  //     });
  // }

  changeKycStatus(reason) {
    this.disableButton = false;
    this._dataService.changeDocumentStatus(this.id, this.status, this.doc_type, reason, this.user._id)
      .subscribe(res => {
        this.disableButton = true;
        if (res.msg == "success") {
          document.getElementById("kyc_message").click();
          this.getKyc();
        }
        this.users_docs = res.users_doc;
      });
  }

  loadImg(img, doc_detail) {
    document.getElementById("imgbutton").click();
    this.loadImge = img;
    this.doc_detail_name = doc_detail;
  }

  verfied(value) {
    console.log(value);
    this.isVerified = value
    this.getKyc();
  }

  SortBycountry(name) {
    this.country = name;
    this.getKyc();
  }

  getKyc() {
    this.loader = 1;
    this._dataService.getKyc(this.isVerified, this.p, this.model.search, this.country)
      .subscribe(res => {
        this.loader = 0;
        this.idStatusSelected = null;
        this.selectedIndex = null;
        this.bankStatusSelected = null;
        this.users_docs = res.users_doc;
        this.count = res.count;
      });
  }
  pageChange(p) {
    this.p = p;
    this.getKyc();
  }

  searchResult($event) {
    if ($event.which == 13) {
      document.getElementById("clicked_to_search").click();
    }
    if (this.model.search == '') {
      this.p = 1;
      this.getKyc();
    }
  }

  searchResultData() {
    this.p = 1;
    this.getKyc();

  }



}




