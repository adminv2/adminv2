import { Component, OnInit, Input, Pipe, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../data.service';
import { FormGroup, FormControl, Validators, FormsModule } from '@angular/forms';
import * as moment from 'moment'

import { WithdrawService } from '../withdraw.service';


@Component({
  selector: 'app-manual-withdrawal',
  templateUrl: './manual-withdrawal.component.html',
  styleUrls: ['./manual-withdrawal.component.css']
})
export class ManualWithdrawalComponent implements OnInit {
  withdrawal: any;
  message: any;
  disableButton: boolean = true;
  full_name: any;
  user :any;
  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _dataService: DataService, private _withdrawService: WithdrawService) {
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('current'));

    this.defineVariables();
  }

  addManualWithdrawal(data) { 
    var dd = data.transaction_date;
    data.transaction_date = moment(dd).format('YYYY-MM-DD');
    data.changed_by = this.user._id

    if (data.client_id == '' || data.currency == '' || data.amount == '' || data.created) {
      this.disableButton = true;
      this.message = 'Please Submit All the fields before save';
    } else {
      this.disableButton = false;
      this._withdrawService.saveManualWithdrawal(data)
        .subscribe(res => {
          this.disableButton = true;
          this.message = res.message;
          setTimeout(function () {
            window.location.reload();
          }, 2500);
        });
    }
  }
  defineVariables() {
    this.full_name = '';
    this.withdrawal = { client_id: '', currency: '', amount: '', created: '', bank: '', comments: ''};
  }
  getclientName(client_id) {
    this._dataService.getclientName(client_id)
      .subscribe(res => {
        if (res.data) {
          this.disableButton = true;
          this.full_name = res.data.full_name;
        } else {
          this.disableButton = false;
          this.withdrawal.client_id = '';
          this.full_name = 'This user does not Exists';
        }
      });
  }
}
