import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualWithdrawalComponent } from './manual-withdrawal.component';

describe('ManualWithdrawalComponent', () => {
  let component: ManualWithdrawalComponent;
  let fixture: ComponentFixture<ManualWithdrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualWithdrawalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
