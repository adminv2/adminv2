import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserProfile } from '../user.service';
import { DataService } from '../data.service';
import { WithdrawService } from '../withdraw.service';
import { DatePipe } from '@angular/common'
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']

})
export class UserProfileComponent implements OnInit {
  today = Date.now();
  fixedTimezone = '2015-06-15T09:03:01+0900';

  client_id: number;
  usersInfo: any;
  dailyInterest: any;

  send_mail: any;
  data: any = [];
  loader: any;

  imgUrl: any;
  basic_info: any;
  document_name: any;
  dataStatus = 0;

  count: any;
  p = 1;
  page_no = 1;
  sat = 1;

  dailyInterestPage: 1;
  currency_name: 'inr'; // used as default currency.
  status: any;
  currency_records: any;
  trade_records: any;
  notification: any;
  payU: number = 0;
  countDeposit: number = 0;
  countWithdrawal: number = 0;
  model: any;
  deposit: any;
  trade: any;
  withdrawal: any;
  deposit_loader: any;
  withdrawal_loader: any;
  edit_basic_info: number = 0;
  edit_bank_info: number = 0;
  withdrawal_type: number = 0;

  bank_info: any;
  withd: any;

  message: any;

  public users_withdrawa: any;

  user_referral: any;
  trade_loader: any;

  tradeCount: number = 0;
  show: number = 0;

  edit_aadhaar_number: number = 0;
  edit_pan_number: number = 0;
  edit_withdrawal_limit: number = 0;
  disableButton: boolean = true;
  disableEditBasicInfo: boolean = true;
  update_bal_currency: any = null;
  bal: any;
  old_balance: any;
  user: any;
  email_exists: any;

  sort: any;
  toSortBy: any;
  currencies: any;
  currencies_are: any;
  td: any;

  suspend: any;
  suspend_changed_by: any;

  show_suspend_block: any;
  searchByType: any;

  allEquivalent: any;
  estimatedBalance :any;
  // withdrawlas : any;

  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _dataService: DataService, private _withdrawService: WithdrawService, public _userService: UserProfile, public datepipe: DatePipe) {
    this.model = { search: '' }
    this.deposit = { currency_name: 'inr' }
    this.trade = { currency_pair: 'btc_inr' }
    this.td = { currency_pair: 'btc_inr' }
    this.withdrawal = { currency_name: 'inr' }
    this.edit_basic_info = 0;
  }
  ngOnInit() {
    this.suspend = { suepention_reason: '' }
    this.td = {}
    this.td = []
    this.show_suspend_block = 0;
    this.searchByType = 0;
    this.activatedRoute.params.subscribe((params: Params) => {
      this.client_id = params['client_id'];
    })
    // console.log("update balance is", this.update_bal);
    this.basic_info = { name: '', email: '', mobile_number: '' };
    this.bank_info = { client_id: '', registered_name: '', account_number: '', ifsc_code: '', bank_name: '', bank_address: '' }
    this.withd = { withdrawal_limit: '', withdrawal_limit_inr: '', withdrawal_progress: '', withdrawal_progress_inr: '' }
    this.count = 0;
    this.show = 0;
    this.sort = { sortField: 'created', sortType: '-1' }

    this.currency_name = 'inr';
    this.user = JSON.parse(localStorage.getItem('current'));


    this.getCurrenciesDetails();
    this.getEquivalentBalanceOf(this.client_id);

    this.getUsersData();

    this.getDailyInterest();
    this.getDeposits(this.currency_name);
    this.getWithdrawals(this.currency_name);
    this.getReferrals();

    this.getTrades(this.trade.currency_pair, this.client_id);
    this.getNotifications(this.client_id);


  }

  getEquivalentBalanceOf(client_id) {
    this._userService.getEquivalentBalanceOf(client_id)
      .subscribe(res => {
          this.estimatedBalance = res.total_balance;
          this.allEquivalent = res.data;

          // console.log(this.allEquivalent);

      })
  }


  changeStatus(id, status, doc_type, reason = null) {
    this.disableButton = false;
    this.suspend_changed_by = this.user.f_name + ' ' + this.user.l_name;
    this._userService.changeUsersStatus(id, status, doc_type, reason, this.suspend_changed_by)
      .subscribe(res => {
        this.disableButton = true;
        console.log("message iss", res.msg)
        if (res.msg == "success") {
          document.getElementById("suspendmodelbutton").click();
          this.ngOnInit();
        }
      });
  }

  ChangeGoogleAuthStatus(id, status, doc_type, reason = null) {
    this.disableButton = false;
    this.suspend_changed_by = this.user.f_name + ' ' + this.user.l_name;
    this._userService.ChangeGoogleAuthStatus(id, status, doc_type, reason, this.suspend_changed_by)
      .subscribe(res => {
        this.disableButton = true;
        if (res.msg == "success") {
          this.ngOnInit();
        }
      });
  }


  getCurrenciesDetails() {
    this._dataService.getCurrencyPairs()
      .subscribe(res => {
        this.trade = res.currencies_details.trade_pairs;
        this.currencies = res.currencies_details.currencies;
      })
  }


  getNotifications(client_id) {
    this._userService.getNotifications(client_id)
      .subscribe(res => {
        this.notification = res.notifications;
      });
  }
  updateBalance(currency, reset = null) {
    if (reset == 0) {
      this.show = 0;
      this.update_bal_currency = '';
      // this.bal.current_balance = this.old_balance;
    } else {
      this.show = 1;
      this.update_bal_currency = currency;
    }
  }
  showhideSuspend() {
    if (this.show_suspend_block == 0) {
      this.show_suspend_block = 1
    } else {
      this.show_suspend_block = 0
    }
  }
  saveUpdateBalance(balance) {
    this.disableButton = false;
    this._userService.updateBalance(balance, this.update_bal_currency, this.client_id, this.user._id)
      .subscribe(res => {
        this.disableButton = true;
        this.message = res.message;
        this.update_bal_currency = null
        this.show = 0;
        this.getUsersData();
      });
  }

  editInfo(field, to_close = null) {
    if (field == 'aadhaar') {
      this.edit_aadhaar_number = 1;
      if (to_close == 0) {
        this.edit_aadhaar_number = 0;
      }
    } else if (field == 'pan') {
      this.edit_pan_number = 1;
      if (to_close == 0) {
        this.edit_pan_number = 0;
      }
    }
  }



  sendMail(data) {
    var d = { 'client_id': this.client_id };
    data = Object.assign({}, ...data, d);
    this._userService.sendMail(data)
      .subscribe(res => {
        this.data = res.message;
      });
  }
  loadImgFromProfile(url, doc_name) {
    this.imgUrl = url;
    this.document_name = doc_name;
    document.getElementById("profileimgbutton").click();
  }

  pageChangedDailyInterest(p) {
    // this.p = p;
    this.dailyInterestPage = p;
    this.getDailyInterest();
  }
  pageChangedDeposits(p) {
    this.p = p;
    this.getDeposits(this.currency_name);
  }
  pageChanged(p, sat) {
    this.p = p;
    this.sat = sat;
    this.getDeposits(this.currency_name);
    this.getWithdrawals(this.currency_name);

  }
  loadPage(page) {

    this.page_no = page;
    this.getReferrals();
  }

  getDailyInterest() {
    // console.log("daily pagae is", this.dailyInterestPage);
    this._userService.getDailyInterestDetails(this.client_id, this.dailyInterestPage)
      .subscribe(res => {
        if (res.data) {
          this.loader = '0';
          this.dataStatus = 1;
          this.dailyInterest = res.data;
          this.count = res.count;
        } else {
          this.loader = '0';
          this.dataStatus = 0;
          this.dailyInterest = '';
        }
      });
  }

  getReferrals() {
    // console.log(this.page_no)
    // console.log(typeof this.page_no)
    this.model.search = this.client_id;
    this._dataService.getReferral(this.page_no, this.model.search).subscribe(res => {
      this.user_referral = res.data;
      this.currencies_are = res.currencies_are;

      this.count = res.count;
    });
  }

  getWithdrawals(currency_name) {
    this.withdrawal_loader = 1;
    // console.log("currency name is ", currency_name);
    this.currency_name = currency_name;
    if (currency_name == 'inr') {
      this.status = 'all';
    }
    this.model.search = this.client_id;

    this._withdrawService.getwithdraw(currency_name, this.status, this.p, this.withdrawal_type, this.sort, this.model.search).subscribe(res => {
      this.message = '';
      this.users_withdrawa = res.users_withdrawa;
      this.withdrawal_loader = 0;

      if (res.users_withdrawa == '') {
      } else {
        this.countWithdrawal = res.count;
      }
    });
  }

  getDeposits(currency_name) {
    this.deposit_loader = 1;
    this.currency_name = currency_name;
    if (currency_name == 'inr') {
      this.status = 'all';
    } else if (currency_name == 'xlm' || currency_name == 'xrp') {
      this.status = 'confirm';
    } else {
      this.status = 'confirm';
    }
    this.model.search = this.client_id;
    // this.p = 1
    this._dataService.getDeposits(this.currency_name, this.status, this.payU, this.withdrawal_type, this.p, this.sort, this.model.search)
      .subscribe(res => {
        this.deposit_loader = 0;
        this.currency_records = res.depositInr;
        if (res.count) {
          this.countDeposit = res.count;
        }
        else {
          this.countDeposit = 0;
        }
      });
  }

  updateKyc(document_name, val) {
    var updateKyc = {}
    if (document_name == "aadhaar_number") {
      updateKyc = {
        aadhaar_number: val,
        client_id: this.client_id
      }
    } else if (document_name == "id_number") {
      updateKyc = {
        id_number: val,
        client_id: this.client_id
      }
    }
    this._userService.updateKyc(updateKyc)
      .subscribe(res => {
        if (this.edit_aadhaar_number == 1) {
          this.edit_aadhaar_number = 0;
        } else if (this.edit_pan_number == 1) {
          this.edit_pan_number = 0;
        }
        this.message = res.message;
        this.getUsersData();
      });
  }

  getUsersData() {
    this.loader = '1';
    this.send_mail = { subject: '', message: '' };
    this.activatedRoute.params.subscribe((params: Params) => {
      this.client_id = params['client_id'];
      this._userService.getUserDetails(this.client_id)
        .subscribe(res => {
          // for (var i = 0; i < 10; i++) {
          //   console.log(typeof(res.data.balances[i].current_balance));
          // }

          if (res.data) {
            this.loader = '0';
            this.dataStatus = 1;
            this.usersInfo = res.data;
            this.basic_info = {
              aadhaar_number: res.data.aadhaar_number,
              id_number: res.data.id_number,
              client_id: res.data.client_id,
              full_name: res.data.full_name,
              email: res.data.email,
              mobile_number: res.data.mobile_number,
              country: res.data.country,
              date_of_birth: this.datepipe.transform(res.data.date_of_birth, 'yyyy-MM-dd')
            };
            this.bank_info = {
              registered_name: res.data.bank_details[res.data.bank_details.length - 1] ? res.data.bank_details[res.data.bank_details.length - 1].registered_name : '',
              account_number: res.data.bank_details[res.data.bank_details.length - 1] ? res.data.bank_details[res.data.bank_details.length - 1].account_number : '',
              ifsc_code: res.data.bank_details[res.data.bank_details.length - 1] ? res.data.bank_details[res.data.bank_details.length - 1].ifsc_code : '',
              bank_name: res.data.bank_details[res.data.bank_details.length - 1] ? res.data.bank_details[res.data.bank_details.length - 1].bank_name : '',
              bank_address: res.data.bank_details[res.data.bank_details.length - 1] ? res.data.bank_details[res.data.bank_details.length - 1].bank_address : '',
              client_id: res.data.client_id
            }

            this.withd = { withdrawal_limit: res.data.withdrawal_limit, withdrawal_limit_inr: res.data.withdrawal_limit_inr, withdrawal_progress: res.data.withdrawal_progress, withdrawal_progress_inr: res.data.withdrawal_progress_inr }


          } else {
            this.loader = '0';
            this.dataStatus = 0;
            this.usersInfo = '';
          }
        });
      // this.p = 1

    });
  }

  editBasicInfo(reset = null) {
    if (reset == 0) {
      this.edit_basic_info = 0;
    } else {
      this.edit_basic_info = 1;
    }
  }
  editBankInfo(reset = null) {
    if (reset == 0) {
      this.edit_bank_info = 0;
    } else {
      this.edit_bank_info = 1;
    }
  }
  editWithdrawal(reset = null) {
    if (reset == 0) {
      this.edit_withdrawal_limit = 0;
    } else {
      this.edit_withdrawal_limit = 1;
    }
  }



  saveBasicInfo(body) {
    body.updated_by = this.user._id;
    this._userService.updateUsersInfo(body)
      .subscribe(res => {
        this.edit_basic_info = 0;
        this.message = res.message;
        this.getUsersData();
      });
  }
  saveBankInfo(body) {
    body.updated_by = this.user._id
    this._userService.addBankInfo(body)
      .subscribe(res => {
        // console.log("ok");
        this.edit_bank_info = 0;
        this.message = res.message;
        this.getUsersData();
      });
  }
  saveWithdrawalLimtis(withd) {
    this.disableButton = false;
    var withdrawlas = {
      withdrawal_limit: withd.withdrawal_limit,
      withdrawal_limit_inr: withd.withdrawal_limit_inr,
      withdrawal_progress: withd.withdrawal_progress,
      withdrawal_progress_inr: withd.withdrawal_progress_inr,
      client_id: this.client_id
    }
    this._userService.updateWithdlimits(withdrawlas)
      .subscribe(res => {
        this.disableButton = true;
        this.edit_withdrawal_limit = 0;
        this.message = res.message;
        this.getUsersData();
      });
    // console.log(withd);
  }

  changeWithdrawalLimitStatus(status) {
    this.disableButton = false;
    var data = {
      client_id: this.client_id,
      status: status,
      updated_by : this.user._id
    }
    this._userService.changeWithdrawalLimitStatus(data)
      .subscribe(res => {
        this.usersInfo.blocked = res.blocked_status
        this.disableButton = true;
        // this.edit_withdrawal_limit = 0;
        // this.message = res.message;
        // this.getUsersData();
      });
  }
  checkEmail(email, client_id) {
    this._userService.checkEmailExists(email, client_id)
      .subscribe(res => {
        if (res.status == 200) {
          this.email_exists = "Email already exists";
          this.disableEditBasicInfo = false;
        } else {
          this.email_exists = "";
          this.disableEditBasicInfo = true;
        }

      });
  }

  suspendUserPopup(client_id, status, suspend) {
    document.getElementById("suspendmodelbutton").click();
  }




  searchBy(value) {
    this.searchByType = value
    if (!this.trade.currency_pair) {
      this.trade = { currency_pair: 'btc_inr' }
    }
    this.getTrades(this.trade.currency_pair, this.client_id);
    // this.getKyc();
  }

  getTrades(currency_name, client_id) {
    this.trade_records = []

    this.trade.currency_pair = currency_name
    this.trade_loader = 1;
    this.currency_name = currency_name;
    this.client_id = client_id;
    // this._TradeService.getTradeList(this.searchByType, this.currency, this.page, this.model.search).subscribe(res => {
    this._dataService.getTrades(this.searchByType, this.currency_name, this.client_id, this.sat)
      .subscribe(res => {
        if (res.data && res.data.length) {
          this.trade_loader = 0;
          this.tradeCount = res.data.length;
          this.trade_records = res.data;
        } else {
          console.log("0000")
          this.tradeCount = 0;
          this.trade_records = [];
        }
      });

  }





}
