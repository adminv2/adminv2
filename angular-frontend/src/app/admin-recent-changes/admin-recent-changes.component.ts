import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { AdminDetails } from '../admin.service';
import { ExcelService } from '../excel.service';

@Component({
  selector: 'app-admin-recent-changes',
  templateUrl: './admin-recent-changes.component.html',
  styleUrls: ['./admin-recent-changes.component.css']
})
export class AdminRecentChangesComponent implements OnInit {
  track_data: any;
  disableButton: boolean = true;
  message: any;
  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _adminService: AdminDetails, private excelService: ExcelService) {
  }
  ngOnInit() {
    this.getRecentChanges();
  }

  getRecentChanges() {
    this._adminService.getAdminRecentChanges()
      .subscribe(res => {
        this.track_data = res.data_is;
      });
  }


}
