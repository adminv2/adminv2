import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRecentChangesComponent } from './admin-recent-changes.component';

describe('AdminRecentChangesComponent', () => {
  let component: AdminRecentChangesComponent;
  let fixture: ComponentFixture<AdminRecentChangesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRecentChangesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRecentChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
