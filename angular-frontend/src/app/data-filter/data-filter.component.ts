import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Trade } from '../trade.service';
import { DataService } from '../data.service';

import { ExcelService } from '../excel.service';
import * as _ from "lodash";
import * as moment from 'moment';
import { Client } from '_debugger';


@Component({
  selector: 'app-data-filter',
  templateUrl: './data-filter.component.html',
  styleUrls: ['./data-filter.component.css']
})
export class DataFilterComponent implements OnInit {
  data_filter: any;
  trade: any;
  model: any;

  currency_records: any;
  currency: any;
  tradeData: any;
  message: any;
  count: number = 0;
  page: number = 1;
  inrdiposit = new Array<any>();
  emptyArray = new Array<any>();
  disableButton: boolean = true;
  td: any;
  searchByType: any;
  loader: number;
  deposits_data: any;
  search_for: any;
  tradesearch_data: any
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private _TradeService: Trade, public _dataService: DataService, private excelService: ExcelService) {
    this.model = { search: '' }
  }
  ngOnInit() {
    this.loader = 0
    this.data_filter = { client_id: '', search_for: '', start_date: '', end_date: '' }
    // this.getALlTrades()
  }

  serchData(data) {
    this.loader = 1
    this.search_for = data.search_for;
    if(this.search_for == 'trade'){
      this.getTradeSearchedData(data);
    }else{
      this.getSearchedData(data);
    }
  }

  getTradeSearchedData(data) {
    this._dataService.getSearchedData(data)
      .subscribe(res => {
        this.loader = 0

        this.deposits_data = '';
        this.tradesearch_data = res.data;
      })
  }

  getSearchedData(data) {
    this._dataService.getSearchedData(data)
      .subscribe(res => {
        this.loader = 0
        this.tradesearch_data = ''
        this.deposits_data = res.data;
      })
  }

  exportToExcel() {
    this.inrdiposit.forEach(function (element) {
    })
    this.excelService.exportAsExcelFile(this.inrdiposit, this.search_for + '_' + new Date() + '_' + this.randomName());
  }

  isAllChecked() {
    return this.inrdiposit.every(_ => _.state);
  }
  buttonState() {
    return !this.inrdiposit.some(_ => _.state);
  }

  randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 3; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  check(option, state, e) {
    if (state) {
      option['state'] = true;
      this.inrdiposit.push(option);
    } else {
      for (var i = 0; i < this.inrdiposit.length; i++) {
        if (this.inrdiposit[i].state === true) {
          _.remove(this.inrdiposit, { _id: option._id });
        }
      }
    }
  }


checkAllTrade(ev){
  this.emptyArray.length = 0;
   var  updateStatus;
    this.inrdiposit = this.tradesearch_data;
    var checkedList = {};
    if (ev.target.checked) {
      this.disableButton = false;
      this.inrdiposit.forEach(element => {
        if(element.status == '0'){
          updateStatus = 'Empty';
        }else if(element.status == '1'){
          updateStatus = 'Filling';
        }else if(element.status == '2'){
          updateStatus = 'Filled';
        }else{
          updateStatus = 'Cancelled';
        }
        element.state = ev.target.checked;
        checkedList['Date'] = element.created;
        checkedList['client_id'] = element.client_id;
        checkedList['Status1_from_table'] = element.type;
        checkedList['Pair'] = element.pair;
        checkedList['Rate'] = element.rate;
        checkedList['Execution_Rate'] = element.exec_rate;
        checkedList['Quantity'] = element.quantity;
        checkedList['Progress'] = element.progress;
        checkedList['Trade_type'] = element.trade_type;
        checkedList['Order_type'] = element.order_type;
        checkedList['status'] =  updateStatus;

        this.emptyArray.push(checkedList);
        checkedList = {};
      });
      this.inrdiposit = this.emptyArray;
    } else {
      this.inrdiposit.forEach(x => {
        x.state = ev.target.checked
      })
    }
  }





  checkAll(ev) {
    this.emptyArray.length = 0;
    var  updateStatus;

    this.inrdiposit = this.deposits_data;
    var checkedList = {};
    if (ev.target.checked) {

     

      this.disableButton = false;
      this.inrdiposit.forEach(element => {

        if(element.status == '0'){
          updateStatus = 'Pending';
        }else if(element.status == '1'){
          updateStatus = 'Complete';
        }else{
          updateStatus = 'Cancelled';
        }

        element.state = ev.target.checked;
        checkedList['client_id'] = element.client_id;
        checkedList['Currency'] = element.currency;
        checkedList['Amount'] = element.amount;
        checkedList['Withdrawal_Type'] = element.withdrawal_type;
        checkedList['Date'] = element.created;
        checkedList['Type'] = element.type;
        checkedList['Transaction_Id'] = element.txid;
        // checkedList['Status'] = element.status;
        checkedList['Status'] = updateStatus;

        this.emptyArray.push(checkedList);
        checkedList = {};
      });
      this.inrdiposit = this.emptyArray;
    } else {
      this.inrdiposit.forEach(x => {
        x.state = ev.target.checked
      })
    }
  }


}
