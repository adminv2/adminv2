import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { AdminSettings } from '../admin_settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  useraccess: any;
  trade: any;
  currencies: any;
  withdrawals: any;
  con_deposits: any;
  uncon_deposits: any;
  add_currency: any;
  disableButton: boolean = true;
  currency_settings: any;
  type: any;

  add_trade_pair: any;
  other: any;


  upd: any;
  update_currency: any;
  objId: any;
  message: any;
  show: any;
  constructor(private activatedRoute: ActivatedRoute, private admin_settings: AdminSettings, private _dataService: DataService, private router: Router, private _http: Http) {
  }

  ngOnInit() {
    // this.withdrawals = {}
    // this.con_deposits = {}
    // this.uncon_deposits = {}

    this.getCurrenciesDetails();
    this.add_currency = { name: '', type: 'currency' }
    this.add_trade_pair = { trade_pair: '', type: 'trade_pair' }
    this.other = { schema_type: '', currency: '', schema_name: '', type: 'other' }
  }

  editCurrency(currency, i, reset) {
    // this.disableButton = false;

    this.update_currency = currency;
    this.upd = { 'currency': currency };

    this.objId = i
    // this.show = 1;

    if (reset == 0) {
      this.show = 0;
    } else {
      this.show = 1;
      this.update_currency = currency;
    }

  }
  saveUpdateCurrency(currency) {
    this.disableButton = false;
    this._dataService.saveUpdateCurrency(currency, this.objId)
      .subscribe(res => {
        this.disableButton = true;
        this.message = res.message;
        // this.update_bal_currency = null
        this.show = 0;
        this.getCurrenciesDetails();
      });
  }

  getCurrenciesDetails() {
    this.disableButton = true;
    this._dataService.getCurrencyPairs()
      .subscribe(res => {
        console.log("result isss", res.currencies_details);
        if (res.currencies_details.trade_pairs) {
          this.trade = res.currencies_details.trade_pairs;
        } else {
          this.trade = {}
        }
        if (res.currencies_details.currencies) {
          this.currencies = res.currencies_details.currencies;
        } else {
          this.currencies = {}
        } if (res.currencies_details.withdrawals) {
          this.withdrawals = res.currencies_details.withdrawals;
        } else {
          this.withdrawals = {}
        } if (res.currencies_details.con_deposits) {
          this.con_deposits = res.currencies_details.con_deposits;
        } else {
          this.con_deposits = {}
        } if (res.currencies_details.uncon_deposits) {
          this.uncon_deposits = res.currencies_details.uncon_deposits;
        } else {
          this.uncon_deposits = {}
        }
      })
  }

  addCurrency(type) {
    this.type = type
    this.currency_settings = '';
    document.getElementById("mymodelbutton").click();
  }

  addCurrencyName(data) {
    // console.log("ye b");
    this.disableButton = false;
    this.admin_settings.addCurrency(data)
      .subscribe(res => {
        this.disableButton = true;
        this.currency_settings = res.message;
        this.getCurrenciesDetails();
        document.getElementById("mymodelbutton").click();
      });
  }
}
