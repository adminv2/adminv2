import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';

//import { UsersComponent } from './users/users.component';
import { BodyComponent } from './body/body.component';
import { DepositInrPendingComponent } from './deposit-inr-pending/deposit-inr-pending.component';
import { SignInComponent } from './admin-users/sign-in/sign-in.component';
import { SignUpComponent } from './admin-users/sign-up/sign-up.component';
import { KycComponent } from './kyc/kyc.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { InrWithdrawalComponent } from './inr-withdrawal/inr-withdrawal.component';
import { SendEmailComponent } from './send-email/send-email.component';//created by satish
import { UserProfileComponent } from './user-profile/user-profile.component';
import { DailyinterestsComponent } from './dailyinterests/dailyinterests.component';
import { ReferralsComponent } from './referrals/referrals.component';
import { UsersListComponent } from './users-list/users-list.component';

import { DepositManualComponent } from './deposit-manual/deposit-manual.component';
import { DepositUsingExcelComponent } from './deposit-using-excel/deposit-using-excel.component';

import { BalancesComponent } from './balances/balances.component';//created by satish
import { TradeComponent } from './trade/trade.component';
import { NotificationsComponent } from './notifications/notifications.component';//created by satish

import { ManualWithdrawalComponent } from './manual-withdrawal/manual-withdrawal.component';
import { TransactionsComponent } from './transactions/transactions.component';

import { FileUploadComponent } from './file-upload/file-upload.component';
import { DailyinterestsAddComponent } from './dailyinterests-add/dailyinterests-add.component';

import { PayoutHistrComponent } from './payout-histr/payout-histr.component';
import { CompanyBankComponent } from './company-bank/company-bank.component';
import { AddCompanyBankComponent } from './add-company-bank/add-company-bank.component';

import { SettingsComponent } from './settings/settings.component';

import { TutorialLinksComponent } from './tutorial-links/tutorial-links.component';
import { UpdatedRecordsComponent } from './updated-records/updated-records.component';
import { DataFilterComponent } from './data-filter/data-filter.component';

import { AdminRecentChangesComponent } from './admin-recent-changes/admin-recent-changes.component';
import { ManageTxIdsComponent } from './manage-tx-ids/manage-tx-ids.component';
import { PayoutWithdrawalsComponent } from './payout-withdrawals/payout-withdrawals.component';


const routes: Routes = [
  {path: '', pathMatch: 'full', component: SignInComponent },
  {path: 'body', component: BodyComponent },
  {path: 'about', component: HeaderComponent },
  {path: 'interest', component: DailyinterestsComponent },
  {path: 'add-daily-interest', component: DailyinterestsAddComponent },
  {path: 'userProfile/:client_id', component: UserProfileComponent}, 
  {path: 'userList', component: UsersListComponent},
  {path: 'deposit/:currency/:status', component: DepositInrPendingComponent},
  {path: 'Manual_deposit',component: DepositManualComponent}, 
  {path: 'Deposit_using_excel',component: DepositUsingExcelComponent},
  {path: 'kyc',component: KycComponent},
  {path: 'sign_up',component: SignUpComponent},
  {path: 'view_support_users',component: AdminUsersComponent},
  {path: 'withdrawal/:currency/:status',component: InrWithdrawalComponent},
  {path: 'Manual_withdrawal',component: ManualWithdrawalComponent},
  {path: 'referral',component: ReferralsComponent},
  {path: 'email',component: SendEmailComponent},
  {path: 'userbalances',component: BalancesComponent},
  {path: 'trade/:currency',component: TradeComponent},
  {path: 'notifications',component: NotificationsComponent},
  {path: 'transactions_history/:currency',component: TransactionsComponent},
  {path: 'all/uploads',component: FileUploadComponent},
  {path: 'payout/history',component: PayoutHistrComponent},
  {path: 'admin/CompanyBank',component: CompanyBankComponent}, 
  {path: 'admin/AddCompanyBank', component: AddCompanyBankComponent},
  {path: 'admin/settings', component: SettingsComponent},
  {path: 'admin/tutorialLinks', component: TutorialLinksComponent},
  {path: 'admin/updatedRecords', component: UpdatedRecordsComponent},
  {path: 'data-filter', component: DataFilterComponent},
  {path: 'admin/recent_changes', component: AdminRecentChangesComponent},
  {path: 'admin/manage_tx_ids', component: ManageTxIdsComponent},
  {path: 'payout_withdrawals', component: PayoutWithdrawalsComponent}
  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
