import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

import { baseUrl } from './app.config';

@Injectable()
export class AdminSettings {
  constructor(private _http: Http, private router: Router) {
  }

  
  addCurrency(data) {
    return this._http.post(baseUrl + "admin/addCurrency", data)
      .map(result => result.json());
  }


}
