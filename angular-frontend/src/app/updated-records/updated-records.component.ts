import { Component, OnInit, Input, Pipe, PipeTransform, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../data.service';
import { FormGroup, FormControl, Validators, FormsModule, NgForm } from '@angular/forms';
import * as moment from 'moment'

import { AdminDetails } from '../admin.service';
import { ExcelService } from '../excel.service';

@Component({
  selector: 'app-updated-records',
  templateUrl: './updated-records.component.html',
  styleUrls: ['./updated-records.component.css']
})
export class UpdatedRecordsComponent implements OnInit {
  updBalances: any;
 
  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _adminService: AdminDetails, private excelService: ExcelService) {
    // this.model = {search:''};
    // this.excelService = excelService;
  }
  ngOnInit() {

    this.getUpdatedBalanesRecords();
    
  }

  getUpdatedBalanesRecords() {
    this._adminService.getUpdatedBalanesRecords()
      .subscribe(res => {
        this.updBalances = res.updBalances;
      });
  }


}
