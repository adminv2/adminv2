import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatedRecordsComponent } from './updated-records.component';

describe('UpdatedRecordsComponent', () => {
  let component: UpdatedRecordsComponent;
  let fixture: ComponentFixture<UpdatedRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatedRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatedRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
