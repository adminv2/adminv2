import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositUsingExcelComponent } from './deposit-using-excel.component';

describe('DepositUsingExcelComponent', () => {
  let component: DepositUsingExcelComponent;
  let fixture: ComponentFixture<DepositUsingExcelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositUsingExcelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositUsingExcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
