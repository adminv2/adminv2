import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { AdminDetails } from '../admin.service';
import { ExcelService } from '../excel.service';

@Component({
  selector: 'app-company-bank',
  templateUrl: './company-bank.component.html',
  styleUrls: ['./company-bank.component.css']
})
export class CompanyBankComponent implements OnInit {
  bankList: any;
  addbank: any;
  bankd: any;
  count: number = 0;
  disableButton: boolean = true;
  toEdit: number;
  full_name: any;
  loadBankedit: number;
  message: any;
  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _adminService: AdminDetails, private excelService: ExcelService) {
    // this.model = {search:''};
    // this.excelService = excelService;
  }
  ngOnInit() {

    this.getBanks();
    this.defineVariables();
  }

  getBanks() {
    this._adminService.getBankList()
      .subscribe(res => {
        this.disableButton = true;
        this.bankList = res.bankData;
        this.count = res.count;
      });
  }

  updateCompanyBank(id) {
    // console.log("id is", id);
    this.message = '';
    this.loadBankedit = 1;
    this.toEdit = id;
    this._adminService.getBankDetails(id)
      .subscribe(res => {
        this.loadBankedit = 0;
        this.addbank = {
          beneficiary: res.bankD.beneficiary,
          ifsc: res.bankD.ifsc,
          bankName: res.bankD.bankName,
          account: res.bankD.account,
          country: res.bankD.country,
          _id: res.bankD._id
        };
      });

    document.getElementById("mymodelbutton").click();
  }

  defineVariables() {
    this.full_name = '';
    this.addbank = { beneficiary: '', ifsc: '', bankName: '', account: '', country: '' };
  }

  editBankDetails(bankData) {
    this._adminService.editCompanyBank(bankData)
      .subscribe(res => {
        this.disableButton = true;
        this.message = res.message;
        setTimeout(function () {
          document.getElementById("mymodelbutton").click();
        }, 3000)
        this.defineVariables();
        this.getBanks();
      });
  }

  changeCompBankStatus(id, status) {
    this.disableButton = false;
    this._adminService.changeCompBankStatus(id, status)
      .subscribe(res => {
        this.getBanks();
      });
  }


}
