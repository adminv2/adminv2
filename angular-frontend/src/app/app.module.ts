import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { NgxPaginationModule } from 'ngx-pagination';
//import { FormsModule } from '@angular/forms';
//import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeadComponent } from './head/head.component';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UsersComponent } from './users/users.component';
import { HttpModule } from '@angular/http';
import { DataService } from './data.service';
import { AppRoutingModule } from './app-routing.module'
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { DepositInrPendingComponent } from './deposit-inr-pending/deposit-inr-pending.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { SignInComponent } from './admin-users/sign-in/sign-in.component';
import { SignUpComponent } from './admin-users/sign-up/sign-up.component';
import { KycComponent } from './kyc/kyc.component';
import { InrWithdrawalComponent } from './inr-withdrawal/inr-withdrawal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WithdrawService } from './withdraw.service';
import { UserProfile } from './user.service';
import { Dashboard } from './dashboard.service';
import { DatePipe } from '@angular/common'
import { ExcelService } from './excel.service';
import { Trade } from './trade.service';
import { AdminDetails } from './admin.service';
import { AdminSettings } from './admin_settings.service';


import { NgxPaginationModule } from 'ngx-pagination';
import { SendEmailComponent } from './send-email/send-email.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import {FilterPipe, rowPipe, searchPipe} from './filter.pipe';
import { MatTableModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material';
import { MatSortModule} from '@angular/material';
import { ReferralsComponent } from './referrals/referrals.component'; 
import { UsersListComponent } from './users-list/users-list.component';
import { DepositManualComponent } from './deposit-manual/deposit-manual.component';
import { DailyinterestsComponent } from './dailyinterests/dailyinterests.component';
import { BalancesComponent } from './balances/balances.component';
import { TradeComponent } from './trade/trade.component'; 



// Date picker
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { DateAdapter, MatDateFormats } from '@angular/material';
import {MatNativeDateModule} from '@angular/material';
import { ManualWithdrawalComponent } from './manual-withdrawal/manual-withdrawal.component';

import { NotificationsComponent } from './notifications/notifications.component';
import { DepositUsingExcelComponent } from './deposit-using-excel/deposit-using-excel.component'; 
import { TransactionsComponent } from './transactions/transactions.component'; 
import { FileUploadComponent } from './file-upload/file-upload.component'; 
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import { DailyinterestsAddComponent } from './dailyinterests-add/dailyinterests-add.component';
import { PayoutHistrComponent } from './payout-histr/payout-histr.component';
import { CompanyBankComponent } from './company-bank/company-bank.component';
import { AddCompanyBankComponent } from './add-company-bank/add-company-bank.component';
import { SettingsComponent } from './settings/settings.component';
import { TutorialLinksComponent } from './tutorial-links/tutorial-links.component';
import { LoaderComponent } from './loader/loader.component';
import { UpdatedRecordsComponent } from './updated-records/updated-records.component';
import { DataFilterComponent } from './data-filter/data-filter.component';
import { AdminRecentChangesComponent } from './admin-recent-changes/admin-recent-changes.component';
import { ManageTxIdsComponent } from './manage-tx-ids/manage-tx-ids.component';
import { PayoutWithdrawalsComponent } from './payout-withdrawals/payout-withdrawals.component';


@NgModule({
  declarations: [
    AppComponent,
    HeadComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    SidebarComponent,
    UsersComponent,
    DepositInrPendingComponent,
    AdminUsersComponent,
    SignInComponent,
    SignUpComponent,
    KycComponent,
    InrWithdrawalComponent,
    FilterPipe,
    rowPipe,
    searchPipe,
    SendEmailComponent,
    UserProfileComponent,
    
    UsersListComponent,
    DepositManualComponent,
    DailyinterestsComponent,
    ReferralsComponent,
    ReferralsComponent,
    BalancesComponent,
    TradeComponent,
    NotificationsComponent,
    ManualWithdrawalComponent,
    DepositUsingExcelComponent,
    TransactionsComponent,
    
    FileUploadComponent,
    FileSelectDirective,
    DailyinterestsAddComponent,
    PayoutHistrComponent,
    CompanyBankComponent,
    AddCompanyBankComponent,
    SettingsComponent,
    TutorialLinksComponent,
    LoaderComponent,
    UpdatedRecordsComponent,
    DataFilterComponent,
    AdminRecentChangesComponent,
    ManageTxIdsComponent,
    PayoutWithdrawalsComponent,
  
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    BrowserModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule ,
    MatInputModule,
    MatNativeDateModule
  ],
  providers: [DataService, WithdrawService, UserProfile,Dashboard,ExcelService,Trade,DatePipe,AdminDetails, AdminSettings,
     { provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})

export class AppModule { }
