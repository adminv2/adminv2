import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutWithdrawalsComponent } from './payout-withdrawals.component';

describe('PayoutWithdrawalsComponent', () => {
  let component: PayoutWithdrawalsComponent;
  let fixture: ComponentFixture<PayoutWithdrawalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutWithdrawalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutWithdrawalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
