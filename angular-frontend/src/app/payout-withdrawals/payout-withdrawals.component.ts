import { Component, OnInit, Input, Pipe, PipeTransform, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../data.service';
import { FormGroup, FormControl, Validators, FormsModule, NgForm } from '@angular/forms';
import * as moment from 'moment'
import * as _ from "lodash";

import { AdminDetails } from '../admin.service';
import { ExcelService } from '../excel.service';


@Component({
  selector: 'app-payout-withdrawals',
  templateUrl: './payout-withdrawals.component.html',
  styleUrls: ['./payout-withdrawals.component.css']
})
export class PayoutWithdrawalsComponent implements OnInit {
  txn_details: any;
  disableButton: boolean = true;
  message: any;
  user: any;
  txn: any;
  disable_save: any;
  txn_message: any;
  model: any;
  p: number = 1;
  count: any;

  data_filter: any;

  emptyArray = new Array<any>();
  payout_export = new Array<any>();
  datas = new Array<any>();

  // payout_export: any;
  exportButton: boolean = true;


  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _adminService: AdminDetails, private excelService: ExcelService) {
    this.model = { search: '' };
    // this.excelService = excelService;
  }

  @ViewChild('ManageTransactionId') ManageTransactionId: NgForm

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('current'));
    this.data_filter = { start_date: '', end_date: '' }

    this.exportButton = false;

    this.getPayoutHistories();
    this.defineVariables();
  }

  getPayoutHistories(data = null) {
    if (data == null) {
      this.data_filter = { start_date: '', end_date: '' }
    }
    this._adminService.getPayoutHistories(this.p, this.model.search, data)
      .subscribe(res => {
        // this.disableButton = true;
        this.txn_details = res.txDetails;
        this.count = res.count;
      });
  }

  addTransactionIds() {
    this.disableButton = false;

    document.getElementById("mymodelbutton").click();
  }

  updateTutorial(i, type) {
    console.log("i isss", i);
    document.getElementById("mymodelbutton").click();
    this._adminService.getTutorialDetails(i, type)
      .subscribe(res => {
        // this.getTutorialLinks();
      });
  }

  changeTxnIdStatus(id, status) {
    this.disableButton = false;
    this._adminService.changeTxnIdStatus(id, status)
      .subscribe(res => {
        this.getPayoutHistories();
      });
  }

  checkTxId(txnId) {
    this.disable_save = 0

    this._adminService.checkTxId(txnId)
      .subscribe(res => {
        console.log("resss", res)
        if (res.status == 300) {
          this.txn_message = "Transaction Id Already Exists"
          this.disable_save = 1
          this.disableButton = false;
        } else {
          this.txn_message = ""
          this.disable_save = 0
          this.disableButton = true;
        }
        // this.getTransactionIds();
      });
  }


  defineVariables() {

    this.txn = {
      txId: '',
      amount: '',
      received_date: ''
    }
  }
  saveTxnIds(txData) {
    txData.admin_id = this.user._id
    this._adminService.saveTxnIds(txData)
      .subscribe(res => {
        this.disableButton = true;
        this.message = res.message;
        // setTimeout(function () {
        //   document.getElementById("mymodelbutton").click();
        // }, 3000)
        this.defineVariables();
        this.getPayoutHistories();

      });
  }

  searchResult($event) {
    if ($event.which == 13) {
      document.getElementById("searchTransactionIds").click();
    }
    if (this.model.search == '') {
      this.p = 1;
      this.getPayoutHistories();
    }
  }
  searchResultData() {
    this.p = 1;
    this.getPayoutHistories();
  }

  pageChange(p) {
    this.p = p;
    this.getPayoutHistories();
    // this.userListArr.length=0;
  }

  checkAll(ev) {
    this.exportButton = false;

    this.emptyArray.length = 0;
    var updateStatus;
    this.payout_export = this.txn_details;
    var checkedList = {};
    if (ev.target.checked) {
      this.exportButton = true;

      var i = 0;
      this.payout_export.forEach(element => {
        i++;
        if (element.status == '0') {
          updateStatus = 'Not Completed';
        } else {
          updateStatus = 'Completed';
        }
        element.state = ev.target.checked;
        checkedList['Sr.No'] = i;
        checkedList['client_id'] = element.client_id;
        checkedList['Amount'] = element.amount;
        checkedList['Date'] = element.created;
        checkedList['status'] = updateStatus;

        this.emptyArray.push(checkedList);
        checkedList = {};
      });
      this.payout_export = this.emptyArray;
    } else {
      this.payout_export.forEach(x => {
        x.state = ev.target.checked
      })
    }
  }



  check(option, element, state, e) {
    var checkedList = {};
    var payout_export = {}
    var datas = {}
    var updateStatus;
    if (option.state) {
      this.exportButton = true;
      if (option.status == '0') {
        updateStatus = 'Not Completed';
      } else {
        updateStatus = 'Completed';
      }

      datas['client_id'] = option.client_id;
      datas['Amount'] = option.amount;
      datas['Date'] = option.created;
      datas['status'] = updateStatus;
      delete option._id;
      delete option.__v;
      this.payout_export.push(datas);
      _.uniqBy(this.payout_export, 'client_id');
    } else {
      for (var i = 0; i < this.payout_export.length; i++) {
        if (this.payout_export[i].state === false) {
          _.remove(this.payout_export, { client_id: option.client_id });
        }
      }
    }
  }


  exportToExcel() {
    // this.payout_export.forEach(function (element) {
    // })
    this.excelService.exportAsExcelFile(this.payout_export, 'payout_withdrawals' + '_' + new Date());
  }


}
