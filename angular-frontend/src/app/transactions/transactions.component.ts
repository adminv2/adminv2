import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { ExcelService } from '../excel.service';
import * as _ from "lodash";
import * as moment from 'moment';
import { DataService } from '../data.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  currency: any;
  currency_trans: any;
  model: any;
  array: any = [];
  // arraydata: any = [];
  trans: any;
  constructor(private activatedRoute: ActivatedRoute, private excelService: ExcelService, private _dataService: DataService,
    private router: Router, private _http: Http) {
    this.model = { limit: '' };
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.currency = params['currency'];
    });
    this._dataService.get_transactions(50).subscribe(res => {
      this.trans = res.transactions;
      for (var data of this.trans) {
        var DestinationTag: any;
        if (data.tx.DestinationTag != null) {
          DestinationTag = data.tx.DestinationTag.toString();
        } else {
          DestinationTag = "null";
        }
          var arraydata = { 
          hash: data.hash,
          Amount: data.tx.Amount,
          Fee: data.tx.Fee,
          Account: data.tx.Account,
          Destination: data.tx.Destination,
          DestinationTag: DestinationTag
        };
      
        this.array.push(arraydata);
      }
      this.currency_trans = this.array;
      this.array = [];
    });
  }

  get_transactions(limit) {
    this._dataService.get_transactions(limit).subscribe(res => {
      this.trans = res.transactions;
      for (var data of this.trans) {
        var DestinationTag: any;
        if (data.tx.DestinationTag != null) {
          DestinationTag = data.tx.DestinationTag.toString();
        } else {
          DestinationTag = "null";
        }
          var arraydata = { 
          hash: data.hash,
          Amount: data.tx.Amount,
          Fee: data.tx.Fee,
          Account: data.tx.Account,
          Destination: data.tx.Destination,
          DestinationTag: DestinationTag
        };
      
        this.array.push(arraydata);
      }
      this.currency_trans = this.array;
      this.array = [];
    });
  }
}
