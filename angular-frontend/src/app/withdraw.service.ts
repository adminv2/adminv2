import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

import { baseUrl } from './app.config';

@Injectable()
export class WithdrawService {

  constructor(private _http: Http, private router: Router) { }

  getwithdraw(currency, status,page,withdrawal_type,sort, search) {
    return this._http.post(baseUrl + 'withdrawal/' + currency + '/' + status+ '/' + page + '/' + withdrawal_type ,{search:search,sort:sort})
      .map(result => result.json());
  }

  changeWithdrawStatus(id, status, currency, txnId, changed_by) {
    let data = {
      id : id,
      status: status,
      currency: currency,
      txnId : txnId,
      changed_by : changed_by
    };
    return this._http.post(baseUrl + 'withdrawal/changesWithdrawalStatus', data)
    .map(result => result.json());
  }

  // akash

  saveManualWithdrawal(data){
    return this._http.post(baseUrl + "withdrawal/saveManualWithdrawals", data)
    .map(result => result.json());
  }
  
}
