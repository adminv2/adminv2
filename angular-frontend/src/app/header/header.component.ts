import { Component, OnInit } from '@angular/core';
//import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { baseUrl } from '../app.config';

import { AdminDetails } from '../admin.service';
import { ExcelService } from '../excel.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  selectedIndex: number;
  ip: any;
  data: any;
  useraccess: any;
  user_details: any;
  pass: any;
  message: any;
  disableButton: boolean = true;

  user: any;

  constructor(private router: Router, public _dataService: DataService,
    private _http: Http, public _adminService: AdminDetails) {
    this.useraccess = JSON.parse(localStorage.getItem('useraccess'));
    this.user_details = JSON.parse(localStorage.getItem('current'));

    this._dataService.authCheck('WithdrawINR');

    // constructor(private _dataService: DataService, private router: Router, private _http: Http) {
  }
  ngOnInit() {

    this.user = JSON.parse(localStorage.getItem('current'));

    this.pass = {
      current_password: '',
      update_password: '',
      admin_user_id: this.user._id
    }
  }

 

  changePassword(admin_id) {
    document.getElementById("mymodelbuttonChangePass").click();
  }

  updatePassword(data) {
    this.disableButton = false;
    
    this._adminService.changePassword(data)
      .subscribe(res => {
        this.disableButton = true;
        this.message = res.message;
        if (res.status == 200) {
          // setTimeout(function () {
            document.getElementById("mymodelbuttonChangePass").click();
           
          // }, 2500)
          this.logout();

          
        }
      });
  }


  logout() {
    console.log("log out me aaya");
    this.data = JSON.parse(localStorage.getItem('current'));
    this._dataService.logout(this.data.username, this.data.email).subscribe(res => {
      if (res.code == '152') {
        localStorage.removeItem('current');
        localStorage.removeItem('useraccess');
        this.router.navigate(['/']);
      } else {
        console.log('login fail');
      }
    });
  }
}
