import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

import { baseUrl } from './app.config';

@Injectable()
export class Dashboard {

  constructor(private _http: Http, private router: Router) {

  }

  get_highest_balancesOf(){
    return this._http.post(baseUrl + "dashboard/getHighestBalances", '')
      .map(result => result.json());
  }

  adminStats(){
    return this._http.post(baseUrl + "dashboard/adminStats", '')
      .map(result => result.json());
  }

  getDepositsStatic() {
    return this._http.post(baseUrl + "dashboard/depositsStat", '')
      .map(result => result.json());
  }
  getPendingDepositsStatic() {
    return this._http.post(baseUrl + "dashboard/PendingdepositsStat", '')
      .map(result => result.json());
  }
  getWithdrawalStatic() {
    return this._http.post(baseUrl + "dashboard/getWithdrawalStatic", '')
      .map(result => result.json());
  }
  getPendingWithdrawalStatic() {
    return this._http.post(baseUrl + "dashboard/getPendingWithdrawalStatic", '')
      .map(result => result.json());
  }
  getKycStats(status) {
    return this._http.post(baseUrl + "dashboard/getKycStats", {status:status})
      .map(result => result.json());
  }
  getUsers(status) {
    return this._http.post(baseUrl + "dashboard/getUsers", {status:status})
      .map(result => result.json());
  }

  getBalanceStats() {
    return this._http.post(baseUrl + "dashboard/getBalanceStats",'')
      .map(result => result.json());
  }
 



}
