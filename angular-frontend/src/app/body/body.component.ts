import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { Dashboard } from '../dashboard.service';

import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { baseUrl } from '../app.config';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  processing: number = 1;
  depositsSt: any;
  pendingdepositsSt: any;
  withdrawalStats: any;
  pendingwithdrawalStats: any;
  kycstats: any;
  kycVerifiedstats: any;
  kycRejectedstats: any;
  total_users: any;
  count: any;

  useraccess: any;
  user: any;
  model: any;

  totalWithd: any;
  todayWithd: any;
  totalDepo: any;
  todayDepo: any;
  tradeStats: any;

  todayInterest: any;
  totalInterest: any;

  currency_pair: any;

  currencies: any;
  trade: any;
  kyc: any;

  fiat_deposits: any;
  fiat_withdrawals: any;
  highest_balance: any;

  loading : any;
  constructor(private _dataService: DataService, private router: Router, private _http: Http, public _dashboard: Dashboard) {
  }
  ngOnInit() {
    this.model = { search: '' }
    this.kyc = {}
    this.totalWithd = {}
    this.todayWithd = {}
    this.fiat_deposits = {}
    this.fiat_withdrawals = {}
    this.todayDepo = {}
    this.totalDepo = {}
    this.loading = 1;
    this.useraccess = JSON.parse(localStorage.getItem('useraccess'));

    this.user = JSON.parse(localStorage.getItem('current'));

    this.getCurrenciesDetails();
    this.adminStats();
    this.getUsers();
    // this.get_highest('btc');

  }
  get_highest(currency) {
    // this._dataService.getCurrencyPairs()
    //   .subscribe(res => {
    //     // console.log
    //     this.currencies = res.currencies_details.currencies;
    //     for (var i = 0; i < this.currencies.length; i++) {
          this._dashboard.get_highest_balancesOf().subscribe(res => {
            console.log(res)

            // if (i == this.currencies.length) {
            //   this.highest_balance = res.data
            // }

          })
  }

  getCurrenciesDetails() {
    this._dataService.getCurrencyPairs()
      .subscribe(res => {
        this.currency_pair = res.currencies_details.trade_pairs;
        this.currencies = res.currencies_details.currencies;
      })
  }


  adminStats() {
    this.loading = 1;
    this._dashboard.adminStats().subscribe(res => {
      this.loading = 0;

      console.log("result is", res.data);

      this.processing = 0;
      this.totalWithd = res.data.totalWithdraw;
      this.todayWithd = res.data.todayWithdraw;

      this.totalDepo = res.data.totalDeposit;
      this.todayDepo = res.data.todayDeposit;

      this.tradeStats = res.data.transfer;

      this.totalInterest = res.data.totalinterest;
      this.todayInterest = res.data.todayinterest;

      this.kyc = res.data.kyc;

      this.fiat_deposits = res.data.deposit_status_wise;
      this.fiat_withdrawals = res.data.withdrawal_status_wise;
      


    });
  }

  getDepositsStatic() {
    this.processing = 1;
    this._dashboard.getDepositsStatic().subscribe(res => {
      this.processing = 0;
      this.depositsSt = res.data;
    });
  }

  getPendingDeposits() {
    this.processing = 1;
    this._dashboard.getPendingDepositsStatic().subscribe(res => {
      this.processing = 0;
      this.pendingdepositsSt = res.data;
    });
  }
  getWithdrawaStatics() {
    this.processing = 1;
    this._dashboard.getWithdrawalStatic().subscribe(res => {
      this.processing = 0;
      this.withdrawalStats = res.data;
    });
  }
  getPendingWithdrawaStatics() {
    this.processing = 1;
    this._dashboard.getPendingWithdrawalStatic().subscribe(res => {
      this.processing = 0;
      this.pendingwithdrawalStats = res.data;
    });
  }
  getKycStats() {
    this.processing = 1;
    this._dashboard.getKycStats(0).subscribe(res => {
      this.processing = 0;
      this.kycstats = res.data;
    });
  }
  getVerifiedKycStats() {
    this.processing = 1;
    this._dashboard.getKycStats(1).subscribe(res => {
      this.processing = 0;
      this.kycVerifiedstats = res.data;
    });
  }
  getRejectedKycStats() {
    this.processing = 1;
    this._dashboard.getKycStats(2).subscribe(res => {
      this.processing = 0;
      this.kycRejectedstats = res.data;
    });
  }

  getTotalBalances() {
    this.processing = 1;
    this._dashboard.getBalanceStats().subscribe(res => {
      console.log(res);
      this.processing = 0;
      this.kycRejectedstats = res.data;
    });
  }
  getUsers() {
    this.processing = 1;
    this._dashboard.getUsers(1).subscribe(res => {
      this.processing = 0;
      this.total_users = res.data;
    });
  }
}
