import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Trade } from '../trade.service';
import { DataService } from '../data.service';

import { ExcelService } from '../excel.service';
import * as _ from "lodash";
import * as moment from 'moment';


@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css']
})
export class TradeComponent implements OnInit {
  currency_records: any;
  currency: any;
  tradeData: any;
  message: any;
  model: any;
  count: number = 0;
  page: number = 1;
  inrdiposit = new Array<any>();
  emptyArray = new Array<any>();
  disableButton: boolean = true;
  trade: any;
  td: any;
  searchByType: any;
  loader : number;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private _TradeService: Trade,public _dataService: DataService, private excelService: ExcelService) {
    this.model = { search: '' }
  }

  ngOnInit() {
    this.searchByType = 0;
    this.getCurrencyPairs();
    // this.trade = ['bch_btc', 'bch_inr', 'bch_thb', 'btc_inr', 'btc_thb', 'btg_btc', 'btg_inr', 'btg_thb', 'bch_btc', 'ltc_btc', 'ltc_inr', 'ltc_thb', 'xlm_btc', 'xlm_inr', 'xlm_thb', 'xrp_btc', 'xrp_inr', 'xrp_thb', 'pura_btc', 'pura_inr', 'pura_thb', 'dash_btc', 'dash_inr', 'dash_thb', 'ada_btc', 'ada_inr', 'ada_thb','eth_btc','eth_inr', 'eth_thb' ]
    this.activatedRoute.params.subscribe((params: Params) => {
      this.currency = params['currency'];
      this.page = 1
      this.td = { currency_pair: params['currency'] }
      this.tradeRecords()
    });
  }

  getCurrencyPairs(){
    this._dataService.getCurrencyPairs()
    .subscribe(res => {
      this.trade = res.currencies_details.trade_pairs;
    })
  }

  loadTrade(currency_pair){
    this.router.navigate(['/trade/' + currency_pair])
}

  tradeRecords() {
    this.loader = 1;
    this._TradeService.getTradeList(this.searchByType, this.currency, this.page, this.model.search).subscribe(res => {
      this.loader = 0;
      if (res.code == 300) {
        this.tradeData = res.data;
        this.count = res.count;
        this.message = ''
      } else {
        this.message = 'No Records'
        this.tradeData = [];
        this.count = 0;
      }
    });
  }

  pageChange(page) {
    this.page = page
    this.tradeRecords()
  }

  searchResult() {
    if (this.model.search == '') {
      this.tradeRecords();
    }
  }

  searchResultData() {
    this.page = 1
    this.tradeRecords();
  }

  isAllChecked() {
    return this.inrdiposit.every(_ => _.state);
  }
  buttonState() {
    return !this.inrdiposit.some(_ => _.state);
  }

  exportToExcel() {
    this.inrdiposit.forEach(function (element) {
    })
    this.excelService.exportAsExcelFile(this.inrdiposit, 'Trade_history_' + new Date() + '_'+ this.randomName());
  }

  randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 3; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  check(option, state, e) {
    if (state) {
      option['state'] = true;
      this.inrdiposit.push(option);
    } else {
      for (var i = 0; i < this.inrdiposit.length; i++) {
        if (this.inrdiposit[i].state === true) {
          _.remove(this.inrdiposit, { _id: option._id });
        }
      }
    }
  }
  checkAll(ev) {
    this.emptyArray.length = 0;
    this.inrdiposit = this.tradeData;
    var checkedList = {};
    if (ev.target.checked) {
      console.log("all checked");
      this.disableButton = false;

      console.log(this.inrdiposit);

      this.inrdiposit.forEach(element => {
        element.state = ev.target.checked;
        checkedList['_id'] = element._id;

        checkedList['client_id'] = element.client_id;
        checkedList['created'] = element.created;
        checkedList['fee_currency'] = element.fee_currency;
        checkedList['fees'] = element.fees;
        checkedList['order_id'] = element.order_id;
        checkedList['order_type'] = element.order_type;
        checkedList['pair'] = element.pair;
        checkedList['progress'] = element.progress;
        checkedList['quantity'] = element.quantity;
        checkedList['rate'] = element.rate;
        checkedList['trade_type'] = element.trade_type;

        checkedList['state'] = element.state;
        this.emptyArray.push(checkedList);
        checkedList = {};
      });
      this.inrdiposit = this.emptyArray;
    } else {
      this.inrdiposit.forEach(x => {
        x.state = ev.target.checked
      })
    }
  }

  searchBy(value) {
    console.log(value);
    this.searchByType = value
    this.tradeRecords();
  }

  // UncheckAll() {
  //   var w = document.getElementsByTagName('input');

  //   console.log("uncheck all w is", w);
  //   for (var i = 0; i < w.length; i++) {
  //     if (w[i].type == 'checkbox') {
  //       w[i].checked = false;
  //     }
  //   }
  // }
}
