// created by satish date:3/26/2018
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { ExcelService } from '../excel.service';
import * as _ from "lodash";
@Component({
  selector: 'app-referrals',
  templateUrl: './referrals.component.html',
  styleUrls: ['./referrals.component.css']
})
export class ReferralsComponent implements OnInit {
  users_withdrawa: any;
  persons: Person[];
  user_referral: any;
  currency: any;
  message: any;
  count: number = 0;
  cars = new Array<any>();
  emptyArray = new Array<any>();
  balances: any;
  pageNo: number = 1;
  model: any;
  currencies_are: any;
  processing = 1;
  key: string = 'name'; //set default
  reverse: boolean = false;


  constructor(private activatedRoute: ActivatedRoute, private _dataService: DataService,
    private router: Router, private _http: Http, private excelService: ExcelService) {
    this._dataService.authCheck('Referrals');
    this.excelService = excelService;
    this.model = { search: '' };

  }
  ngOnInit() {
    this.getReferrals();
  }


  getReferrals() {
    this.processing = 1;
    this._dataService.getReferral(this.pageNo, this.model.search).subscribe(res => {
      this.processing = 0;
      this.user_referral = res.data;
      this.currencies_are = res.currencies_are;
      this.count = res.count;
    });
  }

  searchResult() {
    if (this.model.search == '') {
      this.getReferrals();
    }
  }
  searchResultData() {
    this.getReferrals();
  }

  loadPage(pageNo) {
    this.pageNo = pageNo;
    this.cars.length = 0
    this.UncheckAll();
    this.getReferrals();


  }
  randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  exportToExcel() {
    this.excelService.exportAsExcelFile(this.cars, 'referrals' + '_' + this.randomName());
  }
  check(option, balances, state, e) {
    if (state) {
      balances.forEach(element => {
        if (element.currency === 'btc') {
          option['btc'] = element.amount;
        }
        if (element.currency === 'btg') {
          option['btg'] = element.amount;
        }
        if (element.currency === 'ltc') {
          option['ltc'] = element.amount;
        }
        if (element.currency === 'xlm') {
          option['xlm'] = element.amount;
        }
        if (element.currency === 'xrp') {
          option['xrp'] = element.amount;
        }
        if (element.currency === 'bch') {
          option['bch'] = element.amount;
        }
        if (element.currency === 'inr') {
          option['inr'] = element.amount;
        }
        if (element.currency === 'thb') {
          option['thb'] = element.amount;
        }
        option['state'] = true;
      });
      this.cars.push(option);
      this.cars = _.uniqBy(this.cars, '_id');
    } else {
      // console.log("this.cars Un-check",this.cars);
      for (var i = 0; i < this.cars.length; i++) {
        if (this.cars[i].state === true) {
          _.remove(this.cars, { _id: option._id });
        }
      }
    }

  }
  checkAll(ev) {
    this.emptyArray.length = 0;
    this.cars = this.user_referral;
    var checkedList = {};
    if (ev.target.checked) {
      //  console.log("all checked");
      this.cars.forEach(element => {
        element.state = ev.target.checked;
        checkedList['_id'] = element._id;
        checkedList['referring_user'] = element.referring_user;
        checkedList['referred_user'] = element.referred_user;
        checkedList['state'] = element.state;
        if (element.balances.length > 0) {
          element.balances.forEach(balKey => {
            if (balKey.currency === 'btc' && ev.target.checked) {
              checkedList['btc'] = balKey.amount;
            }
            if (balKey.currency === 'btg' && ev.target.checked) {
              checkedList['btg'] = balKey.amount;
            }
            if (balKey.currency === 'ltc' && ev.target.checked) {
              checkedList['ltc'] = balKey.amount;
            }
            if (balKey.currency === 'xlm' && ev.target.checked) {
              checkedList['xlm'] = balKey.amount;
            }
            if (balKey.currency === 'xrp' && ev.target.checked) {
              checkedList['xrp'] = balKey.amount;
            }
            if (balKey.currency === 'bch' && ev.target.checked) {
              checkedList['bch'] = balKey.amount;
            }
            if (balKey.currency === 'inr' && ev.target.checked) {
              checkedList['inr'] = balKey.amount;
            }
            if (balKey.currency === 'thb' && ev.target.checked) {
              checkedList['thb'] = balKey.amount;
            }
          });
        }
        this.emptyArray.push(checkedList);
        checkedList = {};
      });
      this.cars = this.emptyArray;
      // _.uniqBy(this.cars, 'client_id');
    } else {
      // console.log("all unchecked");
      this.cars.forEach(x => {
        //  console.log("Uncheckall", x.state = ev.target.checked);
        x.state = ev.target.checked
      })
    }

    // this.cars.forEach(x => x.state = ev.target.checked)
  }
  isAllChecked() {
    return this.cars.every(_ => _.state);
  }
  buttonState() {
    return !this.cars.some(_ => _.state);
  }
  UncheckAll() {
    var w = document.getElementsByTagName('input');
    for (var i = 0; i < w.length; i++) {
      if (w[i].type == 'checkbox') {
        w[i].checked = false;
      }
    }
  }
}
class Person {
  id: number;
  updated: String;
  created: String;
  referring_user: number;
  referred_user: number
  inr: number;
  btc: number;
  btg: number;
  bch: number;
  xlm: number;
  ltc: number;
  thb: number;
  xrp: number;
}


