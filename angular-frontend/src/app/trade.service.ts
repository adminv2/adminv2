import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

import { baseUrl } from './app.config';

@Injectable()
export class Trade {

  constructor(private _http: Http, private router: Router) {
  
  }

    
    getTradeList(searchByType,currency, page,search){
      return this._http.post(baseUrl + "tradeUser/TradeList",{searchByType:searchByType,currency:currency,page:page,search:search})
         .map(result => result.json());
    }
}
