import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

import { baseUrl } from './app.config';

@Injectable()
export class AdminDetails {
  constructor(private _http: Http, private router: Router) {
  }

  getAdminRecentChanges(){
    return this._http.post(baseUrl + "admin/getAdminRecentChanges", '')
      .map(result => result.json());
  }

  getBankList() {
    return this._http.post(baseUrl + "admin/bankList", '')
      .map(result => result.json());
  }
  getUpdatedBalanesRecords() {
    return this._http.post(baseUrl + "admin/getUpdatedBalanesRecords", '')
      .map(result => result.json());
  }
  
  getBankDetails(id) {
    return this._http.post(baseUrl + "admin/getBankDetails", { id: id })
      .map(result => result.json());
  }

  editCompanyBank(bankData) {
    return this._http.post(baseUrl + "admin/editCompanyBank", bankData)
      .map(result => result.json());
  }

  changeCompBankStatus(id, status) {
    var data = {
      id: id,
      status: status
    }
    return this._http.post(baseUrl + "admin/changeCompanyBankStatus", data)
      .map(result => result.json());
  }

  addCompanyBank(bankData) {
    return this._http.post(baseUrl + "admin/addCompanyBank", bankData)
      .map(result => result.json());
  }

  getTutorialLinks() {
    return this._http.post(baseUrl + "admin/getTutorialLinks", '')
      .map(result => result.json());
  }


  saveTutorial(tutorialData){
    return this._http.post(baseUrl + "admin/saveTutorial", tutorialData)
      .map(result => result.json());
  }
  changeStatus(id, status, type) {
    var data = {
      id: id,
      status: status,
      type : type
    }
    return this._http.post(baseUrl + "admin/changeTutorialStatus", data)
      .map(result => result.json());
  }

  changeTxnIdStatus(id, status) {
    var data = {
      id: id,
      status: status
    }
    return this._http.post(baseUrl + "admin/changeTxnIdStatus", data)
      .map(result => result.json());
  }
  


  getTutorialDetails(i, type) {
    return this._http.post(baseUrl + "admin/getTutorialLinks", { i: i, type: type })
      .map(result => result.json());
  }

  changePassword(data){
    return this._http.post(baseUrl + "admin/changePassword",data)
    .map(result => result.json());
  }

  saveTxnIds(txIds){
    return this._http.post(baseUrl + "admin/saveTxnData", txIds)
      .map(result => result.json());
  }
  getTransactionIds(p, search) {
    return this._http.post(baseUrl + "admin/getTransactionIds/" + p,{ search: search })
      .map(result => result.json());
  }
  checkTxId(txnId) {
    return this._http.post(baseUrl + "admin/checkTxId", {txnId:txnId})
      .map(result => result.json());
  }
  getPayoutHistories(p, search, data) {
    return this._http.post(baseUrl + "admin/getPayoutHistories/" + p,{ search: search, data: data })
      .map(result => result.json());
  }

}
