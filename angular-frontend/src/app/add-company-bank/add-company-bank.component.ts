import { Component, OnInit, Input, Pipe, PipeTransform, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../data.service';
import { FormGroup, FormControl, Validators, FormsModule, NgForm } from '@angular/forms';
import * as moment from 'moment'

import { AdminDetails } from '../admin.service';
import { ExcelService } from '../excel.service';

@Component({
  selector: 'app-add-company-bank',
  templateUrl: './add-company-bank.component.html',
  styleUrls: ['./add-company-bank.component.css']
})
export class AddCompanyBankComponent implements OnInit {
  addbank: any;
  message: any;
  disableButton: boolean = true;
  full_name: any;

  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _adminService: AdminDetails, private excelService: ExcelService) {}

  @ViewChild('depositForm') depositForm: NgForm

  ngOnInit() {
    this.defineVariables()
  }
  defineVariables() {
    this.full_name = '';
    this.addbank = { beneficiary: '', ifsc: '', bankName: '', account: '', country: ''};
  }
  addBankDetails(bankData) {
    this._adminService.addCompanyBank(bankData)
      .subscribe(res => {
        this.disableButton = true;
        this.message = res.message;
        this.depositForm.reset()
        this.defineVariables();
      });
  }
}
