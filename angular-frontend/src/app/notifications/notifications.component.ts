
import { Component, OnInit, Input, Pipe, PipeTransform, ViewChild, ElementRef } from '@angular/core';
import { DataService } from '../data.service';
import { FormGroup, FormControl, Validators, FormsModule, NgForm } from '@angular/forms';

import { Router } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  constructor(private _dataService: DataService, private router: Router, private _http: Http) { }
  model: any;
  message: any;
  isVerified: number = 0;

  @ViewChild('registerForm') registerForm: NgForm

  ngOnInit() {
    this.model = { to: [], title: '', message: '', user: this.isVerified };
  }
  getclientEmail(client) {
    let premails = this.model.to;
    this._dataService.getclientEmail(client).subscribe(res => {
      if (res.code == 152 && res.data != null) {
        this.message = ' ';
        let email = res.data.email;
        if (premails.length != 0) {
          this.model.to = this.model.to + ',' + email;
        } else {
          this.model.to = email;
        }
      } else {
        this.message = 'Email Not Found';
      }
      this.model.client = '';
    });
  }
  sendNotifications(data) {
    this._dataService.customNotifications(data).subscribe(res => {
      if (res.status === 404) {
        this.message = 'Notifications Not Found'
      } else if (res.status === 401) {
        this.message = 'provide required fileds'
      } else {
        this.message = res.message;
        this.model = { to: [], title: '', message: '' };
        this.registerForm.reset()
      }
    });
  }

  mail(value) {
    this.isVerified = value;
    this.model.all = this.isVerified;
  }

}
