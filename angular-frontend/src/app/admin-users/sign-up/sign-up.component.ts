import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DataService } from '../../data.service';
import { FormsModule } from '@angular/forms';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { baseUrl } from '../../app.config';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private _dataService: DataService, private router: Router, private _http: Http) {
    this._dataService.authCheck('');
  }
  model: any;
  message: any;
  user: any;

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('current'));

    this.model = { f_name: '', l_name: '', username: '', email: '', password: '', confirm_password: '', login_ip: '', description: '', type: '' };
  }

  register(data) {
    if (data.password != data.confirm_password) {
      this.message = 'Confirm Password Do not Match With Password Field';
    } else {
      data.added_by = this.user._id;
      this._dataService.register(data).subscribe(res => {
        this.message = res.message;
        if (res.code == 152) {
          this.router.navigate(['/view_support_users']);
        }
      });
    }

  }

}
