import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,Router } from '@angular/router';
import { DataService } from '../../data.service';
import { Http, Headers, RequestOptions, Response} from '@angular/http';
import { baseUrl } from '../../app.config';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  selectedIndex: number;
  model: any;
  login_ip: any;
  ip: any;
  message: any;

  constructor(private _dataService: DataService, private router: Router, private _http: Http) {
    if (localStorage.getItem('current')) {
         this.router.navigate(['/body']);
    }
  }

  ngOnInit() {
    this._dataService.get_IP().subscribe(res => {
      this.ip = res.ip;
    });
    this.model  = new FormGroup({
          'username': new FormControl(null, [Validators.required]),
          'password': new FormControl(null, [Validators.required]),
          'login_ip': new FormControl(this.ip),
    });
  }

  Login() {
    this.model.value.login_ip = this.ip;
    this._dataService.authntication(this.model.value).subscribe(res => {
      if (res.code == '152') {
        localStorage.setItem('current', JSON.stringify(res.userData));
        localStorage.setItem('useraccess', JSON.stringify(res.accessdata));
        this.router.navigate(['/body']);
      } else {
        this.message = 'Enter Correct Login Details';
        console.log('login fail');
      }
    });
  }

}

