import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { FormsModule } from '@angular/forms';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { baseUrl } from '../app.config';


@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {
  supportusers: any;
  p: any = 0;
  message: any;
  useraccess: any;
  id: any;
  username: any;
  constructor(private _dataService: DataService, private router: Router, private _http: Http) {
    this._dataService.authCheck('');
  }

  ngOnInit() {
    this._dataService.getSupportTeam().subscribe(res => {
      if (res.code == 152) {
        console.log(res.users);
        this.supportusers = res.users;
      }
    });
  }

  viewStatus(id, username) {
    this.message = '';
    this.id = id;
    this._dataService.getUserAccess(id).subscribe(res => {
      console.log(res.data, id);
      if (res.code == 152) {
        this.useraccess = res.data;
        this.useraccess.username = username;
      } else {
        this.useraccess = {};
      }
    });
  }

  ChangeUserAccess(access, type) {
    this.message = '';
    this._dataService.ChangeUserAccess(this.id, access, type).subscribe(res => {
      if (res.code == 152) {
        this.message = 'Updated Successfully';
      } else {
        this.message = 'Failed, Please Try Again';
      }
    });
  }
}
