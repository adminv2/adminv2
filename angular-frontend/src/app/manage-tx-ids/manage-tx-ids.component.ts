import { Component, OnInit, Input, Pipe, PipeTransform, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../data.service';
import { FormGroup, FormControl, Validators, FormsModule, NgForm } from '@angular/forms';
import * as moment from 'moment'

import { AdminDetails } from '../admin.service';
import { ExcelService } from '../excel.service';


@Component({
  selector: 'app-manage-tx-ids',
  templateUrl: './manage-tx-ids.component.html',
  styleUrls: ['./manage-tx-ids.component.css']
})
export class ManageTxIdsComponent implements OnInit {
  txn_details: any;
  disableButton: boolean = true;
  message: any;
  user: any;
  txn: any;
  disable_save: any;
  txn_message: any;
  model: any;
  p: number = 1;
  count : any;


  
  constructor(private activatedRoute: ActivatedRoute, private route: Router, public _adminService: AdminDetails, private excelService: ExcelService) {
    this.model = {search:''};
    // this.excelService = excelService;
  }

  @ViewChild('ManageTransactionId') ManageTransactionId: NgForm

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('current'));


    this.getTransactionIds();
    this.defineVariables();
  }

  getTransactionIds() {
    this._adminService.getTransactionIds(this.p, this.model.search)
      .subscribe(res => {
        this.disableButton = true;
        this.txn_details = res.txDetails;
        this.count = res.count;
      });
  }

  addTransactionIds() {
    this.disableButton = false;

    document.getElementById("mymodelbutton").click();
  }

  updateTutorial(i, type) {
    console.log("i isss", i);
    document.getElementById("mymodelbutton").click();
    this._adminService.getTutorialDetails(i, type)
      .subscribe(res => {
        // this.getTutorialLinks();
      });
  }

  changeTxnIdStatus(id, status) {
    this.disableButton = false;
    this._adminService.changeTxnIdStatus(id, status)
      .subscribe(res => {
        this.getTransactionIds();
      });
  }

  checkTxId(txnId) {
    this.disable_save = 0

    this._adminService.checkTxId(txnId)
      .subscribe(res => {
        console.log("resss", res)
        if (res.status == 300) {
          this.txn_message = "Transaction Id Already Exists"
          this.disable_save = 1
          this.disableButton = false;
        } else {
          this.txn_message = ""
          this.disable_save = 0
          this.disableButton = true;
        }
        // this.getTransactionIds();
      });
  }


  defineVariables() {
    this.txn = {
      txId: '',
      amount: '',
      received_date: ''
    }
  }
  saveTxnIds(txData) {
    txData.admin_id = this.user._id
    this._adminService.saveTxnIds(txData)
      .subscribe(res => {
        this.disableButton = true;
        this.message = res.message;
        // setTimeout(function () {
        //   document.getElementById("mymodelbutton").click();
        // }, 3000)
        this.defineVariables();
        this.getTransactionIds();

      });
  }


 
  
  searchResult($event){
    if ($event.which == 13) {
      document.getElementById("searchTransactionIds").click();
    }
    if(this.model.search == ''){
      this.p = 1;
      this.getTransactionIds();
    }
  }
  searchResultData(){
    this.p = 1;
    this.getTransactionIds();
  }

  pageChange(p) {
    this.p = p;
    this.getTransactionIds();
    // this.userListArr.length=0;
  }

}
