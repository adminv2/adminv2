import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTxIdsComponent } from './manage-tx-ids.component';

describe('ManageTxIdsComponent', () => {
  let component: ManageTxIdsComponent;
  let fixture: ComponentFixture<ManageTxIdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageTxIdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTxIdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
