// import { Component, OnInit } from '@angular/core';
import { Component, OnInit, Input, Pipe, PipeTransform, ViewChild, ElementRef } from '@angular/core';
import { DataService } from '../data.service';
// import { FormsModule, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { FormGroup, FormControl, Validators, FormsModule, NgForm } from '@angular/forms';

// Wrote this to upload file
import { FileUploader } from 'ng2-file-upload';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import { baseUrl } from '../app.config';
import { ExcelService } from '../excel.service';
import * as _ from "lodash";




@Component({
  selector: 'app-send-email',
  templateUrl: './send-email.component.html',
  styleUrls: ['./send-email.component.css']
})
export class SendEmailComponent implements OnInit {
  constructor(private _dataService: DataService, private router: Router, private _http: Http, private http: Http, private el: ElementRef) { }
  model: any;
  message: any;
  isVerified: number = 0;
  user: any;
  file_names: any;

  filesData = new Array<any>();
  cars = new Array<any>();
  emptyArray = new Array<any>();
  IShow: Boolean = true;
  public uploader: FileUploader = new FileUploader({ url: baseUrl + "file/addMailTemplate" });

  @ViewChild('registerForm') registerForm: NgForm

  ngOnInit() {

    console.log(baseUrl);

    this.getFileNames()
    this.user = JSON.parse(localStorage.getItem('current'));
    this.model = { to: [], subject: '', description: '', range_from: '', range_to: '', user: this.isVerified };

    this.IShow=false;
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        var SomeData=JSON.parse(response);
        console.log(SomeData)
        this.IShow=true,
        this.filesData=SomeData.data;
    }

  }

  getFileNames(){
    this._dataService.getFileNames().subscribe(res => {
      this.file_names = res.file_list;
    })
  }

  getclientEmail(client) {
    let premails = this.model.to;
    this._dataService.getclientEmail(client).subscribe(res => {
      if (res.code == 152 && res.data != null) {
        this.message = ' ';
        let email = res.data.email;
        if (premails.length != 0) {
          this.model.to = this.model.to + ',' + email;
        } else {
          this.model.to = email;
        }
      } else {
        this.message = 'Email Not Found';
      }
      this.model.client = '';
    });
  }

  sendEmail(data) {
    if (this.model.user == 1) {
      this.model.sent_by = this.user._id
      this._dataService.emailing(this.model).subscribe(res => {
        // this.registerForm.reset();
        this.message = res.message;
        // this.model = { to: [], subject: '', description: '' };
      });
    } else if (this.model.user == 2) {
      this._dataService.emailing_batchwise(this.model).subscribe(res => {
        console.log(res.message);
        this.message = res.message;
      });
    } else {
      this.model.to = this.model.to.split(',');

      console.log(this.model.user);


      this._dataService.emailing(this.model).subscribe(res => {
        // this.registerForm.reset();
        this.message = res.message;
        // this.model = { to: [], subject: '', description: '' };
      });
      // this._dataService.sendMailToSingleSome(this.model).subscribe(res => {
      //   console.log(res.message);
      //   this.message = res.message;
      // });
    }


  }

  mail(value) {
    this.isVerified = value
    this.model.user = this.isVerified;

  }


randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  // exportToExcel() {
  //   this.excelService.exportAsExcelFile(this.emptyArray, 'Payment' + '_' + this.randomName());
  // }
 
  buttonState() {
    return !this.emptyArray.some( _=>_ .state);
  }
 
}
