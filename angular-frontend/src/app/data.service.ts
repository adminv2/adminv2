import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

import { baseUrl } from './app.config';
//console.log("baseUrl", baseUrl);
@Injectable()
export class DataService {
  result: any;
  client: any;
  ip: any;
  pagetype: any;
  useraccess: any;


  constructor(private _http: Http, private router: Router) { }

  getUsersInfo() {
    return this._http.get(baseUrl + "user/usersInfo")
      .map(result => result.json());
  }

  getInrOfUsd(){
    return this._http.get("http://api.fixer.io/latest?base=USD")
    .map(result => result.json());
  }

  getKyc(isVerified, p, search, country) {
    return this._http.post(baseUrl + "userKyc/getKyc/" + isVerified + "/" + p + "/" + country, { search: search })
      .map(result => result.json());
  }

  payJoiningBonus(client_id,usd_inr){
    return this._http.post(baseUrl + "deposits/payJoiningBonus", {client_id: client_id, usd_inr:usd_inr})
      .map(result => result.json());
  }

  changeDocumentStatus(id, status, doc_type, reason,changed_by) {
    let data = {
      id: id,
      status: status,
      doc_type: doc_type,
      reason: reason,
      changed_by : changed_by
    };
    return this._http.post(baseUrl + "common/ChangeStatus", data)
      .map(result => result.json());
  }

  changeInrDepositStatus(client_id, id, status, amount, changed_by) {
    let data = {
      client_id: client_id,
      id: id,
      status: status,
      amount: amount,
      changed_by : changed_by
    };
    return this._http.post(baseUrl + "deposits/changeInrDepositStatus", data)
      .map(result => result.json());
  }

  get_IP() {
    return this._http
      .get('http://freegeoip.net/json/?callback')
      .map(response => response.json());
  }

  authntication(data) {
    return this._http.post(baseUrl + 'user/login', data)
      .map(result => result.json());
  }

  logout(username, email) {
    let data = {
      username: username,
      email: email
    };
    return this._http.post(baseUrl + 'user/logout', data)
      .map(result => result.json());
  }

  register(data) {
    return this._http.post(baseUrl + 'user/register', data)
      .map(result => result.json());
  }

  getSupportTeam() {
    return this._http.post(baseUrl + 'user/getsupportUsers', ' ')
      .map(result => result.json());
  }

  getUserAccess(id) {
    let data = {
      id: id,
    };
    return this._http.post(baseUrl + 'user/getUserAccess', data)
      .map(result => result.json());
  }

  ChangeUserAccess(id, access, type) {
    let data = {
      id: id,
      access: access,
      type: type,
    };
    console.log(data)
    return this._http.post(baseUrl + 'user/ChangeUserAccess', data)
      .map(result => result.json());
  }

  authCheck(pagetype) {
    this.useraccess = JSON.parse(localStorage.getItem('useraccess'));
    // console.log("access is", this.useraccess);
    let user = JSON.parse(localStorage.getItem('current'));
    if (JSON.parse(localStorage.getItem('current')) == null && this.useraccess[pagetype] == 1) {
      this.router.navigate(['/']);
    }
  }

  getDeposits(currency, status, payU,withdrawal_type, page,sort, search) {
    //  console.log("sorttttt",sort);
    return this._http.post(baseUrl + "deposits/getDeposits/" + currency + "/" + status + "/" + payU + "/" + withdrawal_type + "/" + page , { search: search,sort:sort })
      .map(result => result.json());
  }

  getDailyInterest(page, search, sort) {
    return this._http.post(baseUrl + "interests/daily-interest", { pageUrl: page, search: search, sort:sort })
      .map(result => result.json());
  }
  getReferral(page, search) {
    return this._http.post(baseUrl + "ref/referals", { pageUrl: page , search : search})
      .map(result => result.json());
  }
  balanceList(pageNo, search, sort) {
    return this._http.post(baseUrl + "bal/balance_info",{pageNo: pageNo, search : search, sort:sort})
      .map(result => result.json().data);
  }
  // balanceList(pageNo, search) {
  //   return this._http.get(baseUrl + 'bal/balance_info/' + pageNo, '/' + search)
  //     .map(result => result.json().data);
  // }

  // sendMailToSingleSome(data) {
  //   return this._http.post(baseUrl + 'email/sendMailToSingleSome', data)
  //     .map(result => result.json());
  // }
  emailing(data) {
    return this._http.post(baseUrl + 'email/service', data)
      .map(result => result.json());
  }
  emailing_batchwise(data) {
    return this._http.post(baseUrl + 'email/emailing_batchwise', data)
      .map(result => result.json());
  }

  saveManualDeposit(data) {
    return this._http.post(baseUrl + "deposits/saveManualDeposits", data)
      .map(result => result.json());
  }
  saveDailyInterest(data) {
    return this._http.post(baseUrl + "interests/saveDailyInterest", data)
      .map(result => result.json());
  }

  customNotifications(data) {
    console.log("customNotifications", data);
    return this._http.post(baseUrl + "notifications/notify", data)
      .map(result => result.json());
  }

 
  getclientEmail(client) {
    let data = {
      client_id: client,
    };
    return this._http.post(baseUrl + 'profile/getEmailByClient', data)
      .map(result => result.json());
  }
  getclientName(client) {
    let data = {
      client_id: client,
    };
    return this._http.post(baseUrl + 'profile/getClientName', data)
      .map(result => result.json());
  }
  getTrades(searchByType,currencyType,client_id,page) {
    let data = {
      search : client_id,
      currency: currencyType,
      searchByType :searchByType,
      page:page
    };
    return this._http.post(baseUrl + 'tradeUser/TradeList', data)
      .map(result => result.json());
  }
 
  checkUserExists(client_id){
    return this._http.post(baseUrl + 'profile/checkUserExists', {client_id: client_id})
      .map(result => result.json());
  }

  get_transactions(limit) {
    return this._http.get('https://data.ripple.com/v2/accounts/rfZie1cED36oaCsR8xMD1rShNFtvFUi3aM/transactions?type=Payment&result=tesSUCCESS&limit='+limit)
    .map(result => result.json());
  }

  getCurrencyPairs(){
    return this._http.post(baseUrl + 'common/getCurrencyPairs', '')
    .map(result => result.json());
  }
  saveUpdateCurrency(currency, objId){
    return this._http.post(baseUrl + 'common/saveUpdateCurrency', {'currency': currency, 'objId': objId})
    .map(result => result.json());
  }
  // getALlTradesData(client_id){
  //   return this._http.post(baseUrl + 'tradeUser/getALlTrades', {'client_id': client_id})
  //   .map(result => result.json());
  // }
  getSearchedData(data){
    let datas = {
      client_id : data.client_id,
      start_date : data.start_date,
      end_date :data.end_date,
      search_for :data.search_for
    }
    return this._http.post(baseUrl + 'admin/getSearchedData', datas)
    .map(result => result.json());
  }


  // for email template listings
  getFileNames(){    
    return this._http.post(baseUrl + 'email/getFileNames', '')
    .map(result => result.json());
  }


}
