var db = require('../db').db;
var sha256 = require('js-sha256');
module.exports.User = function(req,res){
    var condition = { 'username': req.body.username , 'password' : sha256(req.body.password)};
    db.adminUser.find(condition).exec(function(err,result){
        if (result.length > 0) {
            db.adminUser.update(condition, { $set: { is_login: 1, login_from: Date() } }, (err, status) => { });
            res.status(200).send({ code: "152" });
            } else {
            res.status(200).send({ code: "153" });
          }
    })       
};

module.exports.UserLogout = function(req, res){
    db.adminUser.update({ $set: { is_login: 0, login_from: Date() } }, (err, status) => { 
        res.send({ code: "152" });
    });
}

// module.exports.userRegister = function(req, res){
//     console.log(req.body);
// }