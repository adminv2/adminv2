var express = require('express');
var routes = express.Router();
//var db = require('../db').db;
var sha256 = require('js-sha256');
var ObjectId = require('mongodb').ObjectID;
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();
// const commonClassObject = require('../common.js');

// var commonClass = new commonClassObject();
routes.post('/login', commonClass.connect, (req, res) => {
    if (!req.body.username || !req.body.password) {
        res.status(201).send({ code: 202, message: 'all fields requier' });
        return;
    }
    // console.log(sha256(req.body.password));



    var condition = { 'username': req.body.username, 'password': sha256(req.body.password) };
    req.db.collection('admin_users').findOne(condition, (err, result) => {
        if (result) {
            req.db.collection('admin_users').update(condition, { $set: { is_login: 1, login_from: Date(), login_ip: req.body.login_ip } }, (err, status) => {
                if (status) {
                    req.db.collection('admin_accesses').findOne({ 'user': ObjectId(result._id) }, (err, accessdata) => {
                        res.status(200).send({ code: 152, userData: result, accessdata: accessdata });
                    });
                } else {
                    res.status(202).send({ code: 153 });
                }
            });
        } else {
            res.status(202).send({ code: 153 });
        }
    });
});

routes.post('/logout', commonClass.connect, (req, res) => {
    req.db.collection('admin_users').update({ 'username': req.body.username, 'email': req.body.email }, { $set: { is_login: 0, login_from: Date() } }, (err, status) => {
        if (status) {
            res.send({ code: "152" });
        } else {
            res.send({ code: "153" });
        }
    });
});
routes.post('/commonInsert', (req, res) => {
    common_insert.bind(req.query.name);
    common_insert[req.query.name].insert(req.body, function (err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(data);
        }
    })

});

routes.get('/getDeposits', commonClass.connect, (req, res) => {
    console.log("hello");
    req.db.collection('users').find({ "client_id": 17053 }, { balances: { $elemMatch: { currency: 'btc' } }, email: 1 }).forEach(function (err, result) {
        console.log(err, result);
    })
});

routes.post('/register', commonClass.connect, (req, res) => {



    req.db.collection('admin_users').insert({
        f_name: req.body.f_name,
        l_name: req.body.l_name,
        username: req.body.username,
        email: req.body.email,
        password: sha256(req.body.password),
        login_ip: req.body.login_ip,
        description: req.body.description,
        created: new Date(),
        type: req.body.type,
        active: 1,
        is_login: 0,
    }, (err, data) => {
        if (err) {
            console.log(err.message);
            res.status(400).json({ code: 153, message: "Registration Failed, Please Try Again" });
        } else {
            var save_data = {
                added_by: ObjectId(req.body.added_by),
                date: new Date(),
                type: "admin_user",
                admin_type: req.body.type,
                f_name: req.body.f_name,
                l_name: req.body.l_name,
                email: req.body.email
            }
            req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })
        
            req.db.collection('admin_accesses').insert({
                user: data.ops[0]._id,
                DepositINR: req.body.DepositINR,
                WithdrawINR: req.body.WithdrawINR,
                Balances: req.body.Balances,
                DailyIntrest: req.body.DailyIntrest,
                Referrals: req.body.Referrals,
                Documents: req.body.Documents,
                DepositBTC: req.body.DepositBTC,
                WithdrawBTC: req.body.WithdrawBTC,
                MarketOrders: req.body.MarketOrders,
            }, (err, data) => {
                if (err) {
                    console.log(err.message);
                    res.status(400).json({ code: 153, message: "Registration Failed, Please Try Again" });
                } else {
                    res.status(200).json({ code: 152, message: "Successfully registered" });
                }
            });
        }
    });
});

routes.post('/getsupportUsers', commonClass.connect, (req, res) => {
    req.db.collection('admin_users').find({}).toArray((err, status) => {
        if (status) {
            res.status(200).json({ code: 152, users: status })
        } else {
            res.status(201).json({ code: 153 })
        }
    })
})

routes.post('/getUserAccess', commonClass.connect, (req, res) => {
    req.db.collection('admin_accesses').findOne({ "user": ObjectId(req.body.id) }, function (err, data) {
        if (data) {
            res.status(200).json({ code: 152, data: data })
        } else {
            res.status(201).json({ code: 153 })
        }
    })
})

routes.post('/ChangeUserAccess', commonClass.connect, (req, res) => {
    if (!req.body.id || !req.body.access || !req.body.type) {
        res.status(201).send({ code: 202, message: 'all fields requier' });
        return;
    }
    var name = req.body.type;
    var value = req.body.access;
    var query = {};
    query[name] = value;
    req.db.collection('admin_accesses').update({ 'user': ObjectId(req.body.id) }, { $set: query }, (err, status) => {
        if (status) {
            res.send({ code: "152" });
        } else {
            res.send({ code: "153" });
        }
    });
})

module.exports = routes;
