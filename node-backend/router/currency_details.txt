{
    "_id" : ObjectId("5b1f53cfebd23e2e9be5705c"),
    "currencies" : [ 
        "btc", 
        "btg", 
        "bch", 
        "inr", 
        "ltc", 
        "xrp", 
        "thb", 
        "xlm", 
        "pura", 
        "dash", 
        "add", 
        "eth"
    ],
    "withdrawals" : {
        "btc" : "send_tx_btcs"
    },
    "con_deposits" : {
        "btc" : "rec_tx_conf_btcs"
    },
    "uncon_deposits" : {
        "btc" : "rec_tx_unconf_btcs"
    },
    "trade_pairs" : [ 
        "bch_btc", 
        "bch_inr", 
        "bch_thb", 
        "btc_inr", 
        "btc_thb", 
        "btg_btc", 
        "btg_inr", 
        "btg_thb", 
        "bch_btc", 
        "ltc_btc", 
        "ltc_inr", 
        "ltc_thb", 
        "xlm_btc", 
        "xlm_inr", 
        "xlm_thb", 
        "xrp_btc", 
        "xrp_inr", 
        "xrp_thb", 
        "pura_btc", 
        "pura_inr", 
        "pura_thb", 
        "omg_btc", 
        "omg_inr", 
        "omg_thb", 
        "dash_btc", 
        "dash_inr", 
        "dash_thb", 
        "ada_btc", 
        "ada_inr", 
        "ada_thb"
    ]
}