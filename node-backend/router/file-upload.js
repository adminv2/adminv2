var express = require('express');
var router = express.Router();
var multer = require('multer');
var async = require('async');
var _ = require('lodash');
var xlstojson = require("xls-to-json-lc");
var notification = require('../notify.js');
var sheetName;
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();
var handlebars = require('handlebars');
var fs = require('fs');



var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        name_type = file.originalname.split('.')[1];
        if (name_type == 'html') {
            //cb(null, './email_templates/')
             cb(null, '/home/ubuntu/adminv2/node-backend/email_templates/')
        } else if (name_type == 'xls' || name_type == 'xlsx') {
            //cb(null, './uploads/')
             cb(null, '/home/ubuntu/adminv2/node-backend/uploads/')
        } else {
            //cb(null, './email_templates/images/')
             cb(null, '/home/ubuntu/adminv2/node-backend/email_templates/images/')
        }

    },
    filename: function (req, file, cb) {
        name_type = file.originalname.split('.')[1];
        if (name_type == 'xls' || name_type == 'xlsx') {
            var datetimestamp = Date.now();
            cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
        } else {
            cb(null, file.originalname)
        }
    }
});


function randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}
var upload = multer({ //multer settings
    storage: storage,
    fileFilter: function (req, file, callback) { //file filter
        sheetName = file.originalname.split('.')[0];
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(new Error('Wrong extension type'));
        }
        callback(null, true);
    }
}).single('file');

router.post('/upload', commonClass.connect, function (req, res) {
    var exceltojson;
    upload(req, res, function (err) {
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        /** Multer gives us file info in req.file object */
        if (!req.file) {
            res.json({ error_code: 1, err_desc: "No file passed" });
            return;
        }
        /** Check the extension of the incoming file and
         *  use the appropriate module
         */
        if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
            console.log(req.file.originalname);
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }
        try {
            exceltojson({
                input: req.file.path,
                output: null, //since we don't need output.json
                lowerCaseHeaders: true
            }, function (err, result) {
                if (err) {
                    return res.json({ error_code: 1, err_desc: err, data: null });
                }
                var sheetArray = [];
                var count = 0;
                var sum = 0;
                grouped = [];
                result.forEach(function (o) {
                    if (!this[o.client_id]) {
                        this[o.client_id] = { client_id: o.client_id, amount: 0 };
                        grouped.push(this[o.client_id]);
                        count++;
                    }
                    this[o.client_id].amount += Number(o.amount).toFixed(12);
                }, Object.create(null));
                // result.forEach((element, key) => {
                async.forEachSeries(result, function (element, next) {
                    var adding_amount;
                    adding_amount = parseFloat(element.amount);
                    console.log(typeof parseInt(element.client_id))
                    req.db.collection('users').findOne({ "client_id": parseInt(element.client_id) }, { balances: { $elemMatch: { currency: 'btc' } } }, function (er, singleRec) {
                        if (err) {
                            console.log("payout");
                            next()
                        } else {

                            if (singleRec && singleRec.balances[0]) {
                                var curBal = parseFloat(singleRec.balances[0].current_balance) + adding_amount;
                                var prevBal = parseFloat(singleRec.balances[0].previous_balance) + adding_amount;
                                req.db.collection('users').update(
                                    { 'client_id': parseInt(element.client_id), 'balances.currency': 'btc' },
                                    {
                                        $set: {
                                            'balances.$.previous_balance': prevBal.toFixed(12), 'balances.$.current_balance': curBal.toFixed(12)
                                        }
                                    },
                                    { multi: true },
                                    (errUpdate, statusUpdate) => {

                                        //console.log(statusUpdate)
                                        //console.log(errUpdate)

                                        if (statusUpdate) {
                                            console.log("statusUpdate.result " + element.client_id, statusUpdate.result);
                                            if (statusUpdate.result.n === 1 && statusUpdate.result.nModified === 1 && statusUpdate.result.ok === 1) {
                                                var pastValue = parseFloat(singleRec.balances[0].current_balance);
                                                var Update_balance = pastValue + adding_amount;
                                                var mail_content_admin = Number(adding_amount).toFixed(12) + ' BTC Deposited To Client ID:' + element.client_id + ", 'User's balance has been changed from " + pastValue.toFixed(12) + " to " + Update_balance.toFixed(12);
                                                var admin_subject = element.client_id + " BTC Deposited " + Number(adding_amount).toFixed(12);
                                                fs.readFile("email_templates/payout.html", { encoding: 'utf-8' }, function (error, pgResp) {
                                                    var template = handlebars.compile(pgResp);
                                                    var replacementsAdmin = {
                                                        mail_content: mail_content_admin,
                                                        full_name: 'Admin'
                                                    };
                                                    var htmlToSendAdmin = template(replacementsAdmin);
                                                    commonClass.mail('akashbtcmonk@mailinator.com', admin_subject, htmlToSendAdmin);
                                                });
                                                sheetArray.push({ client_id: element.client_id, pastValue: pastValue.toFixed(12), addingbal: element.amount, Update_balance: Update_balance.toFixed(12) });
                                                // next()
                                            } else {
                                                sheetArray.push({ client_id: element.client_id, pastValue: 'not update', addingbal: element.amount, Update_balance: 'not update' });
                                                // next()
                                            }
                                        } else {
                                            sheetArray.push({ client_id: element.client_id, pastValue: 'not update', addingbal: element.amount, Update_balance: 'not update' });
                                            // next()
                                        }




                                        if (result.length == sheetArray.length) {
                                            _.find(sheetArray, function (trns) {
                                                req.db.collection("users").findOne({ "client_id": parseInt(trns.client_id) }, { email: true, device_addresses: true }, function (err, notify) {
                                                    if (notify) {
                                                        var histObj = {};
                                                        histObj.client_id = parseInt(trns.client_id);
                                                        histObj.amount = Number(trns.addingbal).toFixed(12);
                                                        if (sheetName === "Lending_Withdrawal") {
                                                            histObj.txid = sheetName + "_" + trns.client_id + "_" + ID();
                                                        } else {
                                                            histObj.txid = sheetName + "_" + trns.client_id + "_" + ID();
                                                        }
                                                        histObj.sent_address = "admin";
                                                        histObj.transaction_date = new Date();
                                                        histObj.created = new Date();
                                                        histObj.updated = new Date();
                                                        histObj.withdrawal_type = "external";
                                                        histObj.confirmations = "3";
                                                        histObj.status = "1";
                                                        histObj.currency = "btc";
                                                        histObj.type = "receive"
                                                        req.db.collection("rec_tx_con_btcs").insertOne(histObj, function (err, res) {
                                                            if (err) {
                                                                console.log("dipoist history-error", err, res);
                                                            } else {
                                                                console.log("dipoist history");
                                                                histObj = {};
                                                            }
                                                            // next()
                                                        })
                                                    } else {
                                                        console.log("No data for this client_id:", trns.client_id);
                                                        // next()
                                                    }

                                                })
                                            })
                                            res.json({ code: 200, data: sheetArray });
                                        } else {
                                            next()
                                        }
                                    })
                            } else {
                                console.log(singleRec)
                                console.log(er)
                                console.log("no record found for this client", element.client_id);
                                next()
                            }

                        }


                    })

                });
            });
        } catch (e) {
            res.json({ error_code: 1, err_desc: "Corupted excel file" });
        }
    })
});
function uniqueNumber() {
    var date = Date.now();
    if (date <= uniqueNumber.previous) {
        date = ++uniqueNumber.previous;
    } else {
        uniqueNumber.previous = date;
    }
    return date;
}
uniqueNumber.previous = 0;
function ID() {
    return uniqueNumber();
};



// This method is to upload admin email template
router.post('/addMailTemplate', commonClass.connect, function (req, res) {
    var exceltojson;
    uploadTemplate(req, res, function (err) {
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }

    })
});
var uploadTemplate = multer({ //multer settings
    storage: storage,
    fileFilter: function (req, file, callback) { //file filter
        console.log("file", file.originalname);
        sheetName = file.originalname.split('.')[0];
        if (['html', 'gif', 'jpeg', 'jpg', 'png'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(new Error('Wrong extension type'));
        }
        callback(null, true);
    }
}).single('file');




module.exports = router;



// 117355
// 117427