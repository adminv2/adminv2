var express = require('express');
var routes = express.Router();
//var db = require('../db').db;
var ObjectId = require('mongodb').ObjectID;
var notification = require('../notify.js');
var handlebars = require('handlebars');
var fs = require('fs');
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

function commonQuery(table, page, search, res, condition, user, sortAscDesc) {

    var sorting = {};
    var sort_by = sortAscDesc.sortField;
    var sort_in_order = Number(sortAscDesc.sortType);
    sorting[sort_by] = sort_in_order;

    var index = 0;
    var array = [];
    var perPage = 10, page = Math.max(0, page);
    var skip = (perPage * page) - perPage;

    if (search != '') {
        condition.$or.push(
            { client_id: parseInt(search) },
            { txid: new RegExp(search, 'i') },
        )
    } else {
        condition.$or.push(
            { client_id: { $nin: [''] } },
        )
    }

    table.count(condition, (errCount, statusCount) => {
        table.find(condition).skip(skip).limit(perPage).sort(sorting).toArray(function (err, status) {
            if (status && status.length != '') {
                userDetails(index, status, user, array, (data) => {
                    res.status(200).json({ code: 152, users_withdrawa: data, count: statusCount })
                })
            }
            else {
                res.status(201).json({ code: 153, users_withdrawa: [], count: 0 })
            }
        })
    })
}

function userDetails(index, status, user, array, cb) {
    var currency = status[index].currency.toLowerCase();
    var balances = [];
    user.findOne({ 'client_id': parseInt(status[index].client_id) }, { bank_details: true, balances: { $elemMatch: { 'currency': currency } } }, (errUpdate, balancesUpdate) => {
        if (balancesUpdate == null) {
            balances.push({ registered_name: ' ', account_number: ' ', ifsc_code: ' ', previous_balance: ' ' })
            status[index]['bankDetails'] = balances
        } else {
            balances.push({
                registered_name: (balancesUpdate.bank_details.length != 0) ? balancesUpdate.bank_details[balancesUpdate.bank_details.length - 1].registered_name : ' ',
                account_number: (balancesUpdate.bank_details.length != 0) ? balancesUpdate.bank_details[balancesUpdate.bank_details.length - 1].account_number : ' ',
                ifsc_code: (balancesUpdate.bank_details.length != 0) ? balancesUpdate.bank_details[balancesUpdate.bank_details.length - 1].ifsc_code : ' ',
                previous_balance: balancesUpdate.balances[0].current_balance
            });
            status[index]['bankDetails'] = balances
        }
        array.push(status)
        if (array.length == status.length) {
            cb.call(cb, status)
        } else {
            index++;
            balances = [];
            userDetails(index, status, user, array, cb)
        }
    })
}
routes.post('/changesWithdrawalStatus', commonClass.connect, (req, res) => {
    if (!req.body.id || !req.body.status || !req.body.currency) {
        res.status(201).send({ code: 202, message: 'all fields requier' });
        return;
    }
    if (req.body.status == 1 && (req.body.currency == 'inr' || req.body.currency == 'thb')) {
        req.db.collection('withdrawamounts').findOneAndUpdate({ "_id": ObjectId(req.body.id) }, { $set: { status: req.body.status, txid: req.body.txnId } }, (err, result) => {
            if (result) {
                req.db.collection('users').findOne({ 'client_id': result.value.client_id }, { email: true, full_name: true, device_addresses: true, bank_details: true }, (errMail, balancemail) => {
                    if (balancemail) {

                        req.db.collection('statistics').find({}).toArray((errs, results) => {
                            var today_confirm = Number(results[0].withdrawal_status_wise.today_inr_confirm) + Number(result.value.amount);
                            var total_confirm = Number(results[0].withdrawal_status_wise.total_inr_confirm) + Number(result.value.amount);

                            // var today_confirm = today_confirm.toFixed(12);
                            // var total_confirm = total_confirm.toFixed(12);

                            req.db.collection('statistics').update({}, { $set: { 'withdrawal_status_wise.today_inr_confirm': today_confirm, 'withdrawal_status_wise.total_inr_confirm': total_confirm } }, (error, result) => { })

                            var today_withdrawal_inr = Number(results[0].todayWithdraw.inr) + Number(result.value.amount);
                            var total_withdrawal_inr = Number(results[0].totalWithdraw.inr) + Number(result.value.amount);

                            // var today_withdrawal_inr = today_withdrawal_inr.toFixed(12);
                            // var total_withdrawal_inr = total_withdrawal_inr.toFixed(12);

                            req.db.collection('statistics').update({}, { $set: {'todayWithdraw.inr': today_withdrawal_inr, 'totalWithdraw.inr': total_withdrawal_inr} }, (error, result) => { })
                            req.db.collection('statistics').update({}, { $inc: {'todayWithdrawCount.inr': 1, 'totalWithdrawCount.inr': 1 } }, (error, result) => { })

                            var fields = {
                                admin_id: ObjectId(req.body.changed_by),
                                type: "wiithdrawal",
                                status: 1,
                                order_id: ObjectId(req.body.id),
                                date: new Date()
                            }
                            req.db.collection('track_admin_changes').insert(fields, (err, data) => { })

                        })



                        var mail_content = "Your withdraw Request for  " + req.body.currency + ' ' + result.value.amount + " amount has been approved. your transition id : " + req.body.txnId;
                        fs.readFile("email_templates/bankWithdraw.html", { encoding: 'utf-8' }, function (error, pgResp) {
                            var template = handlebars.compile(pgResp);
                            var replacements = {
                                mail_content: mail_content,
                                full_name: balancemail.full_name,
                                amount: result.value.amount,
                                currency: req.body.currency,
                                status: 'Approved',
                                bank_name: (balancemail.bank_details.length != 0) ? balancemail.bank_details[balancemail.bank_details.length - 1].bank_name : ' ',
                            };
                            var htmlToSend = template(replacements);
                            commonClass.mail(balancemail.email, 'Withdrawal Request Approved', htmlToSend);
                        });
                        if (result.value.client_id && req.body.currency == 'inr') {
                            commonClass.sendingNotifications(result.value.client_id, balancemail.device_addresses, 'inr', "Your withdraw request has been initiated. The amount " + "Rs." + result.value.amount + " will be credited within 24 hours", "Withdraw Status", req.db);
                        }
                        res.status(200).json({ code: 152 })
                    } else {
                        res.status(201).json({ code: 153 })
                    }
                });
            } else {
                res.status(201).json({ code: 153 })
            }
        });

    } else if (req.body.status == 2 && (req.body.currency == 'inr' || req.body.currency == 'thb')) {
        req.db.collection('withdrawamounts').findOneAndUpdate({ "_id": ObjectId(req.body.id) }, { $set: { status: req.body.status, txid: "Canceled" } }, (err, withdrawalsdata) => {
            if (withdrawalsdata) {
                if (withdrawalsdata.value.currency == 'inr' || withdrawalsdata.value.currency == 'thb') {
                    req.db.collection('users').findOne({ 'client_id': withdrawalsdata.value.client_id }, { 'balances': { $elemMatch: { currency: withdrawalsdata.value.currency } }, email: true, full_name: true, device_addresses: true, bank_details: true }, (err, balancesdata) => {
                        if (balancesdata) {
                            var updated_prevbalance = (balancesdata.balances[0].previous_balance - 0) + (withdrawalsdata.value.amount - 0) + (withdrawalsdata.value.fee);
                            var updated_currentvbalance = (balancesdata.balances[0].current_balance - 0) + (withdrawalsdata.value.amount - 0) + (withdrawalsdata.value.fee);

                            var updated_prevbalance = updated_prevbalance.toFixed(12);
                            var updated_currentvbalance = updated_currentvbalance.toFixed(12)

                            var amount_to_refund = Number(withdrawalsdata.value.amount) + Number(withdrawalsdata.value.fee);
                            var amount_to_refund = amount_to_refund.toFixed(2);

                            req.db.collection('users').findOneAndUpdate({ 'client_id': withdrawalsdata.value.client_id, 'balances.currency': withdrawalsdata.value.currency }, { $set: { 'balances.$.previous_balance': updated_prevbalance, 'balances.$.current_balance': updated_currentvbalance } }, (errUpdate, balancesUpdate) => {
                                if (balancesUpdate) {

                                    req.db.collection('statistics').find({}).toArray((errs, results) => {
                                        var today_cancelled = Number(results[0].withdrawal_status_wise.today_inr_cancelled) + Number(withdrawalsdata.value.amount);
                                        var total_cancelled = Number(results[0].withdrawal_status_wise.total_inr_cancelled) + Number(withdrawalsdata.value.amount);

                                        // var today_cancelled = today_cancelled.toFixed(12);
                                        // var total_cancelled = total_cancelled.toFixed(12)

                                        req.db.collection('statistics').update({}, { $set: { 'withdrawal_status_wise.today_inr_cancelled': today_cancelled, 'withdrawal_status_wise.total_inr_cancelled': total_cancelled } }, (error, result) => { })

                                        var fields = {
                                            admin_id: ObjectId(req.body.changed_by),
                                            type: "wiithdrawal",
                                            status: 2,
                                            order_id: ObjectId(req.body.id),
                                            date: new Date()
                                        }
                                        req.db.collection('track_admin_changes').insert(fields, (err, data) => { })
                                    })

                                    var mail_content = "Your withdraw Request for  " + req.body.currency + ' ' + amount_to_refund + " has been Canceled.";
                                    fs.readFile("email_templates/bankWithdraw.html", { encoding: 'utf-8' }, function (error, pgResp) {
                                        var template = handlebars.compile(pgResp);
                                        var replacements = {
                                            mail_content: mail_content,
                                            full_name: balancesdata.full_name,
                                            amount: amount_to_refund,
                                            currency: req.body.currency,
                                            status: 'Canceled',
                                            bank_name: (balancesdata.bank_details.length != 0) ? balancesdata.bank_details[balancesdata.bank_details.length - 1].bank_name : ' ',
                                        };
                                        var htmlToSend = template(replacements);
                                        commonClass.mail(balancesdata.email, 'Withdrawal Request Canceled', htmlToSend);
                                    });
                                    if (withdrawalsdata.value.client_id) {
                                        commonClass.sendingNotifications(withdrawalsdata.value.client_id, balancesdata.device_addresses, 'inr', "Your withdraw request has been cancelled. Please contact support for more information", "Withdraw Status", req.db);
                                    }
                                    res.status(200).json({ code: 152 }) //success
                                } else {
                                    res.status(201).json({ code: 153 }) // failed
                                }
                            })
                        }
                        else {
                            res.status(200).json({ code: 154 }) // failed
                        }
                    })
                }
            } else {
                console.log("in else");

                res.status(201).json({ code: 153 })
            }
        });
    }
})

routes.post('/saveManualWithdrawals', commonClass.connect, (req, res) => {
    if (req.body.currency == 'inr') {
        req.body.status = 0
    } else {
        req.body.status = 1;
    }
    switch (req.body.currency) {
        case "inr":
            var schema = "withdrawamounts";
            var fields = {
                client_id: parseInt(req.body.client_id),
                currency: req.body.currency,
                amount: Number(req.body.amount).toFixed(12),
                fee: 10,
                country: req.body.country,
                created: new Date(req.body.transaction_date),
                status: parseInt(req.body.status),
                comments: req.body.comments,
                otp: {
                    otpNumber: Math.random(999999, 1000000),
                    status: 1,
                    date: new Date()
                },
                txid: ''
            }
            var save_data = {
                added_by: ObjectId(req.body.changed_by),
                type: "manual_withdrawal",
                status: 1,
                client_id: parseInt(req.body.client_id),
                amount: Number(req.body.amount).toFixed(12),
                currency: req.body.currency,
                date: new Date()
            }
            break;
    }

    if (parseInt(req.body.status) == 0) {
        req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { balances: { $elemMatch: { currency: req.body.currency } }, email: true, full_name: true }, (err, status) => {
            if (status) {
                if (Number(status.balances[0].current_balance) > Number(req.body.amount)) {
                    var total_previous = Number(status.balances[0].previous_balance) - Number(req.body.amount);
                    var total_current = Number(status.balances[0].current_balance) - Number(req.body.amount);
                    if (req.body.currency == 'inr') {
                        var total_previous = Number(total_previous).toFixed(12)
                        var total_current = Number(total_current).toFixed(12)
                    } else {
                        var total_previous = Number(total_previous).toFixed(12)
                        var total_current = Number(total_current).toFixed(12)
                    }
                    req.db.collection('users').update({ 'client_id': parseInt(req.body.client_id), 'balances.currency': req.body.currency }, { $set: { 'balances.$.previous_balance': total_previous, 'balances.$.current_balance': total_current } }, (errUpdate, statusUpdate) => {
                        if (statusUpdate) {
                            var mail_content = "Hello " + status.full_name + ", <br> Withdraw Request for " + req.body.amount + " " + req.body.currency + " has been created for you, <br> your balance has been changed from " + status.balances[0].current_balance + " to " + total_current + " " + req.body.currency;
                            commonClass.mail(status.email, 'Withdrawal Request Initiated', mail_content);
                            req.db.collection(schema).insert(fields, (err, data) => {
                                if (err) {
                                    res.status(400).json({ code: 151, message: "Something Went wrong while Initiating Request" });
                                } else {
                                    res.status(200).json({ code: 152, message: "Withdraw Request Initiated for " + req.body.amount + " " + req.body.currency + " to Client ID " + req.body.client_id });
                                }
                            })
                        }

                        req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })

                    })
                } else {
                    res.status(204).json({ code: 152, message: "This user does not have Enough balance to withdraw" });
                }
            } else {
                res.status(200).json({ code: 153, message: "Something Went Wrong" });
            }
        });
    } else {
        res.status(200).json({ code: 153, message: "Direct Withdraw ho gaya" });
    }
})




routes.post('/:currency/:status/:page/:withdrawal_type', commonClass.connect, (req, res) => {
    var sortAscDesc = req.body.sort;
    // console.log("sort like this",sortAscDesc);
    var withdrawal_type = parseInt(req.params.withdrawal_type);
    if (withdrawal_type == 1) {
        var withd_type = 'external';
    } else if (withdrawal_type == 2) {
        var withd_type = 'internal';
    }

    if (!req.params.currency || !req.params.status) {
        res.status(201).send({ code: 202, message: 'all fields requiered' });
        return;
    };
    var status = req.params.status;
    var page = req.params.page;


    if (status == 'verified') {
        var condition = { 'status': 1, $or: [] };
        if (withdrawal_type != 0) {
            var condition = { 'withdrawal_type': withd_type, 'status': 1, $or: [] };
        }
    } else if (status == 'pending') {
        var condition = { 'status': 0, $or: [] };
        if (withdrawal_type != 0) {
            var condition = { 'withdrawal_type': withd_type, 'status': 0, $or: [] };
        }
    } else if (status == 'canceled') {
        var condition = { 'status': 2, $or: [] };
        if (withdrawal_type != 0) {
            var condition = { 'withdrawal_type': withd_type, 'status': 2, $or: [] };
        }
    } else {
        var condition = { $or: [] };
        if (withdrawal_type != 0) {
            var condition = { 'withdrawal_type': withd_type, $or: [] };
        }
    }

    if (req.params.currency == 'inr') {
        if (req.params.status == 'all') {
            condition1 = { 'otp.status': 1, currency: 'inr', $or: [] }
        } else {
            condition1 = { 'otp.status': 1, status: condition.status, currency: 'inr', $or: [] }
        }
        commonQuery(req.db.collection('withdrawamounts'), page, req.body.search, res, condition1, req.db.collection('users'), sortAscDesc)
    } else if (req.params.currency == 'thb') {
        if (req.params.status == 'all') {
            condition1 = { 'otp.status': 1, $or: [] }
        } else {
            condition1 = { 'otp.status': 1, status: condition.status, currency: 'thb', $or: [] }
        }
        commonQuery(req.db.collection('withdrawamounts'), page, req.body.search, res, condition1, req.db.collection('users'), sortAscDesc)
    }else if(req.params.currency == 'xrp'){
        commonQuery(req.db.collection('xrp_withdrawals'), page, req.body.search, res, condition, req.db.collection('users'), sortAscDesc)
    }else if(req.params.currency == 'xlm'){
        commonQuery(req.db.collection('xlm_withdrawals'), page, req.body.search, res, condition, req.db.collection('users'), sortAscDesc)
    }else {
        if (req.params.currency == 'dash' || req.params.currency == 'bch') {
            var postfix = "es";
        } else if(req.params.currency == 'xrp'){
            commonQuery(req.db.collection('xrp_withdrawals'), page, req.body.search, res, condition, req.db.collection('users'), sortAscDesc)
        }else {
            var postfix = "s";
        }
        commonQuery(req.db.collection('send_tx_' + req.params.currency + postfix), page, req.body.search, res, condition, req.db.collection('users'), sortAscDesc)
    }

});





module.exports = routes

