var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
var handlebars = require('handlebars');
var fs = require('fs');

const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

router.post('/getCurrencyPairs', commonClass.connect, (req, res, next) => {
    var currencies = []
    var data = []
    var trade_pairs = []

    req.db.collection('currencies').find({}).toArray((err, result) => {
        result.forEach((user, key) => {
            currencies.push(
                user.id
            )
        })
        
        req.db.collection('fees_taxes').find({}).toArray((errs, results) => {
            results.forEach((res, key) => {
                trade_pairs.push(
                    res.id
                )
            })
            data.push({
                currencies: currencies,
                trade_pairs: trade_pairs
            })
            res.json({ code: 300, currencies_details: data[0] })
        })
    })
})
// router.post('/getCurrencyPairs', commonClass.connect, (req, res, next) => {
//     req.db.collection('currencies_details').findOne({}, (err, result) => {
//             console.log("in get currency pair", result)
//         // res.json({ code: 300, currencies_details: result })
//     })
// })

router.post('/ChangeStatus', commonClass.connect, (req, res, next) => {
    req.db.collection('users').findOne({ 'client_id': req.body.id },
        {
            'aadhaar_status': true,
            'client_id': true,
            'id_status': true,
            'full_name': true,
            'email': true,
            'country': true,
            'kyc_status': true,
            'bank_status': true,
            'bank_details': true,
            'device_addresses': true,
            'bonus_alloted': true,
        }, (err, status) => {

            if (status) {
                if (status.bonus_alloted != 1) {
                    status.bonus_alloted = 0
                }
                status[req.body.doc_type] = parseInt(req.body.status);
                status.updated = new Date();
                var alloted_just_now_ignore = 0;
                if (!status.country) {
                    status.country == 'india'
                }
                if (status.country.toLowerCase() == 'india') {
                    if ((req.body.doc_type == 'id_status') && (req.body.status != 1)) {
                        var document_type = "ID Details";
                        status.kyc_status = 0;
                        status.withdrawal_limit = 0.5
                        status.withdrawal_progress = 0.5;
                        status.id_number = '';
                    } else if ((req.body.doc_type == 'aadhaar_status') && (req.body.status != 1)) {
                        var document_type = "Aadhar Details";
                        status.kyc_status = 0;
                        status.withdrawal_limit = 0.5
                        status.withdrawal_progress = 0.5;
                        status.aadhaar_number = '';
                    } else if ((req.body.doc_type == 'bank_status') && (req.body.status != 1)) {
                        var document_type = "Bank Details";
                        status.kyc_status = 0;
                        status.withdrawal_limit = 0.5
                        status.withdrawal_progress = 0.5;
                        status.bank_details[status.bank_details.length - 1].account_number = '';
                    } else if ((req.body.doc_type == 'aadhaar_status') && (req.body.status == 1)) {
                        var mail_content = "Your aadhaar details has been verified";
                        status.kyc_status = (status[req.body.doc_type] == 1 && status.id_status == 1 && status.bank_status == 1) ? 1 : 0;
                        if (status.kyc_status == 1) {
                            status.withdrawal_limit = 2;
                            status.withdrawal_progress = 2;
                            if (status.bonus_alloted == 0 && alloted_just_now_ignore == 0) {
                                status.bonus_exp_date = new Date();
                                status.bonus_exp_date.setDate(status.bonus_exp_date.getDate() + 30);
                                status.bonus_alloted = 1
                                var alloted_just_now_ignore = 1;
                            }
                        }
                    } else if ((req.body.doc_type == 'id_status') && (req.body.status == 1)) {
                        var mail_content = "Your ID details has been verified";
                        status.kyc_status = (status[req.body.doc_type] == 1 && status.aadhaar_status == 1 && status.bank_status == 1) ? 1 : 0;
                        if (status.kyc_status == 1) {
                            status.withdrawal_limit = 2;
                            status.withdrawal_progress = 2;
                            if (status.bonus_alloted == 0 && alloted_just_now_ignore == 0) {
                                status.bonus_exp_date = new Date();
                                status.bonus_exp_date.setDate(status.bonus_exp_date.getDate() + 30);
                                status.bonus_alloted = 1
                                var alloted_just_now_ignore = 1;
                            }
                        }

                    } else if ((req.body.doc_type == 'bank_status') && (req.body.status == 1)) {
                        var mail_content = "Your Bank details has been verified";
                        status.kyc_status = (status[req.body.doc_type] == 1 && status.id_status == 1 && status.aadhaar_status == 1) ? 1 : 0;
                        if (status.kyc_status == 1) {
                            status.withdrawal_limit = 2;
                            status.withdrawal_progress = 2;
                            if (status.bonus_alloted == 0 && alloted_just_now_ignore == 0) {
                                status.bonus_exp_date = new Date();
                                status.bonus_exp_date.setDate(status.bonus_exp_date.getDate() + 30);
                                status.bonus_alloted = 1
                                var alloted_just_now_ignore = 1;
                            }
                        }
                    }
                } else {
                    if ((req.body.doc_type == 'id_status') && (req.body.status != 1)) {
                        var document_type = "ID Details";
                        status.kyc_status = 0;
                        status.withdrawal_limit = 0.5
                        status.withdrawal_progress = 0.5;
                        status.id_number = '';
                    } else if ((req.body.doc_type == 'bank_status') && (req.body.status != 1)) {
                        var document_type = "Bank Details";
                        status.kyc_status = 0;
                        status.withdrawal_limit = 0.5
                        status.withdrawal_progress = 0.5;
                        status.bank_details[status.bank_details.length - 1].account_number = '';
                    } else if ((req.body.doc_type == 'id_status') && (req.body.status == 1)) {
                        var mail_content = "Your ID details has been verified";
                        status.kyc_status = (status[req.body.doc_type] == 1 && status.bank_status == 1) ? 1 : 0;
                        if (status.kyc_status == 1) {
                            status.withdrawal_limit = 2;
                            status.withdrawal_progress = 2;
                            if (status.bonus_alloted == 0) {
                                status.bonus_exp_date = new Date();
                                status.bonus_exp_date.setDate(status.bonus_exp_date.getDate() + 30);
                                status.bonus_alloted = 1
                            }
                        }
                    } else if ((req.body.doc_type == 'bank_status') && (req.body.status == 1)) {
                        var mail_content = "Your Bank details has been verified";
                        status.kyc_status = (status[req.body.doc_type] == 1 && status.id_status == 1) ? 1 : 0;
                        if (status.kyc_status == 1) {
                            status.withdrawal_limit = 2;
                            status.withdrawal_progress = 2;
                            if (status.bonus_alloted == 0 && alloted_just_now_ignore == 0) {
                                status.bonus_exp_date = new Date();
                                status.bonus_exp_date.setDate(status.bonus_exp_date.getDate() + 30);
                                status.bonus_alloted = 1
                            }
                        }
                    }

                }
                req.db.collection('users').findOneAndUpdate({ '_id': ObjectId(status._id) }, { $set: status }, (saveErr, saveStatus) => {
                    if (saveStatus) {
                        if (req.body.status == 1) {
                            if (status.kyc_status == 1) {
                                // req.db.collection('statistics').findOne({ '_id': ObjectId("5afed619eb87d42108f4a5dd") }, (err, result) => {
                                req.db.collection('statistics').find({}).toArray((errs, results) => {

                                    var today_verified = results[0].kyc.today_verified + 1;
                                    var total_verified = results[0].kyc.total_verified + 1;

                                    var today_unverified = results[0].kyc.today_unverified
                                    if (status.bonus_alloted == 1) {
                                        var total_unverified = results[0].kyc.total_unverified - 1;
                                        if (today_unverified > 0) {
                                            var today_unverified = results[0].kyc.today_unverified - 1;
                                        } else {
                                            var today_unverified = 0
                                        }
                                    } else {
                                        var total_unverified = results[0].kyc.total_unverified;
                                    }
                                    req.db.collection('statistics').update({}, { $set: { 'kyc.today_verified': today_verified, 'kyc.total_verified': total_verified, 'kyc.today_unverified': today_unverified, 'kyc.total_unverified': total_unverified } }, (error, result) => { })

                                    var fields = {
                                        admin_id: ObjectId(req.body.changed_by),
                                        type: "kyc",
                                        status: 1,
                                        order_id: ObjectId(status._id),
                                        date: new Date()
                                    }
                                    req.db.collection('track_admin_changes').insert(fields, (err, data) => { })

                                })
                            }
                            fs.readFile("email_templates/kycVerified.html", { encoding: 'utf-8' }, function (error, pgResp) {
                                var template = handlebars.compile(pgResp);
                                var replacements = {
                                    mail_content: mail_content,
                                    full_name: status.full_name
                                };
                                var htmlToSend = template(replacements);
                                commonClass.mail(saveStatus.value.email, 'Kyc Updated', htmlToSend);
                                commonClass.sendingNotifications(status.client_id, status.device_addresses, 'kyc', mail_content, "KYC status", req.db)
                            });
                        } else {
                            var count = 0;
                            if (status.id_status == 2) {
                                count = count + 1;
                            } if (status.aadhaar_status == 2) {
                                count = count + 1;
                            } if (status.bank_status == 2) {
                                count = count + 1;
                            }
                            // console.log("total isss", count);

                            if (count == 1) {
                                req.db.collection('statistics').find({}).toArray((errs, result) => {
                                    var today_unverified = result[0].kyc.today_unverified + 1;
                                    var total_unverified = result[0].kyc.total_unverified + 1;
                                    var today_verified = result[0].kyc.today_verified
                                    if (status.bonus_alloted == 1) {
                                        if (today_verified > 0) {
                                            var today_verified = result[0].kyc.today_verified - 1;
                                        } else {
                                            var today_verified = 0
                                        }
                                        var total_verified = result[0].kyc.total_verified - 1;
                                    } else {
                                        var total_verified = result[0].kyc.total_verified;
                                    }
                                    req.db.collection('statistics').update({}, { $set: { 'kyc.today_verified': today_verified, 'kyc.today_unverified': today_unverified, 'kyc.total_unverified': total_unverified, 'kyc.total_verified': total_verified } }, (error, result) => { })
                                })
                            }

                            var fields = {
                                admin_id: ObjectId(req.body.changed_by),
                                type: "kyc",
                                status: 2,
                                order_id: ObjectId(status._id),
                                date: new Date()
                            }
                            req.db.collection('track_admin_changes').insert(fields, (err, data) => { })


                            fs.readFile("email_templates/kycReject.html", { encoding: 'utf-8' }, function (error, pgResp) {
                                var template = handlebars.compile(pgResp);
                                var replacements = {
                                    document_type: document_type,
                                    full_name: status.full_name,
                                    reason: req.body.reason.reason
                                };
                                var htmlToSend = template(replacements);
                                commonClass.mail(saveStatus.value.email, 'Kyc Updated', htmlToSend);
                                commonClass.sendingNotifications(status.client_id, status.device_addresses, 'kyc', "Your " + document_type + " has been Rejected because " + req.body.reason.reason, "KYC status", req.db)
                            });
                        }
                        // console.log("mail sent");
                        res.json({ msg: "success", status: 200 });
                    } else {
                        res.json({ msg: "err", status: 202 });
                    }
                })
            } else {
                res.json({ msg: "err", status: 201 });
            }
        })
})

router.post('/saveUpdateCurrency', commonClass.connect, (req, res, next) => {
    var id = req.body.objId;
    var to_change = "currencies" + "." + id;

    var update = {
        [to_change]: req.body.currency
    }
    var upd = update
    req.db.collection('currencies_details').update({ "_id": ObjectId("5b1f53cfebd23e2e9be5705c") }, { $set: upd }, (errUpdate, statusUpdate) => {
        if (errUpdate) {
            res.status(200).json({ code: 151, message: "Something Went wrong while Updating Currency" });
        } else {
            res.status(200).json({ code: 151, message: "currency Updated Successfully" });
        }
    })
})



function getUsersBalance(req, res) {
    // console.log("here 1");
    req.db.collection('users').findOne({}, (saveErr, saveStatus) => {
        // console.log("here 2");

        router.post('/http://freegeoip.net/json/?callback', commonClass.connect, (req, res, next) => {
            // console.log("reeees", res);
        })

        if (saveStatus) {
            res.json({ msg: "err", status: 202 });
        }
    })
}

module.exports = router;
