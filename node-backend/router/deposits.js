var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
var notification = require('../notify.js');
var handlebars = require('handlebars');
var fs = require('fs');
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

function getDepositsWithSchemaName(schemaName, user, search, page, res, condition = null, sortAscDesc) {
    var sorting = {};
    var sort_by = sortAscDesc.sortField;
    var sort_in_order = Number(sortAscDesc.sortType);

    sorting[sort_by] = sort_in_order;
    var index = 0;
    var array = [];
    var perPage = 10,
        page = Math.max(0, page);
    var skip = (perPage * page) - perPage;
    if (condition != null) {
        if (search != '') {
            condition.$or.push(
                { client_id: parseInt(search) },
                { sent_address: search },
                { withdrawal_type: search },
            )
        } else {
            condition.$or.push(
                { client_id: { $nin: [''] } },
            )
        }
    }
    // console.log("to sprt by",sorting)

    schemaName.count(condition, (errCount, statusCount) => {
        schemaName.find(condition).skip(skip).limit(perPage).sort(sorting).toArray((err, status) => {
            if (status && status.length != '') {
                userDetails(index, status, user, array, (data) => {
                    res.json({ code: 300, depositInr: data, count: statusCount })
                })
            } else {
                res.json({ status: 301 })
            }
        });
    });
}
function userDetails(index, status, user, array, cb) {

    // console.log(status[index].currency);
    if (status[index].currency) {
        var currency = status[index].currency.toLowerCase();
    }
    var balances = [];
    user.findOne({ 'client_id': parseInt(status[index].client_id) }, { bank_details: true, balances: { $elemMatch: { 'currency': currency } } }, (errUpdate, balancesUpdate) => {
        if (balancesUpdate == null) {
            balances.push({ registered_name: ' ', account_number: ' ', ifsc_code: ' ', previous_balance: ' ' })
            status[index]['bankDetails'] = balances
        } else {
            balances.push({
                registered_name: (balancesUpdate.bank_details.length != 0) ? balancesUpdate.bank_details[balancesUpdate.bank_details.length - 1].registered_name : ' ',
                account_number: (balancesUpdate.bank_details.length != 0) ? balancesUpdate.bank_details[balancesUpdate.bank_details.length - 1].account_number : ' ',
                ifsc_code: (balancesUpdate.bank_details.length != 0) ? balancesUpdate.bank_details[balancesUpdate.bank_details.length - 1].ifsc_code : ' ',
                previous_balance: balancesUpdate.balances[0].current_balance
            });
            status[index]['bankDetails'] = balances
        }
        array.push(status)
        if (array.length == status.length) {
            cb.call(cb, status)
        } else {
            index++;
            balances = [];
            userDetails(index, status, user, array, cb)
        }
    })
}

router.post('/changeInrDepositStatus', commonClass.connect, (req, res) => {
    if (req.body.status == '1') {
        req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { balances: { $elemMatch: { currency: "inr" } }, email: true, full_name: true, device_addresses: true, bank_details: true }, (err, status) => {
            if (status) {

                var total_previous = Number(status.balances[0].previous_balance) + Number(req.body.amount);
                var total_current = Number(status.balances[0].current_balance) + Number(req.body.amount);
                var old_balance = status.balances[0].current_balance;

                var total_previous = total_previous.toFixed(12)
                var total_current = total_current.toFixed(12)

                req.db.collection('depositinrs').findOneAndUpdate({ '_id': ObjectId(req.body.id) }, { $set: { "status": 1, 'old_balance': old_balance } }, (depErr, depStatus) => {
                    if (depStatus) {
                        req.db.collection('users').update({ 'client_id': parseInt(req.body.client_id), 'balances.currency': 'inr' }, { $set: { 'balances.$.previous_balance': total_previous, 'balances.$.current_balance': total_current, 'balances.$.old_balance': old_balance } }, (errUpdate, statusUpdate) => {
                            if (statusUpdate) {

                                req.db.collection('statistics').find({}).toArray((errs, results) => {
                                    var today_confirm = Number(results[0].deposit_status_wise.today_inr_confirm) + Number(req.body.amount);
                                    var total_confirm = Number(results[0].deposit_status_wise.total_inr_confirm) + Number(req.body.amount);

                                    // var today_confirm = today_confirm.toFixed(12);
                                    // var total_confirm = total_confirm.toFixed(12);

                                    req.db.collection('statistics').update({}, { $set: { 'deposit_status_wise.today_inr_confirm': today_confirm, 'deposit_status_wise.total_inr_confirm': total_confirm } }, (error, result) => { })

                                    var today_deposit_inr = Number(results[0].todayDeposit.inr) + Number(req.body.amount);
                                    var total_deposit_inr = Number(results[0].totalDeposit.inr) + Number(req.body.amount);

                                    // var today_deposit_inr = today_deposit_inr.toFixed(12);
                                    // var total_deposit_inr = total_deposit_inr.toFixed(12);

                                    req.db.collection('statistics').update({}, { $set: { 'todayDeposit.inr': today_deposit_inr, 'totalDeposit.inr': total_deposit_inr } }, (error, result) => { })
                                    req.db.collection('statistics').update({}, { $inc: { 'todayDepositCount.inr': 1, 'totalDepositCount.inr': 1 } }, (error, result) => { })

                                    var fields = {
                                        admin_id: ObjectId(req.body.changed_by),
                                        type: "deposits",
                                        status: 1,
                                        order_id: ObjectId(req.body.id),
                                        date: new Date()
                                    }
                                    req.db.collection('track_admin_changes').insert(fields, (err, data) => { })

                                })





                                // req.db.collection('statistics').update({}, { $inc: {'deposit_status_wise.today_inr_confirm': Number(req.body.amount), 'deposit_status_wise.total_inr_confirm': Number(req.body.amount) } }, (error, result) => {})
                                // req.db.collection('statistics').update({}, { $inc: {'todayDeposit.inr': Number(req.body.amount), 'totalDeposit.inr': Number(req.body.amount) } }, (error, result) => {})

                                var mail_content = "Your Deposite Request for INR " + req.body.amount + " has been Approved, Your Balance has been changed from " + old_balance + " to " + total_current + " INR ";
                                fs.readFile("email_templates/bankDeposit.html", { encoding: 'utf-8' }, function (error, pgResp) {
                                    var template = handlebars.compile(pgResp);
                                    var replacements = {
                                        mail_content: mail_content,
                                        full_name: status.full_name,
                                        amount: req.body.amount,
                                        status: 'Approved',
                                        bank_name: (status.bank_details.length != 0) ? status.bank_details[status.bank_details.length - 1].bank_name : ' ',
                                    };
                                    var htmlToSend = template(replacements);
                                    commonClass.mail(status.email, 'Deposit Request Approved', htmlToSend);

                                    if (status.device_addresses != []) {
                                        commonClass.sendingNotifications(req.body.client_id, status.device_addresses, 'inr', `Your BTCMONK account has been credited with ${req.body.amount} INR. Happy trading!!!`, "Deposit status", req.db)
                                    } else {
                                        console.log("data array is empty now");
                                    }
                                });
                            } else {
                                console.log("not matching any client id");
                            }
                        })
                        res.json({ status: 200 })
                    } else {
                        res.json({ status: 201 })
                    }
                });
            } else {
                res.json({ status: 201 })
            }
        })
    } else {
        req.db.collection('depositinrs').findOneAndUpdate({ '_id': ObjectId(req.body.id) }, { $set: { "status": 2 } }, (depErr, depStatus) => {
            if (depStatus) {
                req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { email: true, full_name: true, device_addresses: true, bank_details: true }, (err, user) => {
                    if (user) {


                        req.db.collection('statistics').find({}).toArray((errs, results) => {
                            var today_cancelled = Number(results[0].deposit_status_wise.today_inr_cancelled) + Number(req.body.amount);
                            var total_cancelled = Number(results[0].deposit_status_wise.total_inr_cancelled) + Number(req.body.amount);

                            // var today_cancelled = today_cancelled.toFixed(12);
                            // var total_cancelled = total_cancelled.toFixed(12)

                            req.db.collection('statistics').update({}, { $set: { 'deposit_status_wise.today_inr_cancelled': today_cancelled, 'deposit_status_wise.total_inr_cancelled': total_cancelled } }, (error, result) => { })

                            var fields = {
                                admin_id: ObjectId(req.body.changed_by),
                                type: "deposits",
                                status: 1,
                                order_id: ObjectId(req.body.id),
                                date: new Date()
                            }
                            req.db.collection('track_admin_changes').insert(fields, (err, data) => { })

                        })


                        var mail_content = "Your Deposite Request for INR " + req.body.amount + " has been Canceled, Please Re-submit your deposit Request with proper details";
                        fs.readFile("email_templates/bankDeposit.html", { encoding: 'utf-8' }, function (error, pgResp) {
                            var template = handlebars.compile(pgResp);
                            var replacements = {
                                mail_content: mail_content,
                                full_name: user.full_name,
                                amount: req.body.amount,
                                status: 'Rejected',
                                bank_name: (user.bank_details.length != 0) ? user.bank_details[user.bank_details.length - 1].bank_name : ' ',
                            };
                            var htmlToSend = template(replacements);
                            commonClass.mail(user.email, 'Deposit Request Canceled', htmlToSend);
                        });

                        if (user.device_addresses != []) {
                            commonClass.sendingNotifications(req.body.client_id, user.device_addresses, 'inr', 'Your deposit request has been cancelled. Please contact support for more information', "Deposit status", req.db)
                        } else {
                            console.log("data array is empty now");
                        }
                    }
                })
                res.json({ status: 200 })
            } else {
                res.json({ status: 201 })
            }
        });
    }
});


router.post('/saveManualDeposits', commonClass.connect, (req, res) => {
    if (req.body.currency == 'inr') {
        req.body.status = 0
    } else {
        req.body.status = 1;
    }
    if (req.body.currency == 'inr' || req.body.currency == 'thb') {
        var schema = "depositinrs";
        var fields = {
            client_id: parseInt(req.body.client_id),
            amount: Number(req.body.amount).toFixed(12),
            txid: req.body.txid,
            created: new Date(req.body.transaction_date),
            bank: req.body.bank,
            comments: req.body.comments,
            status: parseInt(req.body.status),
            currency: req.body.currency
        }
        var save_data = {
            added_by: ObjectId(req.body.changed_by),
            type: "manual_deposit",
            status: 0,
            client_id: parseInt(req.body.client_id),
            amount: Number(req.body.amount).toFixed(12),
            currency: req.body.currency,
            date: new Date()
        }
    } else if (req.body.currency == 'btc') {
        var schema = "rec_tx_con_btcs";
        var fields = {
            txid: req.body.txid,
            client_id: parseInt(req.body.client_id),
            sent_address: req.body.sent_address,
            amount: Number(req.body.amount).toFixed(12),
            created: new Date(req.body.transaction_date),
            withdrawal_type: 'internal',
            confirmations: '3',
            comments: req.body.comments,
            status: parseInt(req.body.status),
            currency: req.body.currency,
            type: 'receive'
        }
        var save_data = {
            added_by: ObjectId(req.body.changed_by),
            type: "manual_deposit",
            status: 1,
            client_id: parseInt(req.body.client_id),
            amount: Number(req.body.amount).toFixed(12),
            currency: req.body.currency,
            date: new Date()
        }
    } else if (req.body.currency == 'xrp' || req.body.currency == 'xlm') {
        var schema = req.body.currency + "_deposits";
        var fields = {
            id: req.body.txid,
            client_id: parseInt(req.body.client_id),
            sender_address: req.body.sent_address,
            amount: Number(req.body.amount).toFixed(12),
            destination_address: req.body.destination_address,
            destination_tag: 0000,
            signing_pub_key: "manual from admin",
            transaction_type: "manual from admin",
            transaction_date: new Date(req.body.transaction_date),
            created: new Date(),
            withdrawal_type: 'internal',
            comments: req.body.comments,
            status: parseInt(req.body.status),
            currency: req.body.currency
        }
        var save_data = {
            added_by: ObjectId(req.body.changed_by),
            type: "manual_deposit",
            status: 1,
            client_id: parseInt(req.body.client_id),
            amount: Number(req.body.amount).toFixed(12),
            currency: req.body.currency,
            date: new Date()
        }
    } else {
        if (req.body.currency == 'dash' || req.body.currency == 'bch') {
            var schema = "rec_tx_con_" + req.body.currency + "es";
        } else {
            var schema = "rec_tx_con_" + req.body.currency + "s";
        }
        var fields = {
            txid: req.body.txid,
            client_id: parseInt(req.body.client_id),
            sent_address: req.body.sent_address,
            amount: Number(req.body.amount).toFixed(12),
            created: new Date(),
            transaction_date: new Date(req.body.transaction_date),
            withdrawal_type: 'internal',
            confirmations: '3',
            comments: req.body.comments,
            status: parseInt(req.body.status),
            currency: req.body.currency,
            type: 'receive'
        }
        var save_data = {
            added_by: ObjectId(req.body.changed_by),
            type: "manual_deposit",
            status: 1,
            client_id: parseInt(req.body.client_id),
            amount: Number(req.body.amount).toFixed(12),
            currency: req.body.currency,
            date: new Date()
        }
    }
    req.db.collection(schema).insert(fields, (err, data) => {
        if (err) {
            res.status(200).json({ code: 151, message: "Something Went wrong while depositing amount" });
        } else {
            if (parseInt(req.body.status) == 0) {
                req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })

                res.status(200).json({ code: 151, message: "Deposit Request Initiated" });
            }
            else if (parseInt(req.body.status) == 1) {
                req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { balances: { $elemMatch: { currency: req.body.currency } }, email: true, full_name: true }, (err, status) => {
                    if (status) {
                        var old_current_balance = Number(status.balances[0].current_balance);
                        var total_previous = Number(status.balances[0].previous_balance) + Number(req.body.amount);
                        var total_current = Number(status.balances[0].current_balance) + Number(req.body.amount);

                        var total_previous = total_previous.toFixed(12);
                        var total_current = total_current.toFixed(12);

                        req.db.collection('users').update({ 'client_id': parseInt(req.body.client_id), 'balances.currency': req.body.currency }, { $set: { 'balances.$.previous_balance': total_previous, 'balances.$.current_balance': total_current } }, (errUpdate, statusUpdate) => {
                            if (statusUpdate) {
                                // var mail_content = "Hello " + status.full_name + ", <br> " + req.body.amount + " " + req.body.currency + " has been added to your wallet, <br> your balance has been changed from " + Number(status.balances[0].current_balance) + " to " + total_current.toFixed(12) + " " + req.body.currency;
                                fs.readFile("email_templates/manualDeposit.html", { encoding: 'utf-8' }, function (error, pgResp) {
                                    var template = handlebars.compile(pgResp);
                                    var replacements = {
                                        full_name: status.full_name,
                                        amount: req.body.amount,
                                        status: 'Rejected',
                                        currency: req.body.currency,
                                        updated_balance: total_current,
                                        previous_balance: old_currentbalance.toFixed(12),
                                    };
                                    var htmlToSend = template(replacements);
                                    commonClass.mail(status.email, req.body.amount + ' ' + req.body.currency + ' Deposited', htmlToSend);
                                });
                                req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { device_addresses: true }, (err, status) => {
                                    commonClass.sendingNotifications(parseInt(req.body.client_id), status.device_addresses, 'inr', `Your BTCMONK account has been deposited with ${req.body.amount} ${req.body.currency.toUpperCase()}. Happy Trading!!!`, "Deposit status", req.db)
                                })

                                req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })

                                res.status(200).json({ code: 152, message: "Deposited " + req.body.amount + " " + req.body.currency + " to Client ID " + req.body.client_id });
                            }
                        })
                    } else {
                        console.log("2 me aaya hi nahi");
                    }
                });
            }
        }
    })
})



router.post('/getDeposits/:currency/:status/:payU/:withdrawal_type/:page', commonClass.connect, (req, res, next) => {
    var sortAscDesc = req.body.sort;
    let result = [];
    var st = req.params.status;
    var payU = req.params.payU;
    var page = req.params.page;
    var sort = req.params.sort;
    var withdrawal_type = parseInt(req.params.withdrawal_type);
    if (withdrawal_type == 1) {
        var withd_type = 'external';
    } else if (withdrawal_type == 2) {
        var withd_type = 'internal';
    }
    if (st == 'all') {
        if (payU == 1) {
            var condition = { $or: [] };
        } else {
            var condition = { bank: { $ne: "payU" }, $or: [] };
        }
    } else if (st == 'pending') {
        if (payU == 1) {
            var condition = { 'status': 0, $or: [] };
        } else {
            var condition = { bank: { $ne: "payU" }, 'status': 0, $or: [] };
        }
    } else if (st == 'completed') {
        if (payU == 1) {
            var condition = { 'status': 1, $or: [] };
        } else {
            var condition = { bank: { $ne: "payU" }, 'status': 1, $or: [] };
        }
    } else if (st == 'canceled') {
        if (payU == 1) {
            var condition = { 'status': 2, $or: [] };
        } else {
            var condition = { bank: { $ne: "payU" }, 'status': 2, $or: [] };
        }
    } else if (st == 'confirm') {
        if (payU == 1) {
            var condition = { 'status': 1, $or: [] };
        } else if (withdrawal_type != 0) {
            var condition = { 'withdrawal_type': withd_type, $or: [] };
        } else {
            var condition = { bank: { $ne: "payU" }, 'status': 1, $or: [] };
        }
    }

    if (req.params.currency == 'inr') {
        getDepositsWithSchemaName(req.db.collection('depositinrs'), req.db.collection('users'), req.body.search, page, res, condition, sortAscDesc);
    } else if (req.params.currency == 'thb') {
        var condition = { 'currency': 'thb', $or: [] };
        getDepositsWithSchemaName(req.db.collection('depositinrs'), req.db.collection('users'), req.body.search, page, res, condition, sortAscDesc);
    } else if (req.params.currency == 'xrp' || req.params.currency == 'xlm') {
        if (st == 'confirm') {
            var condition = { 'status': 1, $or: [] }
        } else {
            var condition = { 'status': 0, $or: [] }
        }
        var schemaName = req.db.collection(req.params.currency + '_deposits');
        getDepositsWithSchemaName(schemaName, req.db.collection('users'), req.body.search, page, res, condition, sortAscDesc);
    } else if (req.params.currency == 'dash' || req.params.currency == 'bch') {
        if (st == 'confirm') {
            var schemaName = req.db.collection('rec_tx_con_' + req.params.currency + 'es');
        } else {
            var schemaName = req.db.collection('rec_tx_uncon_' + req.params.currency + 'es');
        }
        getDepositsWithSchemaName(schemaName, req.db.collection('users'), req.body.search, page, res, condition, sortAscDesc);
    } else {
        if (req.body.currency == 'dash' || req.body.currency == 'bch') {
            var postfix = "es";
        } else {
            var postfix = "s";
        }
        if (st == 'confirm') {
            var schemaName = req.db.collection('rec_tx_con_' + req.params.currency + postfix);
        } else {
            var schemaName = req.db.collection('rec_tx_uncon_' + req.params.currency + postfix);
        }
        getDepositsWithSchemaName(schemaName, req.db.collection('users'), req.body.search, page, res, condition, sortAscDesc);
    }
})


router.post('/payJoiningBonus', commonClass.connect, (req, res) => {
    var client_id = req.body.client_id;
    var inr = req.body.client_id;

    req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { balances: { $elemMatch: { currency: 'btc' } }, email: true, full_name: true }, (err, result) => {
        if (result) {
            var total_previous = Number(result.balances[0].previous_balance);
            var total_current = Number(result.balances[0].current_balance);
        }
    })


})

module.exports = router;

