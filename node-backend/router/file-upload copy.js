var express = require('express');
var router = express.Router();
var multer = require('multer');
var async = require('async');
var _ = require('lodash');
var xlstojson = require("xls-to-json-lc");
var notification = require('../notify.js');
var sheetName;
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

var handlebars = require('handlebars');
var fs = require('fs');
var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
});
function randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}
var upload = multer({ //multer settings
    storage: storage,
    fileFilter: function (req, file, callback) { //file filter
        sheetName = file.originalname.split('.')[0];
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(new Error('Wrong extension type'));
        }
        callback(null, true);
    }
}).single('file');
router.post('/upload', commonClass.connect, function (req, res) {
    var exceltojson;
    upload(req, res, function (err) {
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        /** Multer gives us file info in req.file object */
        if (!req.file) {
            res.json({ error_code: 1, err_desc: "No file passed" });
            return;
        }
        /** Check the extension of the incoming file and 
         *  use the appropriate module
         */
        if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
            console.log(req.file.originalname);
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }
        try {
            exceltojson({
                input: req.file.path,
                output: null, //since we don't need output.json
                lowerCaseHeaders: true
            }, function (err, result) {
                if (err) {
                    return res.json({ error_code: 1, err_desc: err, data: null });
                }
                var sheetArray = [];
                var count = 0;
                var sum = 0;
                grouped = [];
                result.forEach(function (o) {
                    if (!this[o.client_id]) {
                        this[o.client_id] = { client_id: o.client_id, amount: 0 };
                        grouped.push(this[o.client_id]);
                        count++;
                    }
                    this[o.client_id].amount += parseFloat(o.amount);
                }, Object.create(null));
                result.forEach((element, key) => {
                    var adding_amount;
                    adding_amount = Number(element.amount);
                    adding_amount = adding_amount.toFixed(8);
                    req.db.collection('users').findOneAndUpdate(
                        { 'client_id': parseInt(element.client_id), 'balances.currency': 'btc' },
                        { $inc: { 'balances.$.previous_balance': parseFloat(adding_amount), 'balances.$.current_balance': parseFloat(adding_amount) } },
                        {
                            projection: {
                                email: 1,
                                client_id: 1,
                                balances: { $elemMatch: { currency: 'btc' } }
                            }
                        }, (errUpdate, statusUpdate) => {
                            if (statusUpdate && statusUpdate.lastErrorObject.updatedExisting && statusUpdate.lastErrorObject.n === 1) {
                                if (statusUpdate.value.balances != []) {
                                    var pastValue = parseFloat(statusUpdate.value.balances[0].current_balance);
                                    adding_amount = parseFloat(adding_amount);
                                    var Update_balance = parseFloat((pastValue + adding_amount));

                                    var mail_content_admin = parseFloat(adding_amount).toFixed(8) + ' BTC Deposited To Client ID:' + element.client_id + ", 'User's balance has been changed from " + pastValue.toFixed(8) + " to " + Update_balance.toFixed(8);
                                    var admin_subject = element.client_id + " BTC Deposited " + parseFloat(adding_amount).toFixed(8);
                                        fs.readFile("email_templates/payout.html", { encoding: 'utf-8' }, function (error, pgResp) {
                                            var template = handlebars.compile(pgResp);
                                            var replacementsAdmin = {
                                                mail_content: mail_content_admin,
                                                full_name: 'Admin'
                                            };
                                            var htmlToSendAdmin = template(replacementsAdmin);
                                            commonClass.mail('akashbtcmonk@mailinator.com', admin_subject, htmlToSendAdmin);
                                        });
                                    sheetArray.push({ client_id: statusUpdate.value.client_id, pastValue: pastValue.toFixed(8), addingbal: element.amount, Update_balance: Update_balance.toFixed(8) });
                                } else {
                                    sheetArray.push({ client_id: element.client_id, pastValue: 'not update', addingbal: element.amount, Update_balance: 'not update' });
                                }
                            } else {
                                sheetArray.push({ client_id: element.client_id, pastValue: 'not update', addingbal: element.amount, Update_balance: 'not update' });
                            }

                            if (result.length == sheetArray.length) {
                                _.find(sheetArray, function (trns) {
                                    req.db.collection("users").findOne({ "client_id": parseInt(trns.client_id) }, { email: true, device_addresses: true }, function (err, notify) {
                                        if (notify) {
                                            var histObj = {};
                                            histObj.client_id = parseInt(trns.client_id);
                                            histObj.amount = parseFloat(trns.addingbal);
                                            if (sheetName === "Lending_Withdrawal") {
                                                histObj.txid = sheetName + "_" + trns.client_id + "_" + ID();
                                            } else {
                                                histObj.txid = sheetName + "_" + trns.client_id + "_" + ID();
                                            }
                                            histObj.sent_address = "admin";
                                            histObj.transaction_date = new Date();
                                            histObj.created = new Date();
                                            histObj.updated = new Date();
                                            histObj.withdrawal_type = "external";
                                            histObj.confirmations = "3";
                                            histObj.status = "1";
                                            histObj.currency = "btc";
                                            histObj.type = "receive"
                                            req.db.collection("rec_tx_con_btcs").insertOne(histObj, function (err, res) {
                                                if (err) {
                                                    console.log("dipoist history-error", err, res);
                                                } else {
                                                    console.log("dipoist history");

                                                    histObj = {};



                                                }
                                            })
                                        } else {
                                            console.log("No data for this client_id:", trns.client_id);
                                        }

                                    })
                                })

                                // _.find(grouped, function (chr) {
                                //     if (count === grouped.length) {
                                //         req.db.collection("users").findOne({ "client_id": parseInt(chr.client_id) }, { email: true, device_addresses: true, full_name: true }, function (err, notify) {
                                //             if (notify && notify.email !== null && notify.device_addresses !== []) {
                                //                 // var mail_content = parseFloat(chr.amount).toFixed(8) + " " + " BTC" + " has been credited to you";
                                //                 // var mail_content_admin = parseFloat(chr.amount).toFixed(8) + ' BTC Deposited To Client ID:' + chr.client_id + ", " + notify.full_name + "'s balance has been changed from " + pastValue.toFixed(8) + " to " + Update_balance.toFixed(8);
                                //                 // var admin_subject = chr.client_id + " BTC Deposited " + parseFloat(chr.amount).toFixed(8);


                                //                 // fs.readFile("email_templates/payout.html", { encoding: 'utf-8' }, function (error, pgResp) {
                                //                 //     var template = handlebars.compile(pgResp);
                                //                 //     // var replacements = {
                                //                 //     //     mail_content: mail_content,
                                //                 //     //     full_name: notify.full_name
                                //                 //     // };

                                //                 //     var replacementsAdmin = {
                                //                 //         mail_content: mail_content_admin,
                                //                 //         full_name: notify.full_name
                                //                 //     };
                                //                 //     // var htmlToSend = template(replacements);
                                //                 //     var htmlToSendAdmin = template(replacementsAdmin);
                                //                 //     // commonClass.mail(notify.email, 'Payout Created', htmlToSend);
                                //                 //     commonClass.mail('akashbtcmonk@mailinator.com', admin_subject, htmlToSendAdmin);
                                //                 // });
                                //             }
                                //         })
                                //     }
                                // });
                                res.json({ code: 200, data: sheetArray });
                            }
                        })
                });
            });
        } catch (e) {
            res.json({ error_code: 1, err_desc: "Corupted excel file" });
        }
    })
});
function uniqueNumber() {
    var date = Date.now();
    if (date <= uniqueNumber.previous) {
        date = ++uniqueNumber.previous;
    } else {
        uniqueNumber.previous = date;
    }
    return date;
}
uniqueNumber.previous = 0;
function ID() {
    return uniqueNumber();
};
module.exports = router;
