var express = require('express');
var routes = express.Router();
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

routes.post('/daily-interest', commonClass.connect, (req, res) => {
    var sortAscDesc = req.body.sort;

    var sorting = {};
    var sort_by = sortAscDesc.sortField;
    var sort_in_order = Number(sortAscDesc.sortType);
    sorting[sort_by] = sort_in_order;

    var perPage = 10, page = Math.max(0, req.body.pageUrl);
    var skip = (perPage * page) - perPage;

    if (req.body.search != '') {
        var conditions = {
            $or: [
                { client_id: parseInt(req.body.search) }
            ]
        }
    } else {
        var conditions = {}
    }
    //console.log(conditions)
    req.db.collection('dailyinterests').count(conditions, (errcount, statusCount) => {
        req.db.collection('dailyinterests').find(conditions).skip(skip).limit(perPage).sort(sorting).toArray((err, status) => {
            if (err) {
                res.json({ error: err });
            } else {
                res.json({ status: 200, data: status, count: statusCount });
            }
        })
    })
});

routes.post('/saveDailyInterest', commonClass.connect, (req, res) => {
    req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { device_addresses: true, balances: { $elemMatch: { currency: 'btc' } }, email: true, full_name: true }, (err, status) => {
        if (status) {
            var total_previous = parseFloat(status.balances[0].previous_balance) + parseFloat(req.body.amount);
            var total_current =  parseFloat(status.balances[0].current_balance) + parseFloat(req.body.amount);

            var total_previous = Number(total_previous.toFixed(8));
            var total_current = Number(total_current.toFixed(8));

            // console.log(typeof total_previous, total_previous);
            // console.log(typeof total_current, total_current);

            var fields = {
                client_id: parseInt(req.body.client_id),
                interest: Number(parseFloat(req.body.amount).toFixed(8)),
                totalAmount: Number((parseFloat(status.balances[0].current_balance) + parseFloat(req.body.amount)).toFixed(8)),
                dailyAmount: Number(parseFloat(status.balances[0].current_balance).toFixed(8)),
                date: new Date()
            }

            req.db.collection('users').update({ 'client_id': parseInt(req.body.client_id), 'balances.currency': 'btc' }, { $set: { 'balances.$.previous_balance': total_previous, 'balances.$.current_balance': total_current } }, (errUpdate, statusUpdate) => {
                if (statusUpdate) {
                    var mail_content = "Hello " + status.full_name + ", <br> " + req.body.amount + "BTC has been added to your wallet, <br> your balance has been changed from " + status.balances[0].current_balance + " to " + total_current + " " + 'BTC';
                    req.db.collection('dailyinterests').insert(fields, (err, data) => {
                        // commonClass.mail(status.email, 'Amount Deposited', mail_content);
                        // notification(status.device_addresses, `Your Daily Interest has been deposited with ${req.body.amount}. Happy Trading!!!`, "Daily Interest Deposited")
                        res.status(200).json({ code: 152, message: "Deposited " + req.body.amount + "BTC to Client ID " + req.body.client_id });
                    })
                }
            })
        } else {
            res.status(200).json({ code: 152, message: "Something went wrong while adding daily interest" });
        }
    })
})

module.exports = routes;