var express = require('express'),
  nodemailer = require('nodemailer'),
  routes = express.Router(),
  config = require("../config/config.json"),
  mailOptions, object,
  transporter = nodemailer.createTransport(config.settings);
var handlebars = require('handlebars');
var fs = require('fs');
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();
var async = require("async");

// this is to get file names of folder
var path = require('path');
var dirPath = './email_templates/';  //directory path
// var fileType = '.'+process.argv[3]; //file extension
var files = [];


routes.post('/service', commonClass.connect, (req, res, next) => {
  if (((!req.body.range_from) || (!req.body.range_to)) && (req.body.user == 1)) {
    res.status(201).json({ status: 201, message: 'Missing Field' });
    return
  }
  if (req.body.user == 1) {
    req.db.collection('users').find({}, { 'email': true, full_name: true }).skip(parseInt(req.body.range_from)).limit(parseInt(req.body.range_to)).toArray((err, result) => {
      if (result) {
        var mail_content = req.body.description;
        var subject = req.body.subject
        result.forEach((user, key) => {
          if (user.full_name.length < 2) {
            user.full_name = 'BTCMonk user'
          }
          fs.readFile("email_templates/sendEmail2.html", { encoding: 'utf-8' }, function (error, pgResp) {
            var template = handlebars.compile(pgResp);
            var replacements = {
              mail_content: mail_content,
              full_name: user.full_name,
            };
            var htmlToSend = template(replacements);
            // commonClass.mail(user.email, subject, htmlToSend);
            // commonClass.mail("arpan.rewatkar@btcmonk.com", subject, htmlToSend);
            // commonClass.mail("akashbtcmonk@mailinator.com", subject, htmlToSend);
          })
        })

        // req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })

      }
    })
  } else {



    var mail_content = req.body.description;
    var subject = req.body.subject

    fs.readFile("email_templates/sendEmail2.html", { encoding: 'utf-8' }, function (error, pgResp) {
      var template = handlebars.compile(pgResp);
      var replacements = {
        mail_content: mail_content,
      };
      var htmlToSend = template(replacements);
      // console.log(email);
      for (var email of req.body.to) {
        commonClass.mail(email, subject, htmlToSend);
      }

      // commonClass.mail(email, req.body.subject, req.body.description);
    })
    // }
  }
  res.status(200).json({ status: 200, message: 'successfully sent your message' });
});




routes.post('/emailing_batchwise', commonClass.connect, (req, res, next) => {
  req.db.collection('users').count({}, (err, result) => {
    var to_skip = 0;
    var batch_total_records = 0;
    var batch_of = 10000;
    // var result = 2
    var limit_iss = 10000;
    // var batch_of = 10000;
    var batch_total_records = Math.round(result / batch_of);
    var x = 0;
    var array = [];
    for (i = 0; i < batch_total_records; i++) {
      if (i == 0) {
        array.push({
          to_skip: 0,
          limit_is: batch_of
        })
      } else {
        to_skip = to_skip + batch_of;
        limit_is = batch_of + to_skip;
        array.push({
          to_skip: to_skip,
          limit_is: limit_is
        })
      }
    }
    async.forEachSeries(array, function (item, next) {
      var count = 0;

      var mail_content = req.body.description;
      var subject = req.body.subject

      req.db.collection('users').find({}, { client_id: true, email: true }).skip(item.to_skip).limit(limit_iss).toArray((err, result) => {
        _.forEach(result, function (value, key) {

          fs.readFile("email_templates/sendEmail2.html", { encoding: 'utf-8' }, function (error, pgResp) {
            var template = handlebars.compile(pgResp);
            var replacements = {
              mail_content: mail_content,
            };
            var htmlToSend = template(replacements);
            if (value.email.length != 0) {
              // commonClass.mail('akashbtcmonk3@mailinator.com', value.email, htmlToSend);
            }
            count++;
            if (result.length == count) {
              console.log('===================================================', item.limit_is)
              next();
            }
          })

        });
      })
    }, () => {
      res.json({ status: 200, message: "Email sent to all users" });
    })
  })
  1
});



routes.post('/getFileNames', commonClass.connect, (req, res, next) => {
  fs.readdir(dirPath, function (err, list) {
    res.json({ status: 200, file_list: list });
  });
})



module.exports = routes;