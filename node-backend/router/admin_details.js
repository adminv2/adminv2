var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
var handlebars = require('handlebars');
var fs = require('fs');
var sha256 = require('js-sha256');
var async = require("async");
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();




router.post('/bankList', commonClass.connect, (req, res, next) => {
    //countWithdraw(req)
   // countWithdrawInr(req,1)    // o pending 1 compt 2 reject
   // countWithdrawInr(req,0)    // o pending 1 compt 2 reject
   // countWithdrawInr(req,2)    // o pending 1 compt 2 reject


    // count(req,'btc')
    // count(req,'omg')
    // count(req,'eth')
    // count(req,'ada')
    // count(req,'dash')
    // count(req,'pura')
    // count(req,'ltc')
    // count(req,'bch')
    //count(req,'inr')
    // count(req,'btg')
    // count(req,'xlm')
    // count(req,'xrp')
    // count(req,'rep')
   // count(req,'ven')
//    count(req,'ae')

    //countDepositnr(req,0)   // o pending 2 reject
    //countDepositnr(req,2)   // o pending 2 reject


//interest(req)


     

    req.db.collection('company_bank_detail').find().toArray((err, result) => {
        if (result) {
            res.json({ status: 300, bankData: result });
        } else {
            res.json({ status: 301 })
        }
    })
})


router.post('/getUpdatedBalanesRecords', commonClass.connect, (req, res, next) => {
    req.db.collection('update_balances').aggregate([
        {
            $lookup:
                {
                    from: 'admin_users',
                    localField: 'updated_by',
                    foreignField: '_id',
                    as: 'admin_details'
                },
        }
    ]).sort({ updated: -1 }).toArray((err, result) => {
        if (result) {
            res.json({ status: 300, updBalances: result });
        } else {
            res.json({ status: 301 })
        }
    })
})


router.post('/addCompanyBank', commonClass.connect, (req, res) => {
    var fields = {
        beneficiary: req.body.beneficiary,
        ifsc: req.body.ifsc,
        bankName: req.body.bankName,
        account: req.body.account,
        country: req.body.country.toLowerCase(),
        status: 1,
        date: new Date()
    }

    req.db.collection("company_bank_detail").insert(fields, (err, data) => {
        if (err) {
            res.status(200).json({ code: 151, message: "Something Went wrong while depositing amount" });
        } else {
            res.status(200).json({ code: 151, message: "New Bank Added" });
        }

    });
})

//////////////////////////// intrest /////////////
function interest (req){
    req.db.collection('dailyinterests').aggregate(
                [
                  {
                    $group:
                      {
                        _id : null, 
                        interest: { $sum: "$interest" },
                        count: { $sum: 1 }
                      }
                  }
                ]
             ).toArray((err,status)=>{

                
                req.db.collection('statistics').update({}, { $set: {totalinterest:status[0].interest} }, (error, result) => { 
                })
                 console.log('status@@@@@@@@@@@@@=================@@@@@@@@@@@@',status)
                // console.log('err@@@@@@@@@@@@@@@@@@@@@@@@@=================@@@@@@@@@@@@',err)
                //  res.json({ code: 5004, status: status,err:err });
             })
        
        }




////////////////////////////////////////////////////////////////////
///////////////////////////////// withdraw count /////////////////
function countWithdraw (req){
    var receiveCurrencies;
    req.db.collection('currencies').find({}).toArray((err, result) => {

       async.forEachSeries(result, function (user, next) {

            if (user.id == 'inr' || user.id == 'thb') {
                receiveCurrencies = 'withdrawamounts'
            }else if (user.id == 'xrp') {
                receiveCurrencies =  'xrp_withdrawals'
            } else {
                if (user.id == 'dash' || user.id == 'bch') {
                    var postfix = "es";
                } else {
                    var postfix = "s";
                }
                receiveCurrencies = 'send_tx_' + user.id + postfix
            }

          
            req.db.collection(receiveCurrencies).aggregate(
                [
                    { $match: { status:1 } },
                {
                $group:
                {
                    _id : null, 
                amount: { $sum: "$amount" },
                }
                }
                ]
                ).toArray((err,amount)=>{
                    if(amount.length != 0){
                        var query = {};
                        query["totalWithdraw."+user.id.toLowerCase()]= amount[0].amount;
                        console.log(query)
                        req.db.collection('statistics').update({}, { $set: query }, (error, result) => { 
                            next();
                        })
                    }else{
                        next(); 
                    }
                })  
        },(err)=>{
            console.log('complate')
        })
    })
}

function countWithdrawInr (req,status){
    var receiveCurrencies;

          
            req.db.collection('withdrawamounts').aggregate(
                [
                    { $match: { 'otp.status':1 ,status:status,currency:'inr'} },
                {
                $group:
                {
                    _id : null, 
                amount: { $sum: "$amount" },
                }
                }
                ]
                ).toArray((err,amount)=>{
                    if(amount.length != 0){
                        var query = {};
                        if(status == 1){
                            query["withdrawal_status_wise.total_inr_confirm"]= amount[0].amount;
                        }else if(status == 2){
                            query["withdrawal_status_wise.total_inr_cancelled"]= amount[0].amount;
                        }else{
                            query["withdrawal_status_wise.total_inr_pending"]= amount[0].amount;
                        }
                        console.log(query)
                        req.db.collection('statistics').update({}, { $set: query }, (error, result) => { 
                            console.log(amount[0].amount+'========')
                        })
                    }else{
                    }
                })  
}

////////////////////////////////////////////////////////////////////
///////////////////////////////// withdraw count end /////////////////



//////////////////////////////////////// deposit //////////////////////

function count(req,currency) {
    var async = require("async");


    req.db.collection('users').count(
        {}
        , (err, result) => {
            // console.log("result",result);
            var to_skip = 0;
            var sum = 0;
            var totalBalance = 0;
            var batch_total_records = 0;
            var batch_of = 10000;
            var loop = 0;
            //var batch_of = 2;
            var batch_total_records = Math.round(result / batch_of);
            var x = 0;
            var array = [];
            for (i = 0; i < batch_total_records; i++) {
                if (i == 0) {
                    array.push({
                        to_skip: 0,
                        limit_is: batch_of
                    })
                } else {
                    to_skip = to_skip + batch_of;
                    limit_is = batch_of + to_skip;
                    array.push({
                        to_skip: to_skip,
                        limit_is: limit_is
                    })
                }
            }
            // client_id: {
            //     $ne: [17077, 17085, 17092, 17446, 17075, 115733, 115734, 116869, 116870, 116871, 116872, 116873, 116874, 116884, 116886, 116887, 116889, 116890, 116891, 116897, 116913, 116942, 116943, 116944, 116958, 117004, 117003, 117067, 117085, 117086, 117105, 117106, 117107, 117194, 117218, 117242]
            // }
          //  , email: { $ne: [new RegExp('@yopmail', 'i'), new RegExp('@mailinator', 'i')] }
            async.forEachSeries(array, function (item, next) {
                var count = 0;
                
                req.db.collection('users').find({
                    
                },
                    {
                        balances: { $elemMatch: { currency: currency } }

                    }).skip(item.to_skip).limit(item.limit_is).toArray((err, result) => {
                         //console.log(result)
                         console.log(currency)

                        _.forEach(result, function (value, key) {
                            count++;
                            loop++;
                            if (isNaN(value.balances[0]['current_balance'])) {
                                sum += 0;
                                console.log("Not number@@@@@@@@@@@@@@@@@@@@@", sum);
                            } else {
                                sum = parseFloat(sum) + parseFloat(value.balances[0]['current_balance']);

                            }
                            if (result.length == count) {
                                //totalBalance += sum;

                                var query = {};
                                if(currency != 'inr' ){
                                    query["totalDeposit."+currency.toLowerCase()]= sum;
                                }else{
                                    query["deposit_status_wise.total_inr_confirm"]= sum;
                                }
                                
                                console.log(query)
                                req.db.collection('statistics').update({}, { $set: query }, (error, result) => { })
                                next();
                                // console.log("from to to loop"+loop, item.to_skip, item.limit_is,  sum);
                            }
                        });
                    })
            }, () => {
                console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ deposit finish',currency)
                // commonClass.mail('ankur.bansal@btcmonk.com', 'btc balance', totalBalance);
                // res.json({ status: 200, allUserBalance: totalBalance,currency:req.query.currency });
            })
        })

}



function countDepositnr (req,status){
    var receiveCurrencies;

          
            req.db.collection('depositinrs').aggregate(
                [
                    { $match: { status:status,currency:'inr'} },
                {
                $group:
                {
                    _id : null, 
                amount: { $sum: "$amount" },
                }
                }
                ]
                ).toArray((err,amount)=>{
                    if(amount.length != 0){
                        var query = {};
                        if(status == 0){
                            query["deposit_status_wise.total_inr_pending"]= amount[0].amount;
                        }else{
                            query["deposit_status_wise.total_inr_cancelled"]= amount[0].amount;
                        }
                        console.log(query)
                        req.db.collection('statistics').update({}, { $set: query }, (error, result) => { 
                            console.log(amount[0].amount+'========')
                        })
                    }else{
                    }
                })  
}






router.post('/editCompanyBank', commonClass.connect, (req, res) => {
    var fields = {
        beneficiary: req.body.beneficiary,
        ifsc: req.body.ifsc,
        bankName: req.body.bankName,
        account: req.body.account,
        country: req.body.country.toLowerCase(),
        status: 1,
        updated: new Date()
    }
    var obj = fields;
    req.db.collection('company_bank_detail').update({ "_id": ObjectId(req.body._id) }, { $set: obj }, (errUpdate, statusUpdate) => {
        if (errUpdate) {
            // console.log(errUpdate.message);
            res.status(200).json({ code: 151, message: "Something Went wrong while Updating Bank Details" });
        } else {
            res.status(200).json({ code: 151, message: "Bank Details Updated Successfully" });
        }
    })
})


router.post('/changeCompanyBankStatus', commonClass.connect, (req, res) => {
    var update = {
        'status': parseInt(req.body.status)
    }
    var upd = update

    req.db.collection('admindetails').update({ "_id": ObjectId(req.body.id) }, { $set: upd }, (errUpdate, statusUpdate) => {
        if (errUpdate) {
            // console.log(errUpdate.message);
            res.status(200).json({ code: 151, message: "Something Went wrong while Changing Bank Status" });
        } else {
            res.status(200).json({ code: 151, message: "Status Updated Successfully" });
        }
    })
})
router.post('/changeTutorialStatus', commonClass.connect, (req, res) => {
    var id = req.body.id;
    var to_change = req.body.type + "." + id + ".status";
    var update = {
        [to_change]: parseInt(req.body.status)
    }
    var upd = update
    req.db.collection('tutorial_details').update({ "_id": ObjectId("5aaabb69dea7a053be42a6a0") }, { $set: upd }, (errUpdate, statusUpdate) => {
        if (errUpdate) {
            // console.log(errUpdate.message);
            res.status(200).json({ code: 151, message: "Something Went wrong while Changing Status" });
        } else {
            res.status(200).json({ code: 151, message: "Status Updated Successfully" });
        }
    })
})

router.post('/changeTxnIdStatus', commonClass.connect, (req, res) => {
    var id = req.body.id;
    var status = req.body.status;
    console.log("id", id);
    console.log("status", status);
    req.db.collection('transaction_ids').update({ "transaction_id": id }, { $set: { 'status': status } }, (errUpdate, statusUpdate) => {
        if (errUpdate) {
            // console.log(errUpdate.message);
            res.status(200).json({ code: 151, message: "Something Went wrong while Changing Status" });
        } else {
            res.status(200).json({ code: 151, message: "Status Updated Successfully" });
        }
    })
})



// router.post('/getBankDetails', commonClass.connect, (req, res, next) => {
//     req.db.collection('company_bank_detail').findOne({ "_id": ObjectId(req.body.id) }, (err, result) => {
//         if (err) {
//             res.json({ error: err });
//         } else {
//             res.json({ status: 200, bankD: result, code: 152 });
//         }
//     })
// })
router.post('/addCurrency', commonClass.connect, (req, res) => {
    if (req.body.type == 'trade_pair') {
        var trade_pair = req.body.trade_pair.toLowerCase()
        req.db.collection('currencies_details').update({ '_id': ObjectId("5b1f53cfebd23e2e9be5705c") }, {
            $push: {
                "trade_pairs": trade_pair
            }
        }, (err, data) => {
            if (data) {
                res.json({ status: 200, message: "New Pair Added", data: data })
            } else {
                res.json({ status: 400, message: "Something went wrong while adding new Pair" });
            }
        });
    } else if (req.body.type == 'currency') {
        var currency = req.body.name.toLowerCase()
        req.db.collection('currencies_details').update({ '_id': ObjectId("5b1f53cfebd23e2e9be5705c") }, {
            $push: {
                "currencies": currency
            }
        }, (err, data) => {
            if (data) {
                res.json({ status: 200, message: "New Currency Added", data: data })
            } else {
                res.json({ status: 400, message: "Something went wrong while adding new currency" });
            }
        });
    } else {
        var obj_name = req.body.schema_type.toLowerCase();
        var currency = req.body.currency.toLowerCase();
        var new_schema_name = req.body.schema_name.toLowerCase();
        var collom = obj_name + '.' + currency
        req.db.collection('currencies_details').update({ '_id': ObjectId("5b1f53cfebd23e2e9be5705c") }, {
            $set: {
                [collom]: new_schema_name
            }
        }, (err, data) => {
            if (data) {
                res.json({ status: 200, message: "New Pair Added", data: data })
            } else {
                res.json({ status: 400, message: "Something went wrong while adding new Pair" });
            }
        });
    }
})
// getTutorialDetails

router.post('/getTutorialLinks', commonClass.connect, (req, res, next) => {
    req.db.collection('tutorial_details').findOne({}, (err, result) => {
        if (result) {
            res.json({ status: 300, tutorials: result });
        } else {
            res.json({ status: 301 })
        }
    })
})


router.post('/saveTutorial', commonClass.connect, (req, res) => {
    var d = new Date();
    var timeStamp = d.getTime();
    req.db.collection('tutorial_details').update({ _id: ObjectId("5aaabb69dea7a053be42a6a0") }, {
        $push: {
            [req.body.type]: {
                $each: [{
                    id: timeStamp,
                    title: req.body.title,
                    link: req.body.link,
                    description: req.body.description,
                    created: new Date(),
                    status: 1
                }]
            }
        }
    }, (err, data) => {
        if (data) {
            res.json({ status: 200, message: "Link Details Successfully", data: data })
        } else {
            res.json({ status: 400, message: "Something went wrong while adding Link Details" });
        }
    });
})



router.post('/changePassword', commonClass.connect, (req, res, next) => {
    var condition = { 'password': sha256(req.body.current_password), '_id': ObjectId(req.body.admin_user_id), };

    var save_data = {
        changd_by: ObjectId(req.body.admin_user_id),
        type: "password_changed",
        date: new Date()
    }


    req.db.collection('admin_users').findOne(condition, (err, result) => {
        if (result) {
            req.db.collection('admin_users').update({ '_id': ObjectId(req.body.admin_user_id) }, {
                $set: {
                    password: sha256(req.body.update_password)
                }
            }, (err, data) => {
                if (data) {
                    fs.readFile("email_templates/admin_pass_changes.html", { encoding: 'utf-8' }, function (error, pgResp) {
                        var template = handlebars.compile(pgResp);
                        var replacements = {
                            full_name: result.f_name + ' ' + result.l_name,
                            new_password: req.body.update_password
                        };
                        var htmlToSend = template(replacements);

                        var replace = {
                            full_name: result.f_name + ' ' + result.l_name,
                            new_password: ''
                        };
                        var htmlToSend2 = template(replace);

                        // commonClass.mail('arpan.rewatkar@btcmonk.com', 'Password Changed', htmlToSend);
                        commonClass.mail('shrikant@btcmonk.com', 'Password Changed', htmlToSend);
                        commonClass.mail('akash.mangal@btcmonk.com', 'Password Changed', htmlToSend2);

                    })
                    req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })

                    res.json({ status: 200, message: "Password Changed Successfully, You will be logged out autometically" })
                } else {
                    res.json({ status: 400, message: "Something went wrong while changing password" });
                }
            });
        } else {
            res.json({ status: 300, message: "Invalid Old Password", code: 152 });
        }
    })
})




// router.post('/getBankDetails', commonClass.connect, (req, res, next) => {
//     req.db.collection('company_bank_detail').findOne({ "_id": ObjectId(req.body.id) }, (err, result) => {
//         if (err) {
//             res.json({ error: err });
//         } else {
//             res.json({ status: 200, bankD: result, code: 152 });
//         }
//     })
// })



router.post('/getBankDetails', commonClass.connect, (req, res, next) => {
    var client_id = 17996;
    req.db.collection('currencies').find({}, { id: 1, name: 1, fees: 1, min_withdrawal: 1, is_fiat: 1, icon: 1 }, (err, currencyDetails) => {
        if (err) { }
        else {
            if (currencyDetails) {
                var currencyInfo = [];
                var count = 0;
                async.forEachSeries(currencyDetails, (currency, callback) => {
                    var id = currency.id;
                    var cur = {};
                    var database = null;
                    var inrDatabase = null;
                    // var isFiat = false;
                    var is_fiat = currency.is_fiat;
                    database = db.TradeBook[`${id}_btc_trade_history`];

                    switch (id) {
                        case "inr": { database = db.TradeBook[`btc_${id}_trade_history`]; break; }
                        case "thb": { database = db.TradeBook[`btc_${id}_trade_history`]; break; }
                        /*
                        case "btc": { inrDatabase = db.BtcInrTradeHistory; break; }
                        case "xrp": { database = db.XrpBtcTradeHistory; inrDatabase = db.XrpInrTradeHistory; break; }
                        case "xlm": { database = db.XlmBtcTradeHistory; inrDatabase = db.XlmInrTradeHistory; break; }
                        case "bch": { database = db.BchBtcTradeHistory; inrDatabase = db.BchInrTradeHistory; break; }
                        case "btg": { database = db.BtgBtcTradeHistory; inrDatabase = db.BtgInrTradeHistory; break; }
                        case "ltc": { database = db.LtcBtcTradeHistory; inrDatabase = db.LtcInrTradeHistory; break; }
                        case "pura": { database = db.PuraBtcTradeHistory; inrDatabase = db.PuraInrTradeHistory; break; }
                        case "dash": { database = db.DashBtcTradeHistory; inrDatabase = db.DashInrTradeHistory; break; }
                    */
                    }
                    console.log(database + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@=========================')
                    var name = "";
                    var btc_factor = 0;
                    var inr_factor = 0;
                    var icon = "";
                    var fees = 0;
                    if (database) {
                        async.waterfall([
                            (callback) => {
                                database.find({ status: { $ne: 3 } }).sort({ _id: -1 }).limit(1).exec((err, details) => {
                                    if (err) { callback(err); }
                                    else {
                                        if (details.length != 0) {
                                            btc_factor = details[0].rate;
                                            if (is_fiat) {
                                                btc_factor = truncateValue((1 / details[0].rate), precisions.btc);
                                                if (id == 'inr') {
                                                    inr_factor = 1;
                                                }
                                            }
                                        }
                                        callback();
                                    }
                                });
                            },
                            (callback) => {
                                if (inrDatabase) {
                                    inrDatabase.find({ status: { $ne: 3 } }).sort({ _id: -1 }).limit(1).exec((err, details) => {
                                        if (err) { callback(err); }
                                        else {
                                            if (details.length != 0) {
                                                inr_factor = details[0].rate;
                                                if (id == 'inr') {
                                                    inr_factor = 1;
                                                }
                                            }
                                            callback();
                                        }
                                    });
                                } else {
                                    callback();
                                }
                            }
                        ], (err) => {
                            if (err) { }
                            else {
                                db.User.findOne({ client_id: client_id, "balances.currency": id }, { balances: { $elemMatch: { currency: id } } }, (err, userDetails) => {
                                    if (userDetails) {
                                        var previous_balance = userDetails.balances[0].previous_balance;
                                        var current_balance = userDetails.balances[0].current_balance;

                                        previous_balance = truncateValue(previous_balance, vPrecisions[id], false);
                                        current_balance = truncateValue(current_balance, vPrecisions[id], false);
                                        //////////////////////////////////////
                                        /// FOR TESTING ///
                                        ///////////////////
                                        // if (client_id == 115626 || client_id == 115734 || client_id == 115733) {
                                        // previous_balance = arthOperation(id, previous_balance, Math.pow(10, precisions[id]), "/", true);
                                        // current_balance = arthOperation(id, current_balance, Math.pow(10, precisions[id]), "/", true);
                                        // }
                                        //////////////////////////////////////
                                        name = currency.name;
                                        icon = currency.icon;
                                        fees = currency.fees;
                                        var min_withdrawal = currency.min_withdrawal;
                                        cur.id = id;
                                        cur.name = name;
                                        cur.btc_factor = btc_factor;
                                        cur.inr_factor = inr_factor;
                                        cur.icon = icon;
                                        cur.fees = fees;
                                        cur.min_withdrawal = min_withdrawal;
                                        cur.is_fiat = is_fiat;
                                        cur.previous_balance = previous_balance;
                                        cur.current_balance = current_balance;
                                        currencyInfo.push(cur);
                                    }
                                    count++;
                                    callback();
                                });
                            }
                        });
                    } else {
                        async.waterfall([
                            (callback) => {
                                if (inrDatabase) {
                                    inrDatabase.find({ status: { $ne: 3 } }).sort({ _id: -1 }).limit(1).exec((err, details) => {
                                        if (err) { callback(err); }
                                        else {
                                            if (details.length != 0) {
                                                inr_factor = details[0].rate;
                                                if (id == 'inr') {
                                                    inr_factor = 1;
                                                }
                                            }
                                            callback();
                                        }
                                    });
                                } else {
                                    callback();
                                }
                            }
                        ], (err) => {
                            if (err) {
                            } else {
                                db.User.findOne({ client_id: client_id, "balances.currency": id }, { balances: { $elemMatch: { currency: id } } }, (err, userDetails) => {
                                    if (userDetails) {
                                        var previous_balance = userDetails.balances[0].previous_balance;
                                        var current_balance = userDetails.balances[0].current_balance;

                                        previous_balance = truncateValue(previous_balance, vPrecisions[id], false);
                                        current_balance = truncateValue(current_balance, vPrecisions[id], false);
                                        //////////////////////////////////////
                                        /// FOR TESTING ///
                                        ///////////////////
                                        // if (client_id == 115626 || client_id == 115734 || client_id == 115733) {
                                        // previous_balance = arthOperation(id, previous_balance, Math.pow(10, precisions[id]), "/", true);
                                        // current_balance = arthOperation(id, current_balance, Math.pow(10, precisions[id]), "/", true);
                                        // }
                                        //////////////////////////////////////
                                        name = currency.name;
                                        btc_factor = 1;
                                        icon = currency.icon;
                                        fees = currency.fees;
                                        var min_withdrawal = currency.min_withdrawal;
                                        cur.id = id;
                                        cur.name = name;
                                        cur.btc_factor = btc_factor;
                                        cur.inr_factor = inr_factor;
                                        cur.icon = icon;
                                        cur.fees = fees;
                                        cur.min_withdrawal = min_withdrawal;
                                        cur.is_fiat = is_fiat;
                                        cur.previous_balance = previous_balance;
                                        cur.current_balance = current_balance;
                                        currencyInfo.push(cur);
                                    }
                                    count++;
                                    callback();
                                });
                            }
                        });
                    }
                }, (err) => {
                    if (err) {
                        res.json({ code: 1212, message: "Something went wrong" });
                        // socket.emit('currencies', JSON.stringify({ code: 1212, message: "Something went wrong" }));
                    } else {
                        if (count == currencyDetails.length) {
                            res.json({ code: 1212, message: currencyInfo });
                            //socket.emit('currencies', JSON.stringify(currencyInfo));
                        }
                    }
                });
            }
        }
    });
})


router.post('/getSearchedData', commonClass.connect, (req, res, next) => {
    client_id = req.body.client_id;
    start_date = req.body.start_date;
    end_date = req.body.end_date;

    if (req.body.search_for == 'deposit') {
        deposit(req, res, client_id, start_date, end_date)
    } else if (req.body.search_for == 'withdrawal') {
        withdraw(req, res, client_id, start_date, end_date)
    } else {
        trade(req, res, client_id, start_date, end_date)
    }
    // 
})



function deposit(req, res, clientId, start_date, end_date) {
    var currencies = [];
    var result = [];
    async.waterfall([
        (callback) => {
            req.db.collection('currencies').find({}).toArray((err, result) => {
                result.forEach((user, key) => {
                    if (user.id == 'inr') {
                        currencies.push('depositinrs')
                    } else if (user.id == 'xrp' || user.id == 'xlm') {
                        currencies.push(user.id + '_deposits')
                    } else {
                        if (user.id == 'dash' || user.id == 'bch') {
                            var postfix = "es";
                        } else {
                            var postfix = "s";
                        }
                        currencies.push('rec_tx_con_' + user.id + postfix)
                    }
                })
                callback()
            })
        },
        (callback) => {
            var currentYear = new Date().getFullYear();
            var currentMonth = new Date().getMonth() + 1;
            var lastDate = new Date(currentYear, currentMonth, 0).getDate();

            if (start_date !== '' && end_date !== '' && clientId !== '') {
                var condition = { client_id: parseInt(clientId), created: { $gte: new Date(start_date), $lt: new Date(end_date) } }
            } else if (start_date !== '' && end_date !== '' && clientId === '') {
                var condition = { created: { $gte: new Date(start_date), $lt: new Date(end_date) } }
            } else if (start_date === '' && end_date === '' && clientId !== '') {
                var condition = { client_id: parseInt(clientId), created: { $gte: new Date(currentYear + '-' + currentMonth + '-' + '1 00:01:00.000Z'), $lt: new Date(currentYear + '-' + currentMonth + '-' + lastDate + ' 00:55:00.000Z') } }
            } else {
                var condition = { created: { $gte: new Date(currentYear + '-' + currentMonth + '-' + '1 00:01:00.000Z'), $lt: new Date(currentYear + '-' + currentMonth + '-' + lastDate + ' 00:55:00.000Z') } }
            }
            //console.log(condition)
            async.forEachSeries(currencies, (receiveCurrencies, cb) => {

                req.db.collection(receiveCurrencies).find(condition, { txid: true, amount: true, currency: true, created: true, client_id: true, withdrawal_type: true, type: true, status: true }).toArray((err, value) => {
                    if (value.length != 0) {
                        var count = 0;
                        for (var deposit of value) {
                            count++
                            var data = {}
                            data['txid'] = deposit.txid;
                            data['currency'] = deposit.currency;
                            data['created'] = deposit.created;
                            data['client_id'] = deposit.client_id;
                            data['amount'] = deposit.amount;
                            data['withdrawal_type'] = deposit.withdrawal_type;
                            data['type'] = deposit.type;
                            data['status'] = deposit.status
                            result.push(data);

                            if (count == value.length) {
                                cb();
                            }

                        }
                    } else {
                        cb();
                    }
                })
            }, (err) => {
                callback();
            });
        },
        (err) => {
            // console.log(result)
            res.json({ status: 200, data: result, count: result.length });
        }
    ]);
}



function withdraw(req, res, clientId, start_date, end_date) {
    var currencies = [];
    var result = [];
    async.waterfall([
        (callback) => {
            req.db.collection('currencies').find({}).toArray((err, result) => {
                result.forEach((user, key) => {
                    if (user.id == 'inr') {
                        currencies.push('withdrawamounts')
                    } else if (user.id == 'xrp') {
                        currencies.push('xrp_withdrawals')
                    } else {
                        if (user.id == 'dash' || user.id == 'bch') {
                            var postfix = "es";
                        } else {
                            var postfix = "s";
                        }
                        currencies.push('send_tx_' + user.id + postfix)
                    }
                })
                callback()
            })
        },
        (callback) => {
            var currentYear = new Date().getFullYear();
            var currentMonth = new Date().getMonth() + 1;
            var lastDate = new Date(currentYear, currentMonth, 0).getDate();

            if (start_date !== '' && end_date !== '' && clientId !== '') {
                var condition = { client_id: parseInt(clientId), created: { $gte: new Date(start_date), $lt: new Date(end_date) } }
            } else if (start_date !== '' && end_date !== '' && clientId === '') {
                var condition = { created: { $gte: new Date(start_date), $lt: new Date(end_date) } }
            } else if (start_date === '' && end_date === '' && clientId !== '') {
                var condition = { client_id: parseInt(clientId), created: { $gte: new Date(currentYear + '-' + currentMonth + '-' + '1 00:01:00.000Z'), $lt: new Date(currentYear + '-' + currentMonth + '-' + lastDate + ' 00:55:00.000Z') } }
            } else {
                var condition = { created: { $gte: new Date(currentYear + '-' + currentMonth + '-' + '1 00:01:00.000Z'), $lt: new Date(currentYear + '-' + currentMonth + '-' + lastDate + ' 00:55:00.000Z') } }
            }
            //console.log(condition)
            async.forEachSeries(currencies, (receiveCurrencies, cb) => {

                req.db.collection(receiveCurrencies).find(condition, { txid: true, amount: true, currency: true, created: true, client_id: true, withdrawal_type: true, type: true, status: true }).toArray((err, value) => {
                    if (value.length != 0) {
                        var count = 0;
                        for (var deposit of value) {
                            count++
                            var data = {}
                            data['txid'] = deposit.txid;
                            data['currency'] = deposit.currency;
                            data['created'] = deposit.created;
                            data['client_id'] = deposit.client_id;
                            data['amount'] = deposit.amount;
                            data['withdrawal_type'] = deposit.withdrawal_type;
                            data['type'] = deposit.type;
                            data['status'] = deposit.status

                            result.push(data);

                            if (count == value.length) {
                                cb();
                            }

                        }
                    } else {
                        cb();
                    }
                })
            }, (err) => {
                callback();
            });
        },
        (err) => {
            // console.log(result)
            res.json({ status: 200, data: result, count: result.length });
        }
    ]);
}

function trade(req, res, clientId, start_date, end_date) {
    var currencies = [];
    var result = [];
    async.waterfall([
        (callback) => {
            req.db.collection('fees_taxes').find({}).toArray((errs, results) => {
                results.forEach((res, key) => {
                    currencies.push(res.id + '_trade_histories')
                    currencies.push(res.id + '_buys')
                    currencies.push(res.id + '_sells')
                })
                callback()
            })
        },
        (callback) => {
            var currentYear = new Date().getFullYear();
            var currentMonth = new Date().getMonth() + 1;
            var lastDate = new Date(currentYear, currentMonth, 0).getDate();

            if (start_date !== '' && end_date !== '' && clientId !== '') {
                var condition = { client_id: parseInt(clientId), created: { $gte: new Date(start_date), $lt: new Date(end_date) } }
            } else if (start_date !== '' && end_date !== '' && clientId === '') {
                var condition = { created: { $gte: new Date(start_date), $lt: new Date(end_date) } }
            } else if (start_date === '' && end_date === '' && clientId !== '') {
                var condition = { client_id: parseInt(clientId), created: { $gte: new Date(currentYear + '-' + currentMonth + '-' + '1 00:01:00.000Z'), $lt: new Date(currentYear + '-' + currentMonth + '-' + lastDate + ' 00:55:00.000Z') } }
            } else {
                var condition = { created: { $gte: new Date(currentYear + '-' + currentMonth + '-' + '1 00:01:00.000Z'), $lt: new Date(currentYear + '-' + currentMonth + '-' + lastDate + ' 00:55:00.000Z') } }
            }
            //console.log(condition)
            async.forEachSeries(currencies, (receiveCurrencies, cb) => {

                req.db.collection(receiveCurrencies).find(condition, { client_id: true, rate: true, quantity: true, progress: true, type: true, pair: true, exec_rate: true, trade_type: true, order_type: true, created: true, status: true }).toArray((err, value) => {
                    if (value.length != 0) {
                        var count = 0;
                        for (var deposit of value) {
                            count++
                            var data = {}
                            data['client_id'] = deposit.client_id;
                            data['rate'] = deposit.rate;
                            data['quantity'] = deposit.quantity;
                            data['progress'] = deposit.progress;
                            data['created'] = deposit.created;
                            data['type'] = (deposit.type) ? deposit.type + ' order' : 'closed order';
                            data['pair'] = deposit.pair;
                            data['exec_rate'] = deposit.exec_rate;
                            data['trade_type'] = deposit.trade_type;
                            data['order_type'] = deposit.order_type;
                            data['status'] = deposit.status;
                            result.push(data);

                            if (count == value.length) {
                                cb();
                            }

                        }
                    } else {
                        cb();
                    }
                })
            }, (err) => {
                callback();
            });
        },
        (err) => {
            // console.log(result)
            res.json({ status: 200, data: result, count: result.length });
        }
    ]);
}



router.post('/getAdminRecentChanges', commonClass.connect, (req, res, next) => {
    req.db.collection('track_admin_changes').aggregate([
        {
            $lookup:
                {
                    from: 'admin_users',
                    localField: 'admin_id',
                    foreignField: '_id',
                    as: 'admin_details'
                },
        }
    ]).sort({ date: -1 }).limit(30).toArray((err, result) => {
        if (result) {
            console.log("res", result)
            res.json({ status: 300, data_is: result });
        } else {
            res.json({ status: 301 })
        }
    })
})


router.post('/saveTxnData', commonClass.connect, (req, res) => {
    var fields = {
        transaction_id: req.body.txId,
        // received_date: new Date(),
        created: new Date(),
        amount: req.body.amount,
        status: 0,
        for: 'deposit',
        admin_id: req.body.admin_id
    }

    req.db.collection("transaction_ids").insert(fields, (err, data) => {
        if (err) {
            res.status(200).json({ code: 151, message: "Something Went wrong while depositing amount" });
        } else {
            res.status(200).json({ code: 151, message: "Transaction Id Added" });
        }

    });
})



router.post('/getTransactionIds/:p', commonClass.connect, (req, res, next) => {
    var page = req.params.p;
    var perPage = 10,
        page = Math.max(0, page);
    var skip = (perPage * page) - perPage;
    if (req.body.search != '') {
        var conditions = {
            $or: [
                { 'transaction_id': req.body.search },
                { 'amount': req.body.search }
            ]
        }
    } else {
        var conditions = {}
    }
    req.db.collection('transaction_ids').count(conditions, (errCount, statusCount) => {
        req.db.collection('transaction_ids').find(conditions).toArray((err, result) => {
            if (result) {
                res.json({ status: 300, txDetails: result, count: statusCount });
            } else {
                res.json({ status: 301 })
            }
        })
    })

})


router.post('/checkTxId', commonClass.connect, (req, res, next) => {
    req.db.collection('transaction_ids').findOne({ 'transaction_id': req.body.txnId }, (err, result) => {
        if (result) {
            res.json({ status: 300 });
        } else {
            res.json({ status: 301 })
        }
    })
})



router.post('/getPayoutHistories/:p', commonClass.connect, (req, res, next) => {

    //    console.log("Date isss",new Date(req.body.search));
    if (req.body.data) {
        if (req.body.data.start_date) {
            start_date = req.body.data.start_date;
            end_date = req.body.data.end_date;
            console.log(start_date);
            console.log(end_date);

        }
    }





    var page = req.params.p;
    var perPage = 10,
        page = Math.max(0, page);
    var skip = (perPage * page) - perPage;
    if (req.body.search != '') {
        var conditions = {
            $or: [
                { 'client_id': parseInt(req.body.search) },
                { 'amount': req.body.search },
                { 'created': new Date(req.body.search) }
            ]
        }
    } else if (req.body.data) {
        if (req.body.data.start_date) {
            var conditions = {
                created: { $gte: new Date(start_date), $lt: new Date(end_date) }
            }
        }

    } else {
        var conditions = {}
    }
    req.db.collection('hike_coin_histories').count(conditions, (errCount, statusCount) => {
        req.db.collection('hike_coin_histories').find(conditions).toArray((err, result) => {
            if (result) {
                res.json({ status: 300, txDetails: result, count: statusCount });
            } else {
                res.json({ status: 301 })
            }
        })
    })

})




module.exports = router;
