var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

router.post('/getKyc/:isVerified/:p/:country', commonClass.connect, (req, res, next) => {
    var page = req.params.p;
    var perPage = 10,
        page = Math.max(0, page);
    var skip = (perPage * page) - perPage;
        if(req.params.isVerified == 3){
           var kyc_status = {$in:[0,1]}
        }else{
            var kyc_status = parseInt(req.params.isVerified)
        }

        var show_rejected_usr = {
            id_status: 2,
            $or: [  { aadhaar_status: 2 },
            { bank_status: 2 }]
        }

    if (req.params.country == 'ind' && req.params.isVerified == '2') {
        var conditions = {
            id_status: 2,
            $or: [  { aadhaar_status: 2 },
            { bank_status: 2 }],
            country: 'India',
        }
    }else if (req.params.country == 'ind') {
        var conditions = {
            'kyc_status': kyc_status, country: 'India', id_uploads: { $exists: true, $not: { $size: 0 } }, bankDetails_uploads: { $exists: true, $not: { $size: 0 } },
            $or: [ ]
        }
    }else if (req.params.country == 'thai' && req.params.isVerified == '2') {
        var conditions = {
            'kyc_status': kyc_status, country: 'Thailand', id_uploads: { $exists: true, $not: { $size: 0 } }, bankDetails_uploads: { $exists: true, $not: { $size: 0 } },
            show_rejected_usr,
            $or: [ ],
            country: 'Thailand'
        }
    }
    else if (req.params.country == 'thai') {
        var conditions = {
            'kyc_status': kyc_status, country: 'Thailand', id_uploads: { $exists: true, $not: { $size: 0 } }, bankDetails_uploads: { $exists: true, $not: { $size: 0 } },
            $or: [ ]
        }
    }
    else if (req.params.country == 'all' && req.params.isVerified == '2') {
        var conditions = {  
            id_status: 2,
            $or: [  { aadhaar_status: 2 },
            { bank_status: 2 }]
        }
    }else if (req.params.country == 'all') {
        var conditions = {  
            'kyc_status': kyc_status ,
            $or: [ ]
        }
    }
        if (req.body.search != '') {
            conditions.$or.push(
                { full_name: new RegExp(req.body.search, 'i') },
                { client_id: parseInt(req.body.search) },
                { mobile_number: parseInt(req.body.search) },
                { email: new RegExp(req.body.search, 'i') },
                { id_number: req.body.search },
                { aadhaar_number: req.body.search },
            )
        } else {
            conditions.$or.push(
                { client_id: { $nin: [''] } },
            )
        }


        console.log("req.params.isVerified", req.params.isVerified);

    req.db.collection('users').count(conditions, (errCount, statusCount) => {

        req.db.collection('users').find(conditions,
            {
                'id': true,
                'client_id': true,
                'full_name': true,
                'mobile_number': true,
                'email': true,
                'id_number': true,
                'isVerified': true,
                'id_uploads': true,
                'aadhaar_number': true,
                'aadhaar_uploads': true,
                'bankDetails_uploads': true,
                'bank_details': true,
                'bank_status': true,
                'id_status': true,
                'country': true,
                'kyc_status': true,
                'aadhaar_status': true
            }).skip(skip).limit(perPage).sort({crated:1}).toArray((err, result) => {
                if (result) {
                    res.json({ status: 300, users_doc: result, count: statusCount });
                } else {
                    res.json({ status: 301 })
                }
            })
    })
});
module.exports = router;


