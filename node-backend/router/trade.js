var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();
router.post('/TradeList', commonClass.connect, (req, res, next) => {

    // console.log(req.body.searchByType)

    let result = [];
    var page = req.body.page;
    var search = req.body.search;
    var stop = 1;

    var currency_pair = []
    var arr = []
    var trade_pairs = []
    req.db.collection('fees_taxes').find({}).toArray((errs, results) => {
        results.forEach((res, key) => {
            trade_pairs.push(
                res.id
            )
        })
        arr.push({
            currency_pair: trade_pairs
        })
        arr[0].currency_pair.forEach(function (cp) {
            if (req.body.currency == cp && stop == 1) {
                stop = 0;
                if (req.body.searchByType == 0) {
                    var collection_name = cp + '_trade_histories';
                    tradeWithSchemaName(req.db.collection(collection_name), page, res, search);
                } else if (req.body.searchByType == 1) {
                    var collection_name = cp + '_buys';
                    showOpenOrders(req.db.collection(collection_name), page, res, search);
                } else if (req.body.searchByType == 2) {
                    var collection_name = cp + '_sells';
                    showOpenOrders(req.db.collection(collection_name), page, res, search);
                }
            }
        })
    })
})

function showOpenOrders(schemaName, page, end, search) {
    var perPage = 10, page = Math.max(0, page);
    var skip = (perPage * page) - perPage;
    if (search != '') {
        var conditions = {
            $or: [
                { client_id: parseInt(search) },
                { order_type: search },
                { fee_currency: search },
                { pair: search },
                { rate: search },
                { exec_rate: parseInt(search) },
                { quantity: search },
                { trade_type: search },
            ]
        }
    } else {
        var conditions = {}
    }
    schemaName.count(conditions, (errCount, statusCount) => {
        schemaName.find(conditions).skip(skip).limit(perPage).sort({ created: -1 }).toArray((err, status) => {
            if (status) {
                end.json({ code: 300, data: status, count: statusCount })
            } else {
                end.json({ code: 301 })
            }
        });
    });
}

function tradeWithSchemaName(schemaName, page, end, search) {
    var perPage = 10, page = Math.max(0, page);
    var skip = (perPage * page) - perPage;
    if (search != '') {
        // var date = stringToDate(search,"dd/MM/yyyy","/");
        var conditions = {
            $or: [
                { client_id: parseInt(search) },
                { order_type: search },
                { fee_currency: search },
                { pair: search },
                { rate: search },
                { quantity: search },
                { trade_type: search },
                // {created: { $gte: date.start,$lt: date.end} },
            ]
        }
    } else {
        var conditions = {}
    }
    schemaName.count(conditions, (errCount, statusCount) => {
        schemaName.find(conditions).skip(skip).limit(perPage).sort({ created: -1 }).toArray((err, status) => {
            if (status) {
                end.json({ code: 300, data: status, count: statusCount })
            } else {
                end.json({ code: 301 })
            }
        });
    });
}

function stringToDate(_date, _format, _delimiter) {
    var formatLowerCase = _format.toLowerCase();
    var formatItems = formatLowerCase.split(_delimiter);
    var dateItems = _date.split(_delimiter);
    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var month = parseInt(dateItems[monthIndex]);
    var dateChange = parseInt(dateItems[dayIndex]);
    var year = parseInt(dateItems[yearIndex]);
    var formatedDate = {
        start: new Date(year + '-' + month + '-' + dateChange + ' 00:01:00.000Z'),
        end: new Date(year + '-' + month + '-' + dateChange + ' 23:55:00.000Z')
    };
    //console.log(formatedDate)
    return formatedDate;
}



router.post('/getALlTrades', commonClass.connect, (req, res, next) => {
    let result = [];
    var trades = [];

    var page = req.body.page;
    var search = req.body.search;
    var stop = 1;
    var currency_pair = []
    var arr = []
    var trade_pairs = []
    var client_id = parseInt(req.body.client_id)
    req.db.collection('fees_taxes').find({}).toArray((errs, results) => {
        results.forEach((res, key) => {
            trade_pairs.push(
                res.id
            )
        })
        arr.push({
            currency_pair: trade_pairs
        })

        var length = arr[0].currency_pair.length;
        var i = 0;
        arr[0].currency_pair.forEach(function (cp) {
            var collection_name = cp + '_trade_histories';
            i++;

            getALlTradesCLientWise(req.db.collection(collection_name), client_id, res, trades, length, i);
        })
    })
})

function getALlTradesCLientWise(schemaName, client_id, end, trades, length, i) {
    
    var conditions = { 'client_id': client_id }
    schemaName.count(conditions, (errCount, statusCount) => {
        schemaName.find(conditions).sort({ created: -1 }).toArray((err, status) => {
            if (status.length != 0) {
                trades.push(
                    status
                )
                // end.json({ code: 300, data: status, count: statusCount })
            }

            console.log(length);
            console.log(i);
            console.log("------")

            // if(length == i){
            //     console.log("i is =>", i);
            //     // console.log("trade records are", trades);
            // }

        });




    });
}




module.exports = router;

