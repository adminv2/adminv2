var express = require('express');
var routes = express.Router();
var async = require("async");
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();
routes.post('/balance_info', commonClass.connect, (req, res) => {
    var page = req.body.pageNo;
    var perPage = 10,
        page = Math.max(0, page);
    var skip = (perPage * page) - perPage;
    if (req.body.search != '') {
        var conditions = {
            $or: [
                { client_id: parseInt(req.body.search) },
                { email: new RegExp(req.body.search, 'i') },
                { full_name: new RegExp(req.body.search, 'i') },
            ],
        };
    }
    else {
        var conditions = {};
    }
    var status = [];
    var result_is = [];
    var currencies = [];
    var c = {};
    req.db.collection('users').find(conditions).count((errcount, statusCount) => {
        req.db.collection('users').find(conditions, { client_id: 1, email: 1, full_name: true, "balances.currency": 1, "balances.current_balance": 1 }).skip(skip).limit(perPage).toArray((err, status) => {
            // console.log("result iss", status);
            var total = status.length;
            if (err) {
                res.json({ error: err });
            } else {
                for (i = 0; i < status[0].balances.length; i++) {
                    currencies.push(status[0].balances[i].currency)
                }
                async.forEachSeries(status, function (value, nextCall) {
                    var count = 0
                    var v = {};
                    for (balance of value.balances) {
                        count++
                        v[balance.currency] = balance.current_balance
                        if (value.balances.length == count) {
                            result_is.push({
                                client_id: value.client_id,
                                full_name: value.full_name,
                                email: value.email,
                                bala: v
                            });
                            nextCall();
                        }
                    }
                });
                res.json({ status: 200, data: { totalData: result_is, currencies: currencies, count: statusCount } });
            }
        })
    })
});
module.exports = routes;