var express = require('express');
var router = express.Router();
var multer = require('multer');
var async = require('async');
var _ = require('lodash');
var xlstojson = require("xls-to-json-lc");
var notification = require('../notify.js');
var sheetName;
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

var handlebars = require('handlebars');
var fs = require('fs');
var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
});
function randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}
var upload = multer({ //multer settings
    storage: storage,
    fileFilter: function (req, file, callback) { //file filter
        sheetName = file.originalname.split('.')[0];
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(new Error('Wrong extension type'));
        }
        callback(null, true);
    }
}).single('file');
router.post('/upload', commonClass.connect, function (req, res) {
    var exceltojson;
    upload(req, res, function (err) {
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        /** Multer gives us file info in req.file object */
        if (!req.file) {
            res.json({ error_code: 1, err_desc: "No file passed" });
            return;
        }
        /** Check the extension of the incoming file and 
         *  use the appropriate module
         */
        if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
            console.log(req.file.originalname);
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }
        try {
            exceltojson({
                input: req.file.path,
                output: null, //since we don't need output.json
                lowerCaseHeaders: true
            }, function (err, result) {
                if (err) {
                    return res.json({ error_code: 1, err_desc: err, data: null });
                }
                var sheetArray = [];
                var count = 0;
                var sum = 0;
                grouped = [];
                result.forEach(function (o) {
                    if (!this[o.client_id]) {
                        this[o.client_id] = { client_id: o.client_id, amount: 0 };
                        grouped.push(this[o.client_id]);
                        count++;
                    }
                    this[o.client_id].amount += Number(o.amount).toFixed(12);
                }, Object.create(null));
                result.forEach((element, key) => {
                    var adding_amount;
                    adding_amount = parseFloat(element.amount);
                    console.log(adding_amount + '@@@@@@@@@@@@')
                    // adding_amount = adding_amount.toFixed(12);
                    req.db.collection('users').findOne({ "client_id": parseInt(element.client_id) }, { balances: { $elemMatch: { currency: 'btc' } } }, function (er, singleRec) {

                        console.log(typeof singleRec.balances[0].current_balance)
                        console.log(typeof singleRec.balances[0].previous_balance)
                        var curBal = parseFloat(singleRec.balances[0].current_balance) + adding_amount;
                        var prevBal = parseFloat(singleRec.balances[0].previous_balance) + adding_amount;

                        //    var curBal = curBal.toFixed(12);
                        //    var prevBal = prevBal.toFixed(12);

                        console.log("ci iss =======> ",  parseInt(element.client_id));
                        console.log("cur id iss =======> ", curBal);
                        console.log("prv id iss =======> ", prevBal);
                        req.db.collection('users').update(
                            { 'client_id': parseInt(element.client_id), 'balances.currency': 'btc' },
                            {
                                $set: {
                                    'balances.$.current_balance': curBal.toFixed(16), 'balances.$.previous_balance': prevBal
                                        .toFixed(16)
                                }
                            },
                            { multi: true },
                            (errUpdate, statusUpdate) => {
                                console.log("errUpdate", errUpdate)
                                if (statusUpdate) {

                                    console.log("in statusUpdate", statusUpdate.result);


                                    if (statusUpdate.value.balances != []) {
                                        var pastValue = Number(statusUpdate.value.balances[0].current_balance).toFixed(12);
                                        adding_amount = Number(adding_amount).toFixed(12);
                                        var Update_balance = Number((pastValue + adding_amount)).toFixed(12);

                                        var mail_content_admin = Number(adding_amount).toFixed(12) + ' BTC Deposited To Client ID:' + element.client_id + ", 'User's balance has been changed from " + pastValue.toFixed(12) + " to " + Update_balance.toFixed(12);
                                        var admin_subject = element.client_id + " BTC Deposited " + Number(adding_amount).toFixed(12);
                                            fs.readFile("email_templates/payout.html", { encoding: 'utf-8' }, function (error, pgResp) {
                                                var template = handlebars.compile(pgResp);
                                                var replacementsAdmin = {
                                                    mail_content: mail_content_admin,
                                                    full_name: 'Admin'
                                                };
                                                var htmlToSendAdmin = template(replacementsAdmin);
                                                commonClass.mail('akashbtcmonk@mailinator.com', admin_subject, htmlToSendAdmin);
                                            });
                                        sheetArray.push({ client_id: statusUpdate.value.client_id, pastValue: pastValue.toFixed(12), addingbal: element.amount, Update_balance: Update_balance.toFixed(12) });
                                    } else {
                                        sheetArray.push({ client_id: element.client_id, pastValue: 'not update', addingbal: element.amount, Update_balance: 'not update' });
                                    }
                                } else {
                                    sheetArray.push({ client_id: element.client_id, pastValue: 'not update', addingbal: element.amount, Update_balance: 'not update' });
                                }

                                if (result.length == sheetArray.length) {
                                    _.find(sheetArray, function (trns) {
                                        req.db.collection("users").findOne({ "client_id": parseInt(trns.client_id) }, { email: true, device_addresses: true }, function (err, notify) {
                                            if (notify) {
                                                var histObj = {};
                                                histObj.client_id = parseInt(trns.client_id);
                                                histObj.amount = Number(trns.addingbal).toFixed(12);
                                                if (sheetName === "Lending_Withdrawal") {
                                                    histObj.txid = sheetName + "_" + trns.client_id + "_" + ID();
                                                } else {
                                                    histObj.txid = sheetName + "_" + trns.client_id + "_" + ID();
                                                }
                                                histObj.sent_address = "admin";
                                                histObj.transaction_date = new Date();
                                                histObj.created = new Date();
                                                histObj.updated = new Date();
                                                histObj.withdrawal_type = "external";
                                                histObj.confirmations = "3";
                                                histObj.status = "1";
                                                histObj.currency = "btc";
                                                histObj.type = "receive"
                                                req.db.collection("rec_tx_con_btcs").insertOne(histObj, function (err, res) {
                                                    if (err) {
                                                        console.log("dipoist history-error", err, res);
                                                    } else {
                                                        console.log("dipoist history");

                                                        histObj = {};
                                                    }
                                                })
                                            } else {
                                                console.log("No data for this client_id:", trns.client_id);
                                            }

                                        })
                                    })
                                    res.json({ code: 200, data: sheetArray });
                                }
                            })
                    })

                });
            });
        } catch (e) {
            res.json({ error_code: 1, err_desc: "Corupted excel file" });
        }
    })
});
function uniqueNumber() {
    var date = Date.now();
    if (date <= uniqueNumber.previous) {
        date = ++uniqueNumber.previous;
    } else {
        uniqueNumber.previous = date;
    }
    return date;
}
uniqueNumber.previous = 0;
function ID() {
    return uniqueNumber();
};
module.exports = router;
