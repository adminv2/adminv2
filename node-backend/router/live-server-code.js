router.post('/getId',(req,res,next)=>{
    var client_id = 17996;
    db.Currency.find({}, { id: 1, name: 1, fees: 1, min_withdrawal: 1, is_fiat: 1, icon: 1 }, (err, currencyDetails) => {
        if (err) { }
        else {
            console.log(currencyDetails)
            if (currencyDetails) {
                var currencyInfo = [];
                var count = 0;
                async.forEachSeries(currencyDetails, (currency, callback) => {
                    var id = currency.id;
                    var cur = {};
                    var database = null;
                    var inrDatabase = null;
                    // var isFiat = false;
                    var is_fiat = currency.is_fiat;
                    database = db.TradeBook[`${id}_btc_trade_history`];

                    switch (id) {
                        case "inr": { database = db.TradeBook[`btc_${id}_trade_history`]; break; }
                        case "thb": { database = db.TradeBook[`btc_${id}_trade_history`]; break; }
                        /*
                        case "btc": { inrDatabase = db.BtcInrTradeHistory; break; }
                        case "xrp": { database = db.XrpBtcTradeHistory; inrDatabase = db.XrpInrTradeHistory; break; }
                        case "xlm": { database = db.XlmBtcTradeHistory; inrDatabase = db.XlmInrTradeHistory; break; }
                        case "bch": { database = db.BchBtcTradeHistory; inrDatabase = db.BchInrTradeHistory; break; }
                        case "btg": { database = db.BtgBtcTradeHistory; inrDatabase = db.BtgInrTradeHistory; break; }
                        case "ltc": { database = db.LtcBtcTradeHistory; inrDatabase = db.LtcInrTradeHistory; break; }
                        case "pura": { database = db.PuraBtcTradeHistory; inrDatabase = db.PuraInrTradeHistory; break; }
                        case "dash": { database = db.DashBtcTradeHistory; inrDatabase = db.DashInrTradeHistory; break; }
                    */
                    }
                 console.log(database+'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@=========================')
                    var name = "";
                    var btc_factor = 0;
                    var inr_factor = 0;
                    var icon = "";
                    var fees = 0;
                    if (database) {
                        async.waterfall([
                            (callback) => {
                                database.find({ status: { $ne: 3 } }).sort({ _id: -1 }).limit(1).exec((err, details) => {
                                    if (err) { callback(err); }
                                    else {
                                        if (details.length != 0) {
                                            btc_factor = details[0].rate;
                                            if (is_fiat) {
                                                btc_factor = truncateValue((1 / details[0].rate), precisions.btc);
                                                if (id == 'inr') {
                                                    inr_factor = 1;
                                                }
                                            }
                                        }
                                        callback();
                                    }
                                });
                            },
                            (callback) => {
                                if (inrDatabase) {
                                    inrDatabase.find({ status: { $ne: 3 } }).sort({ _id: -1 }).limit(1).exec((err, details) => {
                                        if (err) { callback(err); }
                                        else {
                                            if (details.length != 0) {
                                                inr_factor = details[0].rate;
                                                if (id == 'inr') {
                                                    inr_factor = 1;
                                                }
                                            }
                                            callback();
                                        }
                                    });
                                } else {
                                    callback();
                                }
                            }
                        ], (err) => {
                            if (err) { }
                            else {
                                db.User.findOne({ client_id: client_id, "balances.currency": id }, { balances: { $elemMatch: { currency: id } } }, (err, userDetails) => {
                                    if (userDetails) {
                                        var previous_balance = userDetails.balances[0].previous_balance;
                                        var current_balance = userDetails.balances[0].current_balance;

                                        previous_balance = truncateValue(previous_balance, vPrecisions[id], false);
                                        current_balance = truncateValue(current_balance, vPrecisions[id], false);
                                        //////////////////////////////////////
                                        /// FOR TESTING ///
                                        ///////////////////
                                        // if (client_id == 115626 || client_id == 115734 || client_id == 115733) {
                                            // previous_balance = arthOperation(id, previous_balance, Math.pow(10, precisions[id]), "/", true);
                                            // current_balance = arthOperation(id, current_balance, Math.pow(10, precisions[id]), "/", true);
                                        // }
                                        //////////////////////////////////////
                                        name = currency.name;
                                        icon = currency.icon;
                                        fees = currency.fees;
                                        var min_withdrawal = currency.min_withdrawal;
                                        cur.id = id;
                                        cur.name = name;
                                        cur.btc_factor = btc_factor;
                                        cur.inr_factor = inr_factor;
                                        cur.icon = icon;
                                        cur.fees = fees;
                                        cur.min_withdrawal = min_withdrawal;
                                        cur.is_fiat = is_fiat;
                                        cur.previous_balance = previous_balance;
                                        cur.current_balance = current_balance;
                                        currencyInfo.push(cur);
                                    }
                                    count++;
                                    callback();
                                });
                            }
                        });
                    } else {
                        async.waterfall([
                            (callback) => {
                                if (inrDatabase) {
                                    inrDatabase.find({ status: { $ne: 3 } }).sort({ _id: -1 }).limit(1).exec((err, details) => {
                                        if (err) { callback(err); }
                                        else {
                                            if (details.length != 0) {
                                                inr_factor = details[0].rate;
                                                if (id == 'inr') {
                                                    inr_factor = 1;
                                                }
                                            }
                                            callback();
                                        }
                                    });
                                } else {
                                    callback();
                                }
                            }
                        ], (err) => {
                            if (err) {
                            } else {
                                db.User.findOne({ client_id: client_id, "balances.currency": id }, { balances: { $elemMatch: { currency: id } } }, (err, userDetails) => {
                                    if (userDetails) {
                                        var previous_balance = userDetails.balances[0].previous_balance;
                                        var current_balance = userDetails.balances[0].current_balance;
                                        
                                        previous_balance = truncateValue(previous_balance, vPrecisions[id], false);
                                        current_balance = truncateValue(current_balance, vPrecisions[id], false);
                                        //////////////////////////////////////
                                        /// FOR TESTING ///
                                        ///////////////////
                                        // if (client_id == 115626 || client_id == 115734 || client_id == 115733) {
                                            // previous_balance = arthOperation(id, previous_balance, Math.pow(10, precisions[id]), "/", true);
                                            // current_balance = arthOperation(id, current_balance, Math.pow(10, precisions[id]), "/", true);
                                        // }
                                        //////////////////////////////////////
                                        name = currency.name;
                                        btc_factor = 1;
                                        icon = currency.icon;
                                        fees = currency.fees;
                                        var min_withdrawal = currency.min_withdrawal;
                                        cur.id = id;
                                        cur.name = name;
                                        cur.btc_factor = btc_factor;
                                        cur.inr_factor = inr_factor;
                                        cur.icon = icon;
                                        cur.fees = fees;
                                        cur.min_withdrawal = min_withdrawal;
                                        cur.is_fiat = is_fiat;
                                        cur.previous_balance = previous_balance;
                                        cur.current_balance = current_balance;
                                        currencyInfo.push(cur);
                                    }
                                    count++;
                                    callback();
                                });
                            }
                        });
                    }
                }, (err) => {
                    if (err) {
                        res.json({ code: 1212, message: "Something went wrong" });
                       // socket.emit('currencies', JSON.stringify({ code: 1212, message: "Something went wrong" }));
                    } else {
                        if (count == currencyDetails.length) {
                            res.json({ code: 1212, message: currencyInfo });
                            //socket.emit('currencies', JSON.stringify(currencyInfo));
                        }
                    }
                });
            }
        }
    });
})