var express = require('express');
var routes = express.Router();
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();
routes.post('/referals', commonClass.connect, (req, res) => {
  // console.log(req.body.search);

  var perPage = 10, page = Math.max(0, req.body.pageUrl);
  var skip = (perPage * page) - perPage;
  //  console.log("req.body.search",req.body.search);
  if (req.body.search != '') {
    conditions = { $or: [{ referring_user: parseInt(req.body.search) }, { referred_user: parseInt(req.body.search) }] }
  } else {
    conditions = null;
  }
  req.db.collection('referral_individuals').find(conditions).count((errcount, statusCount) => {
    req.db.collection('referral_individuals').find(conditions).skip(skip).limit(perPage).sort({ referring_user: -1, 'balances.currecy': -1, 'balances.currency': -1 }).toArray((err, status) => {
      if (err) {
        res.json({ error: err });
      } else {
        var currencies_are = [];
        if(status != ''){
          for (i = 0; i < status[0].balances.length; i++) {
            currencies_are.push(status[0].balances[i].currency);
          }
        }
        res.json({ status: 200, data: status, count: statusCount, currencies_are: currencies_are });
      }
    })
  })

  
  // req.db.collection('referral_individuals').find(conditions).count((errcount, statusCount) => {
  //   req.db.collection('referral_individuals').find(conditions).skip(skip).limit(perPage).sort({ referring_user: -1, 'balances.currency': -1}).toArray((err, status) => {
  //     if (err) {
  //       res.json({ error: err });
  //     } else {
  //       res.json({ status: 200, data: status, count: statusCount });
  //     }
  //   })
  // })

});


module.exports = routes;