var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

var async = require("async");

router.post('/usersList/:isVerified/:p', commonClass.connect, (req, res, next) => {
    //abc(req)
    var page = req.params.p;
    var perPage = 10,
        page = Math.max(0, page);
    var skip = (perPage * page) - perPage;

    if (req.body.search != '') {
        var conditions = {
            'kyc_status': parseInt(req.params.isVerified),
            $or: [
                { full_name: new RegExp(req.body.search, 'i') },
                { client_id: parseInt(req.body.search) },
                { mobile_number: parseInt(req.body.search) },
                { email: new RegExp(req.body.search, 'i') }]
        }
    } else {
        var conditions = { 'kyc_status': parseInt(req.params.isVerified) }
    }
    req.db.collection('users').count(conditions, (errCount, statusCount) => {
        req.db.collection('users').find(conditions,
            {
                'id': true,
                'client_id': true,
                'full_name': true,
                'mobile_number': true,
                'email': true,
                'isVerified': true,
                'id_status': true,
                'country': true,
                'kyc_status': true,
                'suspended': true,
                'created':true
            }).skip(skip).limit(perPage).sort({'created':-1}).toArray((err, result) => {
                if (result) {
                    res.json({ status: 300, users_doc: result, count: statusCount });
                } else {
                    res.json({ status: 301 })
                }
            })
    })
});


router.post('/getNotifications', commonClass.connect, (req, res, next) => {
    req.db.collection('notifications').find({ client_id: parseInt(req.body.client_id) }, {}).toArray((err, result) => {
        if (result) {
            res.json({ status: 300, notifications: result });
        } else {
            res.json({ status: 301 })
        }
    })
});
router.post('/userprofile_info', commonClass.connect, (req, res, next) => {
    var array = []
    var balances = []

    req.db.collection('users').findOne({ client_id: parseInt(req.body.client_id) }, (err, result) => {
        var count = result.balances.length;
        for (var i = 0; i < count; i++) {
            array.push({
                total_withdrawal: result.balances[i].total_withdrawal,
                total_deposit: result.balances[i].total_deposit,
                updated: result.balances[i].updated,
                current_balance: result.balances[i].current_balance,
                previous_balance: result.balances[i].previous_balance,
                currency: result.balances[i].currency,
            })
            // window = {}

        }
        result.balances = array;
        if (err) {
            res.json({ error: err });
        } else {
            res.json({ status: 200, data: result });
        }
    })
});
router.post('/checkUserExists', commonClass.connect, (req, res, next) => {
    req.db.collection('users').findOne({ client_id: parseInt(req.body.client_id) }, { full_name: true }, (err, result) => {
        if (err) {
            res.json({ status: 400, error: err });
        } else {
            if (result) {
                res.json({ status: 200 });
            } else {
                res.json({ status: 300 });
            }
        }
    })
});
router.post('/checkEmailExists', commonClass.connect, (req, res, next) => {
    req.db.collection('users').findOne({ email: req.body.email, client_id: { $ne: parseInt(req.body.client_id) } }, { client_id: true }, (err, result) => {
        if (err) {
            res.json({ status: 400, error: err });
        } else {
            if (result) {
                res.json({ status: 200 });
            } else {
                res.json({ status: 300 });
            }
        }
    })
});
router.post('/sendMail', commonClass.connect, (req, res, next) => {
    req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { email: true, full_name: true }, (err, user) => {
        if (user) {
            commonClass.mail(user.email, req.body.subject, req.body.message);
            res.json({ status: 200, 'message': "Sent Successfully" });
        } else {
            res.json({ status: 400, 'message': "Something Went Wrong" });
        }
    })
})

// get user by client
router.post('/getEmailByClient', commonClass.connect, (req, res) => {
    req.db.collection('users').findOne({ client_id: parseInt(req.body.client_id) }, (err, result) => {
        if (err) {
            res.json({ error: err });
        } else {
            res.json({ status: 200, data: result, code: 152 });
        }
    })
});

router.post('/getClientName', commonClass.connect, (req, res) => {
    req.db.collection('users').findOne({ client_id: parseInt(req.body.client_id) }, { full_name: true }, (err, result) => {
        if (err) {
            res.json({ error: err });
        } else {
            res.json({ status: 200, data: result, code: 152 });
        }
    })
});
router.post('/usersDailyInterest/:p', commonClass.connect, (req, res, next) => {
    var perPage = 10;
    var page = Math.max(0, req.params.p);
    var skip = (perPage * page) - perPage;
    req.db.collection('dailyinterests').count({ client_id: parseInt(req.body.client_id) }, (errCount, statusCount) => {
        req.db.collection('dailyinterests').find({ client_id: parseInt(req.body.client_id) }).skip(skip).limit(perPage).sort({ date: -1 }).toArray((err, result) => {
            if (err) {
                res.json({ error: err });
            } else {
                res.json({ status: 200, data: result, count: statusCount })
            }
        })
    })
});
router.post('/updateUsersInfo', commonClass.connect, (req, res) => {
    req.body.date_of_birth = new Date(req.body.date_of_birth)
    var obj = req.body;

    var save_data = {
        admin_id: ObjectId(req.body.updated_by),
        type: "user_profile_updated",
        client_id: parseInt(req.body.client_id),
        date: new Date()
    }



    req.db.collection('users').update({ 'client_id': parseInt(req.body.client_id) }, { $set: obj }, (errUpdate, statusUpdate) => {
        if (statusUpdate) {
            req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })

            req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { email: true }, { device_addresses: true }, (err, status) => {
                var mail_content = "Hello " + req.body.full_name + ", <br>  Your Basic Info has been changed";
                res.json({ status: 200, message: "Users Details updated successfully", data: obj })

                // notification(status.device_addresses,`Your basic Info at BTCMonk has been updated`,"Basic Info Updated")
            })
        } else {
            res.json({ status: 400, message: "Something went wrong while updating users info" });
        }
    })
});

router.post('/addUsersBank', commonClass.connect, (req, res) => {
    var save_data = {
        admin_id: ObjectId(req.body.updated_by),
        type: "user_profile_updated",
        client_id: parseInt(req.body.client_id),
        date: new Date()
    }

    req.db.collection('users').update({ client_id: parseInt(req.body.client_id) }, {
        $push: {
            bank_details: {
                $each: [{
                    registered_name: req.body.registered_name,
                    account_number: req.body.account_number,
                    ifsc_code: req.body.ifsc_code,
                    bank_name: req.body.bank_name,
                    bank_address: req.body.bank_address
                }]
            }
        }
    }, (err, data) => {
        if (data) {
            req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })

            res.json({ status: 200, message: "Bank Details Updated Successfully", data: data })
        } else {
            res.json({ status: 400, message: "Something went wrong while updating users info" });
        }
    });
});


router.post('/updateWithdlimits', commonClass.connect, (req, res) => {
    // var save_data = {
    //     admin_id: ObjectId(req.body.updated_by),
    //     type: "withdrawal_limit_updated",
    //     client_id: parseInt(req.body.client_id),
    //     changed_to_btc : req.body.withdrawal_limit,
    //     changed_to_inr : req.body.withdrawal_limit_inr,
    //     date: new Date()
    //   }

    req.body.client_id = parseInt(req.body.client_id);
    obj = req.body;
    req.db.collection('users').update({ 'client_id': parseInt(req.body.client_id) }, { $set: obj }, (errUpdate, statusUpdate) => {
        if (statusUpdate) {
            req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { full_name: true, email: true }, { device_addresses: true }, (err, status) => {
                var mail_content = "Hello " + status.full_name + ", <br>  Your BTCMonk withdrawal limits has been changed to " + req.body.withdrawal_limit + " BTC and INR limit is " + req.body.withdrawal_limit_inr;

                // req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })

                commonClass.mail(status.email, 'BTCMonk Withdrawal Limit Changed', mail_content);
                res.json({ status: 200, message: "Withdrawals Limits updated successfully" })
            })
        } else {
            res.json({ status: 400, message: "Something went wrong while updating Withdrawals Limits" });
        }
    });
});


router.post('/ChangeStatus', commonClass.connect, (req, res, next) => {
    req.db.collection('users').findOneAndUpdate({ 'client_id': parseInt(req.body.id) }, { $set: { "suspended": req.body.status } }, (saveErr, saveStatus) => {
        if (saveStatus) {
            req.db.collection('users').update({ 'client_id': parseInt(req.body.id) }, {
                $push: {
                    suspend_details: {
                        $each: [{
                            suspend_reason: req.body.reason,
                            suspend_changed_by: req.body.suspend_changed_by,
                            on_date: new Date(),
                            status: req.body.status
                        }]
                    }
                }
            }, (err, data) => {

                if (data) {
                    res.json({ msg: "success", status: 200 });
                } else {
                    res.json({ msg: "err", status: 202 });
                }
            });
        } else {
            res.json({ msg: "err", status: 202 });
        }
    })
});

router.post('/changeWithdrawalLimitStatus', commonClass.connect, (req, res, next) => {
    if (req.body.status == 0) {
        blocked = 1;
    } else {
        blocked = 0;
    }

    var save_data = {
        admin_id: ObjectId(req.body.updated_by),
        type: "withdrawal_status_changed",
        client_id: parseInt(req.body.client_id),
        changed_to: req.body.status ? 0 : 1,
        date: new Date()
    }

    req.db.collection('users').findOneAndUpdate({ 'client_id': parseInt(req.body.client_id) }, { $set: { "blocked": blocked } }, (saveErr, saveStatus) => {
        if (saveStatus) {

            req.db.collection('track_admin_changes').insert(save_data, (err, data) => { })

            res.json({ msg: "success", status: 200, blocked_status: blocked });
        } else {
            res.json({ msg: "err", status: 202 });
        }
    })
});


router.post('/updateKyc', commonClass.connect, (req, res) => {
    req.body.client_id = parseInt(req.body.client_id);
    var obj = req.body;

    req.db.collection('users').update({ 'client_id': parseInt(req.body.client_id) }, { $set: obj }, (errUpdate, statusUpdate) => {
        if (statusUpdate) {
            // req.db.collection('users').findOne({ 'client_id': parseInt(req.body.client_id) }, { email: true }, { device_addresses: true }, (err, status) => {
            //     var mail_content = "Hello " + req.body.full_name + ", <br>  Your Basic Info has been changed";
            //     commonClass.mail(status.email, 'BTCMonk Basic Info Changed', mail_content);
            res.json({ status: 200, message: "Users Details updated successfully" })
            // })
        } else {
            res.json({ status: 400, message: "Something went wrong while updating users info" });
        }
    })
});

router.post('/updateBalance/:amount/:currency/:client_id/:updated_by', commonClass.connect, (req, res) => {
    var currency_is = req.params.currency;
    var client_id = parseInt(req.params.client_id);
    // var update_balance = parseFloat(req.params.amount);
    var update_balance = req.params.amount;
    var admin_id = ObjectId(req.params.updated_by);

    var update_balance = Number(update_balance).toFixed(12)

    req.db.collection('users').findOne({ 'client_id': client_id }, { balances: { $elemMatch: { currency: currency_is } }, email: true, full_name: true }, (err, result) => {
        if (result) {
            var fields = {
                currency: currency_is,
                client_id: client_id,
                previous_balance: result.balances[0].previous_balance,
                currenct_balance: result.balances[0].current_balance,
                updated_previous_balance: update_balance,
                updated_current_balance: update_balance,
                updated: new Date(),
                admin_id: admin_id
            }
            req.db.collection("update_balances").insert(fields, (err, data) => {
                if (err) {
                    res.status(200).json({ code: 151, message: "Something Went wrong while updating Balance" });
                } else {
                    req.db.collection('users').update({ 'client_id': client_id, 'balances.currency': currency_is }, { $set: { 'balances.$.current_balance': update_balance, 'balances.$.previous_balance': update_balance } }, (errUpdate, statusUpdate) => {
                        if (statusUpdate) {
                            res.json({ status: 200, message: "Users Balance updated successfully" })
                        } else {
                            res.json({ status: 400, message: "Something went wrong while updating Balance" });
                        }
                    })
                }
            });
        }
    })

});


router.post('/getEquivalentBalance', commonClass.connect, (req, res, next) => {
    var client_id = parseInt(req.body.client_id);

    //req.db.collection('users')
    req.db.collection('currencies').find({}, { id: 1, name: 1, fees: 1, min_withdrawal: 1, is_fiat: 1, icon: 1 }).toArray((err, currencyDetails) => {
        if (err) { }
        else {
            if (currencyDetails) {
                var currencyInfo = [];
                var count = 0;
                async.forEachSeries(currencyDetails, (currency, callback) => {
                    var id = currency.id;
                    var cur = {};
                    var database = null;
                    var inrDatabase = null;
                    var is_fiat = currency.is_fiat;
                    database = req.db.collection(`${id}_btc_trade_histories`);

                    switch (id) {
                        case "inr": { database = req.db.collection(`btc_${id}_trade_histories`); break; }
                        case "thb": { database = req.db.collection(`btc_${id}_trade_histories`); break; }
                    }

                    //console.log(database)
                    var name = "";
                    var btc_factor = 0;
                    var inr_factor = 0;
                    var icon = "";
                    var fees = 0;
                    if (database) {
                        async.waterfall([
                            (callback) => {
                                database.find({ status: { $ne: 3 } }).sort({ _id: -1 }).limit(1).toArray((err, details) => {

                                    if (err) { callback(err); }
                                    else {
                                        if (details.length != 0) {
                                            btc_factor = details[0].rate;
                                            if (is_fiat) {
                                                btc_factor = (1 / details[0].rate);
                                                if (id == 'inr') {
                                                    inr_factor = 1;
                                                }
                                            }
                                        } else {
                                            btc_factor = 1;
                                        }
                                        callback();
                                    }
                                });
                            },
                            (callback) => {
                                if (inrDatabase) {
                                    inrDatabase.find({ status: { $ne: 3 } }).sort({ _id: -1 }).limit(1).exec((err, details) => {
                                        if (err) { callback(err); }
                                        else {
                                            if (details.length != 0) {
                                                inr_factor = details[0].rate;
                                                if (id == 'inr') {
                                                    inr_factor = 1;
                                                }
                                            }
                                            callback();
                                        }
                                    });
                                } else {
                                    callback();
                                }
                            }
                        ], (err) => {
                            if (err) { }
                            else {
                                req.db.collection('users').findOne({ client_id: client_id, "balances.currency": id }, { balances: { $elemMatch: { currency: id } } }, (err, userDetails) => {
                                    if (userDetails) {
                                        var previous_balance = userDetails.balances[0].previous_balance;
                                        var current_balance = userDetails.balances[0].current_balance;

                                        //previous_balance = truncateValue(previous_balance, vPrecisions[id], false);
                                        // current_balance = truncateValue(current_balance, vPrecisions[id], false);

                                        name = currency.name;

                                        cur.id = id;
                                        cur.name = name;
                                        cur.btc_factor = btc_factor;
                                        cur.previous_balance = previous_balance;
                                        cur.current_balance = current_balance;
                                        cur.total = parseFloat(previous_balance) * parseFloat(btc_factor);

                                        currencyInfo.push(cur);
                                    }
                                    count++;
                                    callback();
                                });
                            }
                        });
                    }
                }, (err) => {
                    if (err) {
                        res.json({ code: 400, message: "Something went wrong" });
                    } else {
                        if (count == currencyDetails.length) {
                            var totalBalnce = 0;
                            currencyInfo.forEach(function (val, key) {
                                totalBalnce += val.total;
                            });
                            res.json({ code: 401, data: currencyInfo, total_balance: totalBalnce });
                        }
                    }
                });
            }
        }
    });

})



router.post('/ChangeGoogleAuthStatus', commonClass.connect, (req, res, next) => {
    req.db.collection('users').findOneAndUpdate({ 'client_id': parseInt(req.body.id) }, { $set: { "googleAuthenticator.status": req.body.status } }, (saveErr, saveStatus) => {
        if (saveStatus) {
            res.json({ msg: "success", status: 200 });
        } else {
            res.json({ msg: "err", status: 202 });
        }
    })
});




module.exports = router;
