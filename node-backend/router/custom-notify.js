var express = require('express'),
    nodemailer = require('nodemailer'),
    routes = express.Router();
_ = require("lodash");
var notification = require('../notify.js');
var async = require("async");

const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

routes.post('/all/notify', commonClass.connect, (req, res, next) => {
    if (req.body.all && req.body.message && req.body.title) {
        req.db.collection('users').find({ device_addresses: { $exists: true, $ne: [] } }, { device_addresses: true }).toArray().then((notificationData) => {
            console.log("notificationData", notificationData.length);
            notificationData.forEach((element, key) => {
                if (notificationData.length === key) {
                    res.json({ status: 200, message: "notification sent to all users" });
                }
            })
        }).catch((err) => {
            console.log("error", err);
            res.json({ status: 500, message: err });
        })
    }
});
routes.post('/notify', commonClass.connect, (req, res, next) => {
    if (req.body.all === 1) {
        req.db.collection('users').count({}, (err, result) => {
            var to_skip = 0;
            var batch_total_records = 0;
            // var batch_of = 10000;
            // var limit_iss = 10000;
            var batch_of = 10000;
            var limit_iss = 10000;
            // var result = 2;
            // var limit_is is useless, will need to remove that variable.
            var batch_total_records = Math.round(result / batch_of);
            var x = 0;
            var array = [];
            for (i = 0; i < batch_total_records; i++) {
                if (i == 0) {
                    array.push({
                        to_skip: 0,
                        limit_is: batch_of
                    })
                } else {
                    to_skip = to_skip + batch_of;
                    limit_is = batch_of + to_skip;
                    array.push({
                        to_skip: to_skip,
                        limit_is: limit_is
                    })
                }
            }
            async.forEachSeries(array, function (item, next) {
                var count = 0;
                req.db.collection('users').find({}, { client_id: true, device_addresses: true }).skip(item.to_skip).limit(limit_iss).toArray((err, result) => {
                    _.forEach(result, function (value, key) {
                        // if (value.device_addresses.length != 0) {
                        if (value.device_addresses != 0) {
                            // console.log("____");
                            commonClass.sendingNotifications(value.client_id,value.device_addresses, 'admin', req.body.message, req.body.title,req.db,req.body.link
                        )
                       }
                        count++;
                        if (result.length == count) {
                            console.log('===================================================', item.limit_is)
                            next();
                        }
                    });
                })
            }, () => {
                res.json({ status: 200, message: "Notification sent to all users" });
                // console.log('done loop')
            })
        })
    }
    else {
        if (req.body.to.indexOf(',') > 0) {
            var emailarray = req.body.to.split(',');
            if (emailarray && emailarray.length && req.body.message && req.body.title) {
                req.db.collection('users').find({ email: { "$in": emailarray } }, { device_addresses: true, client_id: true }).toArray((err, notificationData) => {
                    console.log(err, notificationData);
                    if (err) {
                        console.log("error mode", err);
                    } else {
                        notificationData.forEach((notiData, notiKeys) => {
                            if (notiData.device_addresses && notiData.device_addresses.length > 0) {

                                commonClass.sendingNotifications(notiData.client_id, notiData.device_addresses, 'admin', req.body.message, req.body.title, req.db, req.body.link)

                                // notification(notiData.device_addresses, req.body.message, req.body.title);


                                notiData.device_addresses.forEach((val, key) => {
                                    if (notificationData.length - 1 === notiKeys && notiData.device_addresses.length - 1 === key) {
                                        res.json({ status: 200, message: "Notification sent sucessfully" });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        } else {
            if (req.body.to && req.body.message && req.body.title) {
                req.db.collection('users').findOne({ email: req.body.to }, { device_addresses: true, client_id: true }, (err, notificationData) => {
                    if (err) {
                        console.log("while fetching the data you are facing the error", err);
                    } else {
                        if (notificationData.device_addresses && notificationData.device_addresses.length > 0) {
                            commonClass.sendingNotifications(notificationData.client_id, notificationData.device_addresses, 'admin', req.body.message, req.body.title, req.db,req.body.link)
                            // notification(notificationData.device_addresses, req.body.message, req.body.title);
                            res.json({ status: 200, message: "notification sent sucessfully" });
                        } else {
                            console.log("notifications data not found");
                            res.json({ status: 404, message: "notifications data not found" });
                        }
                    }
                });
            } else {
                res.json({ status: 401, message: "required fields" });
            }
        }
    }
});


module.exports = routes;