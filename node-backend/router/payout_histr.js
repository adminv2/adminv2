var express = require('express');
var router = express.Router();
var multer = require('multer');
var async = require('async');
var _ = require('lodash');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
var notification = require('../notify.js');
var sheetName;
var payOutArray = [];

const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
});
function randomName() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}
var upload = multer({ //multer settings
    storage: storage,
    fileFilter: function (req, file, callback) { //file filter
        sheetName = file.originalname.split('.')[0];
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(new Error('Wrong extension type'));
        }
        callback(null, true);
    }
}).single('file');
router.post('/histr', commonClass.connect, function (req, res) {
    var exceltojson;
    upload(req, res, function (err) {
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        /** Multer gives us file info in req.file object */
        if (!req.file) {
            res.json({ error_code: 1, err_desc: "No file passed" });
            return;
        }
        /** Check the extension of the incoming file and 
         *  use the appropriate module
         */
        if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }
        try {
            var count = 0;
            exceltojson({
                input: req.file.path,
                output: null, //since we don't need output.json
                lowerCaseHeaders: true
            }, function (err, result) {
                if (err) {
                    return res.json({ error_code: 1, err_desc: err, data: null });
                } else {
                    console.log("result",result);
                 _.forEach(result, function (trns) {
                        var histObj = {};
                        histObj.client_id = parseInt(trns.client_id);
                        histObj.amount = parseFloat(trns.amount);
                        if (sheetName === "Lending_Withdrawal") {
                            histObj.txid = sheetName + "_" + trns.client_id + "_" + ID();//Lending_Withdrawal
                        } else {
                            histObj.txid = sheetName + "_" + trns.client_id + "_" + ID();//Mining_Output
                        }
                        histObj.sent_address = "admin";
                        histObj.transaction_date = new Date("2018-04-06T22:58:00Z");
                        histObj.created = new Date("2018-04-06T16:58:00Z");
                        histObj.updated = new Date("2018-04-06T17:58:00Z");
                        histObj.withdrawal_type = "external";
                        histObj.confirmations = 3;
                        histObj.status = 1;
                        histObj.currency = "btc";
                        histObj.type = "receive"
                        async.waterfall([
                            function create(cb) {
                                req.db.collection("rec_tx_con_btcs").insertOne(histObj, function (err, res) {
                                    if (err) {
                                        console.log("dipoist history-error", err, res);
                                    } else {
                                        console.log(res)
                                        payOutArray.push(res.ops[0]);
                                        histObj = {};
                                    }
                                   // console.log("hi");
                                    if (payOutArray.length === result.length) {
                                       // console.log("hello");
                                        cb(payOutArray);
                                        payOutArray.length=0;
                                    }
                                })
                             }
                        ], function (arrayData) {
                            return res.json({ code: 200, data: arrayData });
                        });
                      }) 
                    }
                 });
        } catch (e) {
            res.json({ error_code: 1, err_desc: "Corupted excel file" });
        }
    })
});

function uniqueNumber() {
    var date = Date.now();
    if (date <= uniqueNumber.previous) {
        date = ++uniqueNumber.previous;
    } else {
        uniqueNumber.previous = date;
    }
    return date;
}
uniqueNumber.previous = 0;
function ID() {
    return uniqueNumber();
};
module.exports = router;