var express = require('express');
var router = express.Router();
var async = require('async');
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();
router.get('/individualDeposits/:id', commonClass.connect, (req, res, next) => {
  var locals = {};
  async.series([
    function depositinrs(callback) {
      req.db.collection('depositinrs').find({ client_id: parseInt(req.params.id) }).toArray(function (err, inrData) {
        locals.inr_deposit = inrData;
        callback();
      })
     },
    function rec_tx_con_ltcs(callback) {
      req.db.collection('rec_tx_con_ltcs').find({ client_id: parseInt(req.params.id) }).toArray(function (err, tcsData) {
        locals.ltcs = tcsData;
        callback();
      })
    },
    function xrp_deposits(callback) {
      req.db.collection('xrp_deposits').find({ client_id: parseInt(req.params.id) }).toArray(function (err, xrpData) {
        locals.xrp_deposits = xrpData;
        callback();
      })
    },
    function btc_deposits(callback) {
      req.db.collection('rec_tx_con_btcs').find({ client_id: parseInt(req.params.id) }).toArray(function (err, btcData) {
        locals.btc_deposits = btcData;
        callback();
      })
    },
    function bchs_deposits(callback) {
      req.db.collection('rec_tx_con_bchs').find({ client_id: parseInt(req.params.id) }).toArray(function (err, bchData) {
        locals.bch_deposits = bchData;
        callback();
      })
    },
    function xlm_deposits(callback) {
      req.db.collection('xlm_deposits').find({ client_id: parseInt(req.params.id) }).toArray(function (err, xlmData) {
        locals.xlm_deposits = xlmData;
        callback();
      })
    },
    function btg_deposits(callback) {
      req.db.collection('rec_tx_con_btgs').find({ client_id: parseInt(req.params.id) }).toArray(function (err, btgData) {
        locals.btg_deposits = btgData;
        callback();
      })
    }
  ],function (err) {
    if (err) {
      console.log(err);
    }
    res.json({ status: 200,data:locals})
  })
})
module.exports = router;

