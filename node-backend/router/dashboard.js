var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
var async = require("async");
var dateFormat = require('dateformat');
const commonClassObject = require('../common.js');
var commonClass = new commonClassObject();

router.post('/adminStats', commonClass.connect, (req, res, next) => {
    req.db.collection('statistics').findOne({}, (err, resultsFull) => {
        res.json({ status: 200, data: resultsFull });
    })
})

router.post('/depositsStat', commonClass.connect, (req, res, next) => {
    var arr = ['depositinrs', 'rec_tx_con_btcs', 'xrp_deposits', 'xlm_deposits', 'rec_tx_con_bchs', 'rec_tx_con_btgs', 'rec_tx_con_ltcs', 'rec_tx_con_puras'];
    var window = {}
    var array = []
    var arrayCount = 0
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var for_date = dateFormat(today, "isoDate");
    async.forEachSeries(arr, (schema, callback) => {
        var total = parseFloat(0);
        var count = 0;
        var count_t = 0;
        arrayCount++;
        req.db.collection(schema).find({ "created": { $gte: new Date(for_date + ' 23:58:58.000Z') }, status: 1 }).toArray(function (err, result) {
            window["todays_total_count"] = result.length;
            if (result.length != 0) {
                async.forEachSeries(result, (amount_to_add, callback2) => {
                    count++;
                    total += parseFloat(amount_to_add.amount);
                    window["todays_total_amount"] = total;
                    if (count == result.length) {
                    } else {
                        callback2();
                    }
                });
            } else {
                window["todays_total_amount"] = 0;
            }
            req.db.collection(schema).find({ status: 1 }, { 'amount': true, currency: true, status: true }).toArray(function (err, resultsFull) {
                var count_t = 0;
                window["till_today_total_count"] = resultsFull.length;
                if (resultsFull.length != 0) {
                    async.forEachSeries(resultsFull, (till_amount_to_add, callback3) => {
                        count_t++;
                        total += parseFloat(till_amount_to_add.amount);
                        window["till_today_total_amount"] = total;
                        if (count_t == resultsFull.length) {
                            window["currency"] = till_amount_to_add.currency.toUpperCase();
                            array.push(window)
                            window = {}
                            if (arrayCount == arr.length) {
                                res.json({ status: 200, data: array });
                            } else {
                                callback()
                            }
                        } else {
                            callback3();
                        }
                    });
                } else {
                    if (arrayCount == arr.length) {
                        res.json({ status: 200, data: array });
                    } else {
                        callback()
                    }
                }
            })
        })
    });
});


router.post('/PendingdepositsStat', commonClass.connect, (req, res, next) => {
    var arr = ['depositinrs', 'rec_tx_con_btcs', 'xrp_deposits', 'xlm_deposits', 'rec_tx_con_bchs', 'rec_tx_con_btgs', 'rec_tx_con_ltcs', 'rec_tx_con_puras'];
    var window = {}
    var array = []
    var arrayCount = 0
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var for_date = dateFormat(today, "isoDate");
    async.forEachSeries(arr, (schema, callback) => {
        var total = parseFloat(0);
        var count = 0;
        var count_t = 0;
        arrayCount++;
        req.db.collection(schema).find({ "created": { $gte: new Date(for_date + ' 23:58:58.000Z') }, status: 0 }, { 'amount': true, currency: true, status: true }).toArray(function (err, result) {
            window["todays_total_count"] = result.length;
            if (result.length != 0) {
                async.forEachSeries(result, (amount_to_add, callback2) => {
                    count++;
                    total += parseFloat(amount_to_add.amount);
                    window["todays_total_amount"] = total;
                    if (count == result.length) {
                    } else {
                        callback2();
                    }
                });
            } else {
                window["todays_total_amount"] = 0;
            }

            req.db.collection(schema).find({ status: 0 }, { 'amount': true, currency: true, status: true }).toArray(function (err, resultsFull) {
                var count_t = 0;
                window["till_today_total_count"] = resultsFull.length;
                if (resultsFull.length != 0) {
                    async.forEachSeries(resultsFull, (till_amount_to_add, callback3) => {
                        count_t++;
                        total += parseFloat(till_amount_to_add.amount);
                        window["till_today_total_amount"] = total;
                        if (count_t == resultsFull.length) {
                            window["currency"] = till_amount_to_add.currency.toUpperCase();
                            array.push(window)
                            window = {}
                            if (arrayCount == arr.length) {
                                res.json({ status: 200, data: array });
                            } else {
                                callback()
                            }
                        } else {
                            callback3();
                        }
                    });
                } else {
                    if (arrayCount == arr.length) {
                        res.json({ status: 200, data: array });
                    } else {
                        callback()
                    }
                }
            })
        })
    });
});



router.post('/getWithdrawalStatic', commonClass.connect, (req, res, next) => {
    var arr = ['withdrawamounts', 'send_tx_btcs', 'send_tx_btgs', 'send_tx_ltcs', 'send_tx_bches', 'xrp_withdrawals', 'xlm_withdrawls', 'send_tx_puras'];
    var window = {}
    var array = []
    var arrayCount = 0
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var for_date = dateFormat(today, "isoDate");
    async.forEachSeries(arr, (schema, callback) => {
        var total = parseFloat(0);
        var count = 0;
        var count_t = 0;
        arrayCount++;
        req.db.collection(schema).find({ "created": { $gte: new Date(for_date + ' 23:58:58.000Z') }, status: 1 }).toArray(function (err, result) {
            window["todays_total_count"] = result.length;
            if (result.length != 0) {
                async.forEachSeries(result, (amount_to_add, callback2) => {
                    count++;
                    total += parseFloat(amount_to_add.amount);
                    window["todays_total_amount"] = total;
                    if (count == result.length) {
                    } else {
                        callback2();
                    }
                });
            } else {
                window["todays_total_amount"] = 0;
            }
            req.db.collection(schema).find({ status: 1 }, { 'amount': true, currency: true, status: true }).toArray(function (err, resultsFull) {
                var count_t = 0;
                window["till_today_total_count"] = resultsFull.length;
                if (resultsFull.length != 0) {
                    async.forEachSeries(resultsFull, (till_amount_to_add, callback3) => {
                        count_t++;
                        total += parseFloat(till_amount_to_add.amount);
                        window["till_today_total_amount"] = total;
                        if (count_t == resultsFull.length) {
                            window["currency"] = till_amount_to_add.currency.toUpperCase();
                            array.push(window)
                            window = {}
                            if (arrayCount == arr.length) {
                                res.json({ status: 200, data: array });
                            } else {
                                callback()
                            }
                        } else {
                            callback3();
                        }
                    });
                } else {
                    if (arrayCount == arr.length) {
                        res.json({ status: 200, data: array });
                    } else {
                        callback()
                    }
                }
            })
        })
    });
});

router.post('/getPendingWithdrawalStatic', commonClass.connect, (req, res, next) => {
    var arr = ['withdrawamounts', 'send_tx_btcs', 'send_tx_btgs', 'send_tx_ltcs', 'send_tx_bches', 'xrp_withdrawals', 'xlm_withdrawls', 'send_tx_puras'];
    var window = {}
    var array = []
    var arrayCount = 0
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var for_date = dateFormat(today, "isoDate");
    async.forEachSeries(arr, (schema, callback) => {
        var total = parseFloat(0);
        var count = 0;
        var count_t = 0;
        arrayCount++;
        req.db.collection(schema).find({ "created": { $gte: new Date(for_date + ' 23:58:58.000Z') }, status: 0 }).toArray(function (err, result) {
            window["todays_total_count"] = result.length;
            if (result.length != 0) {
                async.forEachSeries(result, (amount_to_add, callback2) => {
                    count++;
                    total += parseFloat(amount_to_add.amount);
                    window["todays_total_amount"] = total;
                    if (count == result.length) {
                    } else {
                        callback2();
                    }
                });
            } else {
                window["todays_total_amount"] = 0;
            }
            req.db.collection(schema).find({ status: 0 }, { 'amount': true, currency: true, status: true }).toArray(function (err, resultsFull) {
                var count_t = 0;
                window["till_today_total_count"] = resultsFull.length;
                if (resultsFull.length != 0) {
                    async.forEachSeries(resultsFull, (till_amount_to_add, callback3) => {
                        count_t++;
                        total += parseFloat(till_amount_to_add.amount);
                        window["till_today_total_amount"] = total;
                        if (count_t == resultsFull.length) {
                            window["currency"] = till_amount_to_add.currency.toUpperCase();
                            array.push(window)
                            window = {}
                            if (arrayCount == arr.length) {
                                res.json({ status: 200, data: array });
                            } else {
                                callback()
                            }
                        } else {
                            callback3();
                        }
                    });
                } else {
                    if (arrayCount == arr.length) {
                        res.json({ status: 200, data: array });
                    } else {
                        callback()
                    }
                }
            })
        })
    });
});

router.post('/getKycStats', commonClass.connect, (req, res, next) => {
    var arr = ['kyc_status', 'bank_status', 'id_status', 'aadhaar_status'];
    var data = {}
    var array = []
    var arrayCount = 0
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var for_date = dateFormat(today, "isoDate");
    async.forEachSeries(arr, (kyc, callback) => {
        var total = parseFloat(0);
        var count = 0;
        var count_t = 0;
        arrayCount++;
        var st = req.body.status;

        req.db.collection('users').count({ [kyc]: parseInt(st), "created": { $gte: new Date(for_date + ' 23:58:58.000Z') } }, (err, result) => {
            data['todays_total_pending_count'] = result;
            data['array_name'] = kyc;
            req.db.collection('users').count({ [kyc]: parseInt(st) }, (err, results) => {
                data['total_pending_count'] = results;
                array.push(data)
                data = {}
                if (arrayCount == arr.length) {
                    res.json({ status: 200, data: array });
                    array = []
                } else {
                    callback()
                }
            })
        })
    });
});

router.post('/getUsers', commonClass.connect, (req, res, next) => {
    var arr = ['kyc_status'];
    var data = {}
    var array = []
    var arrayCount = 0
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var for_date = dateFormat(today, "isoDate");
    async.forEachSeries(arr, (kyc, callback) => {
        var total = parseFloat(0);
        var count = 0;
        var count_t = 0;
        arrayCount++;
        var st = req.body.status;

        req.db.collection('users').count({}, (err, result) => {
            data['total_users'] = result;
            res.json({ status: 200, data: data });
        })
    });
});

router.post('/getHighestBalances', commonClass.connect, (req, res, next) => {
    var highest_balances = [];
    req.db.collection('currencies_details').findOne({}, (err, result) => {
        for (i = 0; i < result.currencies.length; i++) {
            var c = result.currencies[i];

            //    var currency_is = result.currencies[i];

            req.db.collection('users').aggregate([{ $unwind: '$balances' },
            {
                $match: {
                    'balances.currency': result.currencies[i],
                    "balances.current_balance": { "$exists": true, "$ne": "NaN" }
                }
            },
            { $project: { "balances.current_balance": 1, "client_id": 1 } },
            {
                $sort: {
                    'balances.current_balance': -1
                }
            }, { $limit: 3 }], { "allowDiskUse": true }, (err, results) => {
                console.log("results", c);


                // for (j = 0; j < 10; j++) {
                //     console.log("currency_is", currency_is)
                //     highest_balances.push({
                //         client_id: results[j].client_id,
                //         currency: result.currencies[i],
                //         balance: results[j].balances.current_balance
                //     })
                // }
                // res.json({ status: 200, data: highest_balances });

            }
            )
        }
        // res.json({ code: 300, currencies_details: result })
    })

    console.log(highest_balances)

});




router.post('/getBalanceStats', commonClass.connect, (req, res, next) => {
    var arr = ['btc', 'inr'];
    var data = {}
    var array = []
    var arrayCount = 0
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var for_date = dateFormat(today, "isoDate");

    async.forEachSeries(arr, (currency, callback) => {
        var total = parseFloat(0);
        var count = 0;
        var count_t = 0;
        arrayCount++;
        var st = req.body.status;

        // console.log("currency is", currency);
        // req.db.collection('users').find({balances: { $elemMatch: { 'currency': currency } }},{client_id:true, balances:true}).limit(10).toArray( (err, result) => {
        // req.db.collection('users').find({balances: { $elemMatch: { 'currency': currency } }}, (err, result) => {
        req.db.collection('users').aggregate(
            [
                {
                    $group:
                        {
                            balances: { $elemMatch: { 'currency': currency } },
                            'client_id': { $sum: "client_id" },
                            count: { $sum: 1 }
                        }
                }
            ]
        ).limit(10).toArray((err, result) => {

            console.log(result);

            // res.json({ status: 200, data: result });

        })

        // callback();

    });
});




module.exports = router;
