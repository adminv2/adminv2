var express = require('express')
global.mongoose = require('mongoose');
var router = express.Router();
var path = require('path');
var cors = require('cors');
var bodyParser = require('body-parser');
var sha256 = require('js-sha256');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
 //const url = 'mongodb://btcmonk:warning%401234!@54.87.131.220:27017/btcmonk';
//const url = 'mongodb://localhost/btcmonk';
//console.log(sha256('Admin$#@!'))
// connection to mongo
// mongoose.connect('mongodb://btcmonk:warning%401234!@54.87.131.220:27017/btcmonk', {
// });
// const db = mongoose.connection;

var app = express();
// const commonClassObject = require('./common.js');
// global.commonClass = new commonClassObject();
// add routing 
var user = require('./router/user.js');
var profile = require('./router/user-profile-list.js');
var dailyinterests = require('./router/dailyinterests.js');
var kyc = require('./router/kyc.js');
var deposits = require('./router/deposits.js');
var common = require('./router/common.js');
var withdraw = require('./router/withdraw.js');  
var referals = require('./router/referral.js');
var trade = require('./router/trade.js');
var fileupload = require('./router/file-upload.js');
var payout = require('./router/payout_histr.js');
var exportall = require('./router/export_all.js');
var admin_details = require('./router/admin_details.js');



var emailservice = require('./router/email-engine.js');
var notificationservice = require('./router/custom-notify.js');  
var userBalance = require('./router/balance-list.js'); 
var dashboard = require('./router/dashboard.js');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.set('views', path.join(__dirname, 'views'));
// app.set('views', path.join(__dirname, 'views'));
app.use(express.static(__dirname + '/public'));
app.use(cors());
app.use(express.static(path.join(__dirname, '../angular-frontend/dist')));

app.use(function (req, res, next) {
if (req.url.substr(-1) === '/') {
      return res.send({
          message: "Welcome To BTC Monk.!"
      });
  }
  next();
});


app.use('/user',user);
app.use('/profile',profile);
app.use('/admin',admin_details);

app.use('/userKyc',kyc);
app.use('/export',exportall);
app.use('/file',fileupload);
app.use('/notifications',notificationservice);
app.use('/deposits',deposits);
app.use('/common',common);
app.use('/withdrawal',withdraw);
app.use('/ref',referals);
app.use('/bal',userBalance);
app.use('/tradeUser',trade);
app.use('/dashboard',dashboard);

 //email-service api created by Satish Ameda Date:3/16/2018
 app.use('/email',emailservice);
 app.use('/payout',payout);
app.use('/interests',dailyinterests);
app.use(function (err, req, res, next) {
  console.log("This is error handling function",err);
  res.status(err.status).send(err);
});
app.listen(8080, () => console.log('Example app listening on port 8080!'))
 

