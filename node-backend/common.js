var nodemailer = require('nodemailer');
const MongoClient = require('mongodb').MongoClient;
// const url = 'mongodb://btcmonk:warning%401234!@34.207.166.123:27017/btcmonk';
const url = 'mongodb://btcmonk:warning%401234!@34.233.98.34:27017/btcmonk';
// var admin = require("firebase-admin");

var count = 0;
var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
var serviceAccount = require("./serviceAccountKey.json");
var gcm = require("node-gcm");
var sender;

// admin.initializeApp({
//     credential: admin.credential.cert(serviceAccount),
//     databaseURL: "https://btcmonk-45364.firebaseio.com"
// });


class common {
    connect(req, res, next) {
        MongoClient.connect(url, function (err, client) {
            // console.log("Connected successfully to server");
            req.db = client.db('btcmonk');
            next()
        });
    }
    mail(to, subject, html, attachment = null, filename = null, attachmentType = null) {
        var username = "btcmonk"
        var password = "btcmonk@123"
        let transporter = nodemailer.createTransport({
            host: 'smtp.sendgrid.net',
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: username, // generated ethereal user
                pass: password  // generated ethereal password
            }
        });
        if (attachmentType == 'csv') {
            var attachmentData = { filename: filename, content: attachment }
        } else if (attachmentType == 'pdf') {
            var attachmentData = { filename: filename, contentType: 'application/pdf', path: attachment }
        } else {
            var attachmentData = null
        }
        let mailOptions = {
            from: 'BTCMonk<noreply@btcmonk.com>', // sender address
            to: to, // list of receivers
            subject: subject, // Subject line
            // html: html+"<br> ========================================= <br> Thanks and Regards <br> BTCMonk", // plain text body
            html: html, // plain text body
            attachments: (attachmentData != null) ? attachmentData : ''
        };
        transporter.sendMail(mailOptions, function (err, done) {
            console.log("done")

            if (attachmentType == 'pdf') {
                fs.unlink(attachment, (err, doc) => {
                    // console.log(doc) 
                })
            }
            return;
        });
    }

    sendingNotifications(client_id, devices, type, body, title, db, link = null) {
        // console.log("1",client_id);
        this.saveNotificationToDb(client_id, type, body, title, db, link)
        devices.forEach(element => {
            if (element.device_type === 'ios') {
                sender = new gcm.Sender(serviceAccount.ios);//IOS
                if (element.fcmToken && element.fcmToken != undefined && element.fcmToken != null) {
                    var message = new gcm.Message({
                        notification: {
                            title: title,
                            body: body,
                            // sound: "audience_applause_btcmonk.caf"
                              sound: "default"
                        },
                    });
                    sender.sendNoRetry(message, { "to": element.fcmToken }, (err, response) => {
                        //   if (err) console.error("error",err);
                        //   else console.log("Response-ios",response);
                    });
                }
            } else {
                // console.log("Device type is", element.device_type);
                if (element.device_type === 'android') {
                    // console.log("token ",serviceAccount.android);
                    sender = new gcm.Sender(serviceAccount.android);//Andriod
                    if (element.fcmToken && element.fcmToken != undefined && element.fcmToken != null) {
                        var message = new gcm.Message({
                            notification: {
                                title: title,
                                body: body,
                                // sound: "audience_applause_btcmonk.mp3",
                                  sound: "default"
                            },
                        });
                        sender.sendNoRetry(message, { "to": element.fcmToken }, (err, response) => {
                            if (err) console.error("error", err);
                            else
                                console.log("Response-andriod", response);
                        });
                    }
                }
            }
        })
    }






    saveNotificationToDb(client_id, type, message, title, db, link) {
        console.log("2",client_id);
        db.collection('notifications').find({ client_id: client_id }).sort({ 'created': 1 }).toArray((err, userDetails) => {
            if (userDetails.length >= 10) {
                db.collection('notifications').update({ _id: ObjectId(userDetails[0]._id), client_id: client_id }, { $set: { body: message, type: type, created: new Date(), title: title, link: link } }, (err, status) => {
                    console.log('Successfully sent message:');
                })
            } else {
                var obj = {
                    client_id: client_id,
                    body: message,
                    type: type,
                    created: new Date(),
                    status: 1,
                    image: '',
                    title: title,
                    link: link
                }
                db.collection('notifications').insert(obj, (err, data) => {
                    console.log('Successfully sent message:');
                })
            }
        });
    }




    getCurrencies(req, res) {

        // console.log(req);
        req.db.collection('currencies_details').find().toArray((err, userDetails) => {
            res.status(201).json({ code: 153 })
            // console.log("userDetails", userDetails);


        })
    }


    auth(req, res, next) {
    }
}

module.exports = common;